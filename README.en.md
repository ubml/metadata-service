# metadata-service

### Introduction
The business processing flow relies on specific metadata content, and different types of metadata need to be parsed in different processing stages. The Metadata Service provides a unified runtime metadata service interface, supporting services such as runtime metadata loading, external metadata push and storage, and runtime metadata extension.
### Software Architecture

The business processing flow relies on the Metadata RPC Client service to obtain the metadata to be used.

![serviceinvokestructure](./imgs/serviceinvokestructure.png)

The Metadata Service primarily includes services such as:
1. Metadata Retrieval and Query Service: This service handles the retrieval and querying of metadata based on specific criteria.
2. Cache Handling (Loading, Updating): This service manages the loading and updating of metadata in the cache.
3. External System Metadata Push and Storage: This service deals with the storage of metadata pushed from external systems.
4. Metadata Deployment Service: This service is responsible for deploying metadata and ensuring it is properly configured for use.
5. Runtime Metadata Extension: This service enables the extension of runtime metadata by adding or modifying metadata during runtime.

![servicestructure](./imgs/metadataservicestructure.png)

[//]: # (#### 使用)
[//]: # ()
[//]: # (（待补充）)

#### Maven dependency

```xml
<properties>
    <metadata.service.api.version>0.1.0</metadata.service.api.version>
</properties>
```
```xml
<dependencies>
    <dependency>
        <groupId>com.inspur.edp</groupId>
        <artifactId>metadata-service-api</artifactId>
        <version>${metadata.service.api.version}</version>
    </dependency>
</dependencies>
```

### Contributing
To contribute, please refer to [How to Contribute](https://gitee.com/ubml/community/blob/master/CONTRIBUTING.md) for assistance.

### License
[Apache License 2.0](https://gitee.com/ubml/metadata-common/blob/master/LICENSE)





