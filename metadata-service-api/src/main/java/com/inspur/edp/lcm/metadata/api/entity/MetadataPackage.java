/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.lcm.metadata.api.entity;

import lombok.Data;

import java.util.List;

/**
 * @Classname MetadataPackage
 * @Description 元数据包
 * @Date 2019/7/19 16:18
 * @Created by liu_bintr
 * @Version 1.0
 */
@Data
public class MetadataPackage {
    /**
     元数据包的头节点，包含包名及版本等基础信息
     <see cref="MetadataPackageHeader"/>

     */
    private MetadataPackageHeader header;

    /**
     元数据包所在的服务单元信息
     <see cref="MetadataPackageServiceUnitInfo"/>

     */
    private ServiceUnitInfo serviceUnitInfo;

    /**
     元数据包的依赖关系
     <see cref="List{T}"/>
     <see cref="MetadataPackageReference"/>

     */
    private List<MetadataPackageReference> reference;

    /**
     元数据包中所包含的元数据的基本信息
     <see cref="List{T}"/>
     <see cref="GSPMetadata"/>

     */
    private List<GspMetadata> metadataList;
}
