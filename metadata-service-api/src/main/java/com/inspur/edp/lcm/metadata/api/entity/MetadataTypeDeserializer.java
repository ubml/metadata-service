/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.lcm.metadata.api.entity;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.deser.ResolvableDeserializer;
import com.fasterxml.jackson.databind.node.ValueNode;
import io.iec.edp.caf.boot.context.CAFContext;

import java.io.IOException;

/**
 * 元数据类型自定义反序列化器
 *
 * @author liuyuxin01
 * @since
 */
public class MetadataTypeDeserializer extends JsonDeserializer<MetadataType> implements ResolvableDeserializer {

    private final static String LANGUAGE_DEFAULT = "zh-CHS";
    private final static String KEY_CONNECTOR = "_";
    private final static String KEY_TYPENAME = "TypeName";
    private final static String KEY_GROUP_NAME = "GroupName";

    private JsonDeserializer<?> defaultDeserializer;

    public MetadataTypeDeserializer(JsonDeserializer<?> defaultDeserializer) {
        super();
        this.defaultDeserializer = defaultDeserializer;
    }

    @Override
    public MetadataType deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        String language = CAFContext.current.getLanguage();
        String postfix = LANGUAGE_DEFAULT.equals(language) ? "" : KEY_CONNECTOR + language;

        MetadataType metadataType = null;
        TreeNode treeNode = jsonParser.readValueAsTree();
        try (JsonParser jp = jsonParser.getCodec().treeAsTokens(treeNode)) {
            if (jp.currentToken() == null) {
                jp.nextToken();
            }
            metadataType = (MetadataType) defaultDeserializer.deserialize(jp, deserializationContext);
            TreeNode typeNameNode = treeNode.get(KEY_TYPENAME + postfix);
            if (typeNameNode != null && typeNameNode.isValueNode()) {
                metadataType.setTypeName(((ValueNode) typeNameNode).asText(""));
            }
            TreeNode groupNameNode = treeNode.get(KEY_GROUP_NAME + postfix);
            if (groupNameNode != null && groupNameNode.isValueNode()) {
                metadataType.setGroupName(((ValueNode) groupNameNode).asText(""));
            }
        }
        return metadataType;
    }


    @Override
    public void resolve(DeserializationContext deserializationContext) throws JsonMappingException {
        ((ResolvableDeserializer) defaultDeserializer).resolve(deserializationContext);
    }
}
