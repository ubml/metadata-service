/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.lcm.metadata.api.entity;

import lombok.Data;

import java.util.Map;

import java.time.LocalDateTime;

/**
 * Classname MetadataDto
 * Description 通过WebApi调用时前端往后端传递元数据信息所用实体
 * Date 2019/11/14 19:54
 *
 * @author zhongchq
 * @version 1.0
 */
@Data
public class MetadataDto {
    public String id;
    public String nameSpace;
    public String code;
    public String name;
    public String fileName;
    public String type;
    public String bizobjectID;
    public String language;
    public boolean isTranslating;
    public void setIsTranslating(boolean isTranslating){
        this.isTranslating = isTranslating;
    }
    public String relativePath;
    public String extendProperty;
    public String content;
    public boolean extendable;
    public String refs;
    public boolean extented;
    public String previousVersion;
    public String version;
    public MetadataProperties properties;
    public String extendRule;
    public String projectName;
    public String processMode;
    public String mdsha1;
    public String createdOn;
    public String lastChangedOn;
    public String lastChangedBy;
    public String mdpkgId;
    public Map<String, String> nameLanguage;

    public String getName() {
        if(name == null){
            return "";
        }
        return name;
    }
}
