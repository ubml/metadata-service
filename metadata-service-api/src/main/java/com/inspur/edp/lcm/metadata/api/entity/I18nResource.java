/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.lcm.metadata.api.entity;

import lombok.Data;

@Data
public class I18nResource {
    /// <summary>
    /// 语言
    /// </summary>
    private String language;

    /// <summary>
    /// 资源类型
    /// </summary>
    private ResourceType resourceType;

    /// <summary>
    /// 资源位置
    /// </summary>
    private ResourceLocation resourceLocation;

    /// <summary>
    /// 资源包内容
    /// </summary>
    private I18nResourceItemCollection StringResources;

    private I18nResourceItemCollection ImageResources;
}
