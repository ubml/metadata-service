/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.lcm.metadata.api.entity;

import lombok.Data;

/**
 * Classname Metadata4RefDto
 * Description TODO
 * Date 2019/11/15 10:18
 *
 * @author zhongchq
 * @version 1.0
 */
@Data
public class Metadata4RefDto {

    public MetadataPackageHeader packageHeader;
    public MetadataDto metadata;

    /**
     元数据包服务单元信息类
     <see cref="ServiceUnitInfo"/>

     */
    public ServiceUnitInfo serviceUnitInfo;

    public Boolean isAcrossSU;
}
