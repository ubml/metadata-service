/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.lcm.metadata.api.entity;

import lombok.Data;

@Data
public class I18nResourceItem {
    /// <summary>
    /// 构造函数
    /// </summary>
    public I18nResourceItem(){

    }

    /// <summary>
    /// 构造函数
    /// </summary>
    /// <param name="key">key</param>
    /// <param name="value">value</param>
    public I18nResourceItem(String key, String value)
    {
        this.key = key;
        this.value = value;
    }

    /// <summary>
    /// 唯一识别标识
    /// </summary>
    private String key;

    /// <summary>
    /// 对应值
    /// </summary>
    private String value;

    /// <summary>
    /// 注释
    /// </summary>
    private String comment;

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof I18nResourceItem){
            return this.key.equals(((I18nResourceItem)obj).key);
        }
        else{
            return false;
        }
    }
}
