/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.lcm.metadata.api.entity;

/**
 * desc
 *
 * @author liangff
 * @since 0.1.36
 */
public enum OperationEnum {
    // 热加载
    HOT_LOAD,
    // 元数据打包
    METADATA_PACK,
    // 生成web代码
    WEB_GENERATE,
    // 资源元数据编译
    RESOURCE,
    // 多语元数据编译
    LINGUISTIC,
    // java代码生成
    JAVA_GENERATE,
    // maven编译
    MAVEN,
    // 元数据保存
    METADATA_SAVE
}
