/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.metadata.rtcustomization.api;


/**
 * @author zhaoleitr
 */
public interface ICustomizedContent {
    /*
    增量的基本描述信息，需要扩展的元数据需要实现此接口
     */
    //增量id，目前暂与元数据id相同
    String id = null;
    //增量编号，可以暂不赋值，也 可以在元数据编号基础上加-c
    String code = null;
    //增量名称，可以不赋值
    String name = null;
    //增量类型，增量类型与具体元数据类型相同，表明这是哪种元数据的增量
    String type = null;
}
