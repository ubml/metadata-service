/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.metadata.rtcustomization.api.entity.config;

import java.io.Serializable;
import lombok.Data;

/**
 * 配置数据：配置信息节点
 */
@Data
public class ConfigDataConfiguration implements Serializable {
    /**
     * 表名称
     */
    private String tableName;
    /**
     * 描述
     */
    private String description;
    /**
     * 是否启用
     */
    private Boolean active;
    /**
     * 顺序号
     */
    private Integer order;
}
