/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.metadata.rtcustomization.api.entity;

public enum SourceTypeEnum {
    CUSTOMIZED(0, "运行时定制发布"),
    MDPKG(1, "元数据包"),
    NOCODE(2, "零代码发布"),
    GENERATED(3, "运行时生成"),
    CUSTOMIZING(4, "运行时定制中"),
    UNKNOWN(-1, "未知");
    int code;
    String desc;

    SourceTypeEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public static SourceTypeEnum fromValue(int value) {
        for (SourceTypeEnum type : SourceTypeEnum.values()) {
            if (type.getCode() == value) {
                return type;
            }
        }
        return UNKNOWN; // 默认返回UNKNOWN
    }

}
