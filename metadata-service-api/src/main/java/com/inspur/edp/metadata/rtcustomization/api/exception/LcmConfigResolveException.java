/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.metadata.rtcustomization.api.exception;

import io.iec.edp.caf.commons.exception.CAFRuntimeException;
import io.iec.edp.caf.commons.exception.ExceptionLevel;

/**
 * 配置文件解析异常类
 *
 * 解析配置文件出错后，可以使用此类抛出异常，需抛出配置文件信息或者节点信息
 *
 * @author liangff
 * @since 0.1.0
 */
public class LcmConfigResolveException extends CAFRuntimeException {

    public LcmConfigResolveException(ErrorCodes errCodes, String... messageParams) {
        super(ExceptionResource.SU, ExceptionResource.RESOURCE_FILE, errCodes.name(), messageParams, null, ExceptionLevel.Error, true);
    }

    public LcmConfigResolveException(Throwable innerException, ErrorCodes errCodes, String... messageParams) {
        super(ExceptionResource.SU, ExceptionResource.RESOURCE_FILE, errCodes.name(), messageParams, innerException, ExceptionLevel.Error, true);
    }
}
