/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.metadata.rtcustomization.api.entity;

import lombok.Data;

@Data
public class GspMdContentData {
    private String metadataId;
    private String version;
    private String certId;
    private String code;
    private String name;
    private String type;
    private String bizObjId;
    private boolean extended;
    private String nameSpace;

    public GspMdContentData(String metadataId,
                            String version,
                            String certId,
                            String code,
                            String name,
                            String type,
                            String bizObjId,
                            boolean extended,
                            String nameSpace) {
        this.metadataId = metadataId;
        this.version = version;
        this.certId = certId;
        this.code = code;
        this.name = name;
        this.type = type;
        this.bizObjId = bizObjId;
        this.extended = extended;
        this.nameSpace = nameSpace;
    }
}
