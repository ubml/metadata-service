/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.metadata.rtcustomization.api.entity;

import com.inspur.edp.lcm.metadata.api.entity.ProcessMode;

import java.util.ArrayList;
import java.util.List;

public class MetadataFilter {
    private List<String> suCodes;
    
    private List<String> businessObjectIds;

    private List<String> metadataTypes;

    private List<ProcessMode> processModes;

    private String metadataNamespace;
    private String metadataCode;
    private String mdpkgName;
    private SourceTypeEnum sourceType;

    public MetadataFilter() {}

    public MetadataFilter(String metadataCode, String metadataTypeCode, String metadataNamespace, SourceTypeEnum sourceType) {
        this.metadataCode = metadataCode;
        this.metadataTypes = new ArrayList<>();
        this.metadataTypes.add(metadataTypeCode);
        this.metadataNamespace = metadataNamespace;
        this.sourceType = sourceType;
    }

    public MetadataFilter(SourceTypeEnum sourceType, List<String> metadataTypes) {
        this.sourceType = sourceType;
        this.metadataTypes = metadataTypes;
    }

    public MetadataFilter(List<String> metadataTypes) {
        this.metadataTypes = metadataTypes;
    }

    public String getMdpkgName() {
        return mdpkgName;
    }

    public void setMdpkgName(String mdpkgName) {
        this.mdpkgName = mdpkgName;
    }

    public List<String> getSuCodes() {
        return suCodes;
    }

    public void setSuCodes(List<String> suCodes) {
        this.suCodes = suCodes;
    }

    public List<String> getMetadataTypes() {
        return metadataTypes;
    }

    public void setMetadataTypes(List<String> metadataTypes) {
        this.metadataTypes = metadataTypes;
    }

    public List<ProcessMode> getProcessModes() {
        return processModes;
    }

    public void setProcessModes(List<ProcessMode> processModes) {
        this.processModes = processModes;
    }

    public String getMetadataNamespace() {
        return metadataNamespace;
    }

    public void setMetadataNamespace(String metadataNamespace) {
        this.metadataNamespace = metadataNamespace;
    }

    public String getMetadataCode() {
        return metadataCode;
    }

    public void setMetadataCode(String metadataCode) {
        this.metadataCode = metadataCode;
    }

    public SourceTypeEnum getSourceType() {
        return sourceType;
    }

    public void setSourceType(SourceTypeEnum sourceType) {
        this.sourceType = sourceType;
    }

    public List<String> getBusinessObjectIds() {
        return businessObjectIds;
    }

    public void setBusinessObjectIds(List<String> businessObjectIds) {
        this.businessObjectIds = businessObjectIds;
    }
}
