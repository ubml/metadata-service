/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.metadata.rtcustomization.api.entity;

import lombok.Data;

/**
 * 元数据查询参数类
 */
@Data
public class MetadataQueryParam {
    /**
     * 元数据id，非空
     */
    public String metadataId;

    /**
     * 是否国际化，默认true
     */
    public Boolean isI18n = true;

    /**
     * 标识查询设计时或运行时元数据，非空
     */
    public MetadataScopeEnum metadataScopeEnum;

    public MetadataQueryParam() {
    }

    public MetadataQueryParam(String metadataId, Boolean isI18n, MetadataScopeEnum metadataScopeEnum) {
        this.metadataId = metadataId;
        this.isI18n = isI18n;
        this.metadataScopeEnum = metadataScopeEnum;
    }
}
