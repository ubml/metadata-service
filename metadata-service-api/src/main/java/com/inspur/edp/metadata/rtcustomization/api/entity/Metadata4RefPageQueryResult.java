/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.metadata.rtcustomization.api.entity;

import com.inspur.edp.lcm.metadata.api.entity.Metadata4Ref;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;


/**
 * 元数据查询结果类
 */
@Data
public class Metadata4RefPageQueryResult {

    /**
     * 分页查询返回结果
     */
    private List<Metadata4Ref> list;

    /**
     * 元数据总数
     */
    private int total;

    /**
     * 构造函数
     */
    public Metadata4RefPageQueryResult() {
        this.list = new ArrayList<>();
    }

    /**
     * 构造函数
     *
     * @param list  查询结果
     * @param total 本地元数据条数
     */
    public Metadata4RefPageQueryResult(List list, int total) {
        this.list = list;
        this.total = total;
    }

}
