/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.metadata.rtcustomization.api.entity;

import com.inspur.edp.lcm.metadata.api.entity.uri.AbstractURI;
import com.inspur.edp.lcm.metadata.api.entity.uri.MetadataURI;
import lombok.Data;

/**
 * desc
 *
 * @author liangff
 * @since
 */
@Data
public class MetadataURIQueryParam {
    /**
     * 目标元数据标识
     */
    private MetadataURI metadataURI;

    /**
     * 源元数据标识
     */
    private AbstractURI sourceURI;

    /**
     * 是否国际化，默认true
     */
    private Boolean isI18n = true;

    /**
     * 标识查询设计时或运行时元数据，非空
     */
    private MetadataScopeEnum metadataScopeEnum;

    public MetadataURIQueryParam() {
    }

    public MetadataURIQueryParam(MetadataURI metadataURI, MetadataScopeEnum metadataScopeEnum) {
        this.metadataURI = metadataURI;
        this.metadataScopeEnum = metadataScopeEnum;
    }

    public MetadataURIQueryParam(MetadataURI metadataURI, AbstractURI sourceURI, Boolean isI18n, MetadataScopeEnum metadataScopeEnum) {
        this.metadataURI = metadataURI;
        this.sourceURI = sourceURI;
        this.isI18n = isI18n;
        this.metadataScopeEnum = metadataScopeEnum;
    }
}
