/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.metadata.rtcustomization.api.exception;

/**
 * 异常code枚举类
 *
 * 出现异常后，应抛出相应的异常code，异常code对应的异常信息在resources中
 *
 * @author liangff
 * @since 0.1.0
 */
public enum ErrorCodes {
    /**
     * 数据库类型不正确
     */
    ECP_METADATA_DEPLOY_0001,

    /**
     * 数据库连接失败[{0}]
     */
    ECP_METADATA_DEPLOY_0002,

    /**
     * 解析request[{0}]失败
     */
    ECP_METADATA_DEPLOY_0003,

    /**
     * 部署失败
     */
    ECP_METADATA_DEPLOY_0004,

    /**
     * 备份元数据包失败[{0}]
     */
    ECP_METADATA_DEPLOY_0005,

    /**
     * 元数据字符串反序列化为数据库dto失败[{0}]
     */
    ECP_METADATA_0001,

    /**
     * 调用rpc服务异常[{0}]
     */
    ECP_METADATA_0002,

    /**
     * [{0}]不能为空
     */
    ECP_METADATA_0003,

    /**
     * 入参错误, 请检查必传字段项
     */
    ECP_METADATA_0004,

    /**
     * 当前模板格式不支持，请选择正确的模板文件
     */
    ECP_METADATA_0005,

    /**
     * [{0}]，收集的配置数据中包含空数据
     */
    ECP_METADATA_0006,

    /**
     * 无法构造元数据依赖关系，具体错误详情请查看系统日志
     */
    ECP_METADATA_0007,

    /**
     * 配置数据反序列化发生异常
     */
    ECP_METADATA_0008,

    /**
     * 读取配置失败[{0}]
     */
    ECP_METADATA_0009,

    /**
     * 指定版本号位置超出有效范围[{0}]
     */
    ECP_METADATA_0010,

    /**
     * MetadataPackageVersion的CompareTo方法只能传入MetadataPackageVersion类型的参数
     */
    ECP_METADATA_0011,

    /**
     * 不能为空
     */
    ECP_METADATA_0012,

    /**
     * 保存元数据失败[{0}]-[{1}]-[{2}]
     */
    ECP_METADATA_0013,

    /**
     * 元数据冲突，运行时元数据表中已存在元数据[{1}]-[{2}]-[{3}]
     */
    ECP_METADATA_0014,

    /**
     * 构建元数据包失败
     */
    ECP_METADATA_0015,

    /**
     * 值不在标准范围内[{0}]
     */
    ECP_METADATA_0016,
    /**
     * 找不到元数据[{0}]
     */
    ECP_METADATA_0017,
    /**
     * 元数据冲突，自定义元数据表中已存在元数据[{1}]-[{2}]-[{3}]
     */
    ECP_METADATA_0018,
    /**
     * 元数据冲突，设计时元数据表中已存在元数据[{1}]-[{2}]-[{3}]
     */
    ECP_METADATA_0019,
    /**
     * 元数据[{0}]缺少元数据包信息[{1}]，请找到元数据包并重新部署
     */
    ECP_METADATA_0020,
    /**
     * 运行时元数据[{0}]不存在
     */
    ECP_METADATA_0021,


    /**
     * 运行时定制元数据扩展关系不存在[{0}]
     */
    ECP_METADATA_CUSTOMIZATION_0001,

    /**
     * 传入的维度值信息异常，firstDimensionValue为空，secondDimensionValue不为空，应先有firstDimensionValue为空，rootMetadataId [{0}]，secondDimensionValue [{1}]
     */
    ECP_METADATA_CUSTOMIZATION_0002,

    /**
     * 合并检查冲突失败[{0}]-[{1}]
     */
    ECP_METADATA_CUSTOMIZATION_0003,

    /**
     * 元数据[{0}]存在扩展元数据[{1}]的扩展关系[{2}]，但是扩展元数据不存在
     */
    ECP_METADATA_CUSTOMIZATION_0004,

    /**
     * 删除元数据[{0}]失败，要删除的元数据已被[{0}]等扩展
     */
    ECP_METADATA_CUSTOMIZATION_0005,

    /**
     * 删除元数据[{0}]失败
     */
    ECP_METADATA_CUSTOMIZATION_0006,

    /**
     * 元数据[{0}]扩展关系不存在
     */
    ECP_METADATA_CUSTOMIZATION_0007,

    /**
     * 元数据的[{0}]basicMetadata[{1}]不存在
     */
    ECP_METADATA_CUSTOMIZATION_0008,
    /**
     * 传入的扩展关系信息为空，无法更新
     */
    ECP_METADATA_CUSTOMIZATION_0009,
    /**
     * 传入的扩展关系信息中缺失子元数据标识，无法唯一确定扩展关系，请检查调用点参数构造代码
     */
    ECP_METADATA_CUSTOMIZATION_0010,
    /**
     * 根据传入的扩展元数据标识，找不到扩展关系，请确认扩展关系是否正常预置，扩展元数据id:{0}, namespace:{1}, code:{2}, type:{3}
     */
    ECP_METADATA_CUSTOMIZATION_0011,

    /**
     * 获取配置类失败[{0}]
     */
    ECP_CONFIG_RESOLVE_0001,

    /**
     * 读取配置文件失败[{0}]
     */
    ECP_CONFIG_RESOLVE_0002,

    /**
     * 未能正常获取[{0}]的序列化器，请检查配置
     */
    ECP_CONFIG_RESOLVE_0003,

    /**
     * 构建元数据失败[{0}]
     */
    ECP_CONFIG_RESOLVE_0004,

    /**
     * 系统错误，加载复制元数据扩展类异常[{0}]
     */
    ECP_CONFIG_RESOLVE_0005,

    /**
     * 未能正常获取序列化器，请检查配置。类型为[{0}]
     */
    ECP_CONFIG_RESOLVE_0006,

    /**
     * 指定的监听者[{0}]没有实现[{1}]接口
     */
    ECP_EVENT_0001,
    /**
     * 不支持的NoCodeEventArgType
     */
    ECP_EVENT_0002,

    /**
     * 序列化失败
     */
    ECP_PARSE_0001,

    /**
     * 反序列化失败[{1}]
     */
    ECP_PARSE_0002,

    /**
     * 反序列化的元数据content为空[{0}]
     */
    ECP_PARSE_0003,

    /**
     * 反序列化失败
     */
    ECP_PARSE_0004,

    /**
     * 序列化元数据失败[{0}]
     */
    ECP_PARSE_0005,

    /**
     * 无法解析元数据包中的manifest文件[{0}]
     */
    ECP_PARSE_0006,

    /**
     * 元数据包中没有manifest信息[{0}]
     */
    ECP_PARSE_0007,

    /**
     * 解析元数据包中mainfest信息失败[{0}]
     */
    ECP_PARSE_0008,

    /**
     * 序列化失败[{0}]
     */
    ECP_PARSE_0009,

    /**
     * 对象属性转换失败
     */
    ECP_PARSE_0010,

    /**
     * 序列化元数据失败[{0}]-[{1}]-[{2}]-[{3}]
     */
    ECP_PARSE_0011,

    /**
     * 读取文件失败[{0}]
     */
    ECP_FILE_0001,

    /**
     * 写入文件失败[{0}]
     */
    ECP_FILE_0002,

    /**
     * 未在文件[{0}]中发现节点[{1}]
     */
    ECP_FILE_0003,

    /**
     * 文件不存在[{0}]
     */
    ECP_FILE_0004,

    /**
     * 读取文件失败[{0}]，系统不支持此编码[{1}]
     */
    ECP_FILE_0005,

    /**
     * 解压缩[{0}]失败
     */
    ECP_FILE_0006,

    /**
     * 复制目录出错[{0}]->[{1}]
     */
    ECP_FILE_0007,

    /**
     * 创建文件失败[{0}]
     */
    ECP_FILE_0008,

    /**
     * 无效路径[{0}]，[{1}]
     */
    ECP_FILE_0009,

    /**
     * 读取压缩包出错[{0}]
     */
    ECP_FILE_0010,

    /**
     * 读取压缩包[{0}]中文件[{1}]失败
     */
    ECP_FILE_0011,

    /**
     * 未找到资源项[{0}]对应的对象
     */
    ECP_METADATA_RESOURCE_0001,
    /**
     * 删除的资源项[{0}]不存在
     */
    ECP_METADATA_RESOURCE_0002
}
