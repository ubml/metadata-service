/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.metadata.rtcustomization.api.entity;

import com.inspur.edp.lcm.metadata.api.entity.ProcessMode;
import java.util.List;
import lombok.Data;

/**
 * 元数据查询参数类
 */
@Data
public class Metadata4RefQueryParam {
    /**
     * 元数据类型，可空
     */
    public List<String> metadataTypeList;

    /**
     * 工程模式，可空（仅过滤MDPKG类型的数据，其它数据不区分生成型、解析型）
     */
    public ProcessMode processMode;

    /**
     * 标识查询设计时或运行时元数据, 可空 设计时：CUSTOMIZING、MDPKG、GENERATED 运行时：CUSTOMIZED、NOCODE、MDPKG、GENERATED
     */
    public MetadataScopeEnum metadataScopeEnum;

    /**
     * 标识查询哪些范围的数据，可空 (在MetadataScopeEnum参数的基础上做白名单过滤) CUSTOMIZING 查询运行时定制中的数据 CUSTOMIZED 查询运行时定制已发布的数据 NOCODE
     * 查询零代码已发布的数据 MDPKG 查询设计时入库的数据 GENERATED 查询运行时生成的数据
     */
    public List<SourceTypeEnum> sourceTypeEnumList;

}
