/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.metadata.rtcustomization.core.manager;

import com.inspur.edp.metadata.rtcustomization.api.exception.ErrorCodes;
import com.inspur.edp.metadata.rtcustomization.api.exception.LcmMetadataException;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import io.iec.edp.caf.rpc.api.service.RpcClient;
import io.iec.edp.caf.rpc.api.support.Type;

import java.util.LinkedHashMap;

public class RpcManager<T> {
    private RpcClient client;

    private Type<T> type;

    private String functionName;

    private String SU_CODE = "Lcm";

    public LinkedHashMap<String, Object> getParams() {
        return params;
    }

    public void setParams(LinkedHashMap<String, Object> params) {
        this.params = params;
    }

    public void putParam(String name, Object object) {
        this.params.put(name, object);
    }

    private LinkedHashMap<String, Object> params;

    public RpcManager(Type<T> type, String functionName) {
        client = SpringBeanUtils.getBean(RpcClient.class);
        params = new LinkedHashMap<>();
        this.type = type;
        this.functionName = functionName;
    }

    public T invoke() {
        try {
            return client.invoke(type, functionName, SU_CODE, params, null);
        } catch (Exception e) {
            throw new LcmMetadataException(e, ErrorCodes.ECP_METADATA_0002, functionName);
        }
    }

}
