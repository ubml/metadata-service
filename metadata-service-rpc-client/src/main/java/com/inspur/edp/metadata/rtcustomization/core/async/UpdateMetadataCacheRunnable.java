/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.metadata.rtcustomization.core.async;

import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.cache.MetadataDistCacheManager;
import com.inspur.edp.lcm.metadata.cache.MetadataRtDistCache;
import com.inspur.edp.lcm.metadata.cache.RtCacheHandler;
import com.inspur.edp.lcm.metadata.common.context.RuntimeContext;
import com.inspur.edp.metadata.rtcustomization.api.entity.MetadataScopeEnum;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class UpdateMetadataCacheRunnable implements Runnable {
    private MetadataScopeEnum scope;

    private boolean isI18n;

    private GspMetadata metadata;

    public UpdateMetadataCacheRunnable(MetadataScopeEnum scope, boolean isI18n, GspMetadata metadata) {
        this.scope = scope;
        this.isI18n = isI18n;
        this.metadata = metadata;
    }

    @Override
    public void run() {
        String language = isI18n ? RuntimeContext.getLanguage() : "";
        String metadataKey = RtCacheHandler.getMetadataCacheKey(metadata.getHeader().getId(), language);
        switch (scope) {
            case RUNTIME:
                MetadataRtDistCache.put(metadataKey, metadata, null);
                break;
            case CUSTOMIZING:
                MetadataDistCacheManager.put(metadataKey, metadata, null);
        }
    }
}
