/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.metadata.rtcustomization.core.configuration;

import com.inspur.edp.metadata.rtcustomization.api.CustomizationRtService;
import com.inspur.edp.metadata.rtcustomization.api.CustomizationService;
import com.inspur.edp.metadata.rtcustomization.core.CustomizationRtServiceImp;
import com.inspur.edp.metadata.rtcustomization.core.CustomizationServiceImp;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author zhaoleitr
 */
@Configuration(proxyBeanMethods = false)
@EntityScan("com.inspur.edp.metadata.rtcustomization.api.entity")
public class ServiceConfiguration {

    @Bean
    public CustomizationRtService createCustomizationRtService() {
        return new CustomizationRtServiceImp();
    }

    @Bean
    public CustomizationService createCustomizationService(CustomizationRtService customizationRtService) {
        return new CustomizationServiceImp(customizationRtService);
    }
}
