/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.lcm.metadata.rtservice;

import com.google.common.collect.Lists;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.api.entity.Metadata4Ref;
import com.inspur.edp.lcm.metadata.api.entity.MetadataCustomizationFilter;
import com.inspur.edp.lcm.metadata.api.entity.MetadataRTFilter;
import com.inspur.edp.lcm.metadata.api.entity.ServiceUnitInfo;
import com.inspur.edp.lcm.metadata.api.service.MetadataRTService;
import com.inspur.edp.metadata.rtcustomization.api.CustomizationService;
import com.inspur.edp.metadata.rtcustomization.api.CustomizationRtService;
import com.inspur.edp.metadata.rtcustomization.api.entity.MetadataFilter;
import com.inspur.edp.metadata.rtcustomization.api.entity.MetadataQueryParam;
import com.inspur.edp.metadata.rtcustomization.api.entity.MetadataScopeEnum;
import com.inspur.edp.metadata.rtcustomization.api.entity.SourceTypeEnum;
import com.inspur.edp.metadata.rtcustomization.api.exception.ErrorCodes;
import com.inspur.edp.metadata.rtcustomization.api.exception.LcmMetadataException;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Slf4j
public class MetadataRTServiceImp implements MetadataRTService {

    private final CustomizationService customizationService;

    private final CustomizationRtService customizationRtService;

    public MetadataRTServiceImp() {
        customizationService = SpringBeanUtils.getBean(CustomizationService.class);
        customizationRtService = SpringBeanUtils.getBean(CustomizationRtService.class);
    }

    @Override
    public GspMetadata getMetadataRTByID(String id) {
        MetadataQueryParam metadataQueryParam = new MetadataQueryParam();
        metadataQueryParam.setMetadataId(id);
        // 根据需要传入即可，默认是国际化
        metadataQueryParam.setIsI18n(false);
        metadataQueryParam.setMetadataScopeEnum(MetadataScopeEnum.RUNTIME);
        return customizationService.getGspMetadata(metadataQueryParam);

    }

    @Override
    public GspMetadata getMetadata(String id) {
        MetadataQueryParam metadataQueryParam = new MetadataQueryParam();
        // 传入需要查询的元数据id信息
        metadataQueryParam.setMetadataId(id);
        metadataQueryParam.setIsI18n(true);
        metadataQueryParam.setMetadataScopeEnum(MetadataScopeEnum.RUNTIME);
        return customizationService.getGspMetadata(metadataQueryParam);
    }

    @Override
    public GspMetadata getMetadata(MetadataCustomizationFilter metadataCustomizationFilter) {
        MetadataQueryParam metadataQueryParam = new MetadataQueryParam();
        // 传入需要查询的元数据id信息
        metadataQueryParam.setMetadataId(metadataCustomizationFilter.getMetadataId());
        // 根据需要传入即可，默认是国际化
        metadataQueryParam.setIsI18n(metadataCustomizationFilter.getIsI18n());
        metadataQueryParam.setMetadataScopeEnum(MetadataScopeEnum.RUNTIME);
        return customizationService.getGspMetadata(metadataQueryParam);

    }

    @Override
    public List<Metadata4Ref> getMetadataRefList(String metadataTypes) {
        List<String> typeList;
        if (metadataTypes == null || metadataTypes.isEmpty()) {
            throw new LcmMetadataException(ErrorCodes.ECP_METADATA_0003, "metadataTypes");
        } else {
            String[] types = metadataTypes.split(",");
            typeList = new ArrayList<>(Arrays.asList(types));
        }
        MetadataFilter metadataFilter = new MetadataFilter();
        metadataFilter.setMetadataTypes(typeList);
        return customizationRtService.getMetadataListByFilter(metadataFilter);
    }

    @Override
    public List<Metadata4Ref> getMetadataRefList() {
        return customizationRtService.getMetadataListByFilter(new MetadataFilter(SourceTypeEnum.MDPKG, null));
    }

    @Override
    public List<Metadata4Ref> getMetadataListByFilter(MetadataRTFilter filter) {
        // 获取所有元数据包
        List<Metadata4Ref> metadataList = getMetadataRefList();
        // 根据AppCode进行过滤
        if (filter.getAppCode() != null) {
            metadataList = metadataList.stream().filter(item -> item.getServiceUnitInfo() == null || filter.getAppCode().equalsIgnoreCase(item.getServiceUnitInfo().getAppCode())).collect(Collectors.toList());
        }
        // 根据ServiceUnitCode进行过滤
        if (filter.getServiceUnitCode() != null) {
            metadataList = metadataList.stream().filter(item -> item.getServiceUnitInfo() == null || filter.getServiceUnitCode().equalsIgnoreCase(item.getServiceUnitInfo().getServiceUnitCode())).collect(Collectors.toList());
        }
        // 根据BizobjectID进行过滤
        if (filter.getBizobjectId() != null && filter.getBizobjectId().size() > 0) {
            for (String eachBizobjectId : filter.getBizobjectId()) {
                metadataList = metadataList.stream().filter(item -> eachBizobjectId.equals(item.getMetadata().getHeader().getBizobjectID())).collect(Collectors.toList());
            }
        }
        if (filter.getCode() != null) {
            metadataList = metadataList.stream().filter(item -> item.getMetadata().getHeader().getCode() != null && item.getMetadata().getHeader().getCode().equals(filter.getCode())).collect(Collectors.toList());
        }
        if (filter.getNameSpace() != null) {
            metadataList = metadataList.stream().filter(item -> item.getMetadata().getHeader().getNameSpace() != null && item.getMetadata().getHeader().getNameSpace().equals(filter.getNameSpace())).collect(Collectors.toList());
        }
        if (filter.getType() != null) {
            metadataList = metadataList.stream().filter(item -> item.getMetadata().getHeader().getType() != null && item.getMetadata().getHeader().getType().equals(filter.getType())).collect(Collectors.toList());
        }

        return metadataList;
    }

    @Override
    public GspMetadata getMetadataBySemanticID(String metadataNamespace, String metadataCode, String metadataTypeCode) {
        if (Objects.isNull(metadataNamespace) || Objects.isNull(metadataCode) || Objects.isNull(metadataTypeCode)) {
            return null;
        }
        MetadataFilter metadataFilter = new MetadataFilter();
        metadataFilter.setMetadataNamespace(metadataNamespace);
        metadataFilter.setMetadataCode(metadataCode);
        metadataFilter.setMetadataTypes(Lists.newArrayList(metadataTypeCode));
        List<Metadata4Ref> metadata4RefList = customizationRtService.getMetadataListByFilter(metadataFilter);

        if (org.springframework.util.CollectionUtils.isEmpty(metadata4RefList)) {
            return null;
        }

        return metadata4RefList.get(0).getMetadata();
    }

    @Override
    public ServiceUnitInfo getServiceUnitInfo(String metadataId) {
        return customizationRtService.getServiceUnitInfo(metadataId);
    }

}
