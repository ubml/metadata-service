/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.lcm.metadata.inner.api.utils;

import com.inspur.edp.lcm.metadata.inner.api.BoService;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;

public class BoUtils {

    private static BoService boService;
    private static int count = 0;
    private static final Object lockObject = new Object();

    public static BoService getBoService() {
        try {
            if (boService == null && count == 0) {
                synchronized (lockObject) {
                    boService = SpringBeanUtils.getBean(BoService.class);
                    count++;
                }
            }
            return boService;
        } catch (Exception e) {
            return null;
        }
    }
}
