/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.lcm.metadata.serverapi;

import com.inspur.edp.lcm.metadata.api.entity.MetadataDto;
import io.iec.edp.caf.commons.exception.CAFRuntimeException;
import io.iec.edp.caf.rpc.api.annotation.GspServiceBundle;

/**
 * 功能描述:零代码RPC接口，实现暂时放入运行时定制的Server中
 *
 * @ClassName: NoCodeServerService
 * @Author: Fynn Qi
 * @Date: 2021/5/25 15:34
 * @Version: V1.0
 */
@GspServiceBundle(applicationName = "runtime", serviceUnitName = "Lcm", serviceName = "noCodeServerService")
public interface NoCodeServerService {
    /**
     * 保存元数据
     *
     * @param metadataDto 元数据DTO
     * @return 保存成功后返回的元数据DTO
     */
    MetadataDto saveMetadata(MetadataDto metadataDto);

    /**
     * 根据元数据唯一标识获取元数据DTO
     *
     * @param metadataId 元数据id
     * @return 元数据DTO
     */
    MetadataDto getMetadata(String metadataId);

    /**
     * 将元数据从设计表中发布到运行表
     *
     * @param metadataId 元数据ID
     * @return 发布成功，返回true，发布失败，返回false
     **/
    boolean publishMetadata(String metadataId);

    /**
     * 删除元数据
     *
     * @param metadataId 元数据id
     */
    void deleteMetadata(String metadataId);

}
