/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.metadata.rtcustomization.servermanager.event;

import com.inspur.edp.lcm.metadata.spi.event.nodecode.NoCodeEventArgs;
import io.iec.edp.caf.commons.event.EventBroker;
import io.iec.edp.caf.commons.event.config.EventListenerSettings;

/**
 * 功能描述:
 *
 * @ClassName: NoCodeServiceEventBroker
 * @Author: Fynn Qi
 * @Date: 2021/5/26 14:34
 * @Version: V1.0
 */
public class NoCodeServiceEventBroker extends EventBroker {

    private NoCodeServiceEventManager eventManager;

    public NoCodeServiceEventBroker(EventListenerSettings settings){
        super(settings);
        this.eventManager =new NoCodeServiceEventManager();
        this.init();
    }

    @Override
    protected void onInit() {
        this.eventManagerCollection.add(eventManager);
    }

    public void fireMetadataBeforeSaveEvent(NoCodeEventArgs args){
        this.eventManager.fireMetadataBeforeSaveEvent(args);
    }

    public void fireMetadataAfterSaveEvent(NoCodeEventArgs args){
        this.eventManager.fireMetadataAfterSaveEvent(args);
    }

    public void fireMetadataBeforeDeleteEvent(NoCodeEventArgs args){
        this.eventManager.fireMetadataBeforeDeleteEvent(args);
    }

    public void fireMetadataAfterDeleteEvent(NoCodeEventArgs args){
        this.eventManager.fireMetadataAfterDeleteEvent(args);
    }
}
