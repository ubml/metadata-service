/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.metadata.rtcustomization.servermanager.repository;

import com.inspur.edp.metadata.rtcustomization.api.entity.DimensionExtendEntity;
import com.inspur.edp.metadata.rtcustomization.api.entity.GspMdExtRelation;
import com.inspur.edp.metadata.rtcustomization.servermanager.dac.CustomizationExtRelationRepo;
import lombok.var;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.UUID;

public class ExtRelationRepoService {
	private CustomizationExtRelationRepo extRelationRepo;
	public ExtRelationRepoService(CustomizationExtRelationRepo extRelationRepo){
		this.extRelationRepo=extRelationRepo;
	}
	/**
	 *根据扩展元数据信息（元数据id，证书，版本）获取基础元数据 与 扩展元数据扩展关系，仅有一条
	 * @param extMetadataId 扩展元数据id
	 * @param extMdCertId   扩展元数据证书
	 * @return GspMdExtRelation 元数据扩展关系
	 */
	public GspMdExtRelation getExtRelationByExtMdInfo(String extMetadataId, String extMdCertId){
		if (StringUtils.isEmpty(extMdCertId)){
			var relationList = extRelationRepo.findAllByExtMdId(extMetadataId);
			if (relationList != null && relationList.size() > 0){
				return relationList.get(0);
			}
			else{
				return null;
			}
		}
		else{
			var relationList = extRelationRepo.findAllByExtMdIdAndExtMdCertId(extMetadataId,extMdCertId);
			if (relationList != null && relationList.size() > 0){
				return relationList.get(0);
			}
			else{
				return null;
			}
		}

	}

	public GspMdExtRelation getExtRelationByExtMdIdAndCertId(String extMetadataId, String extMdCertId) {
		List<GspMdExtRelation> relations =  null;
		if (StringUtils.isEmpty(extMdCertId)){
			relations = extRelationRepo.findAllByExtMdId(extMetadataId);
		}
		else{
			relations=extRelationRepo.findAllByExtMdIdAndExtMdCertId(extMetadataId, extMdCertId);
		}
		if(relations == null || relations.size() == 0){
			return null;
		}
		return  relations.get(0);
	}

	public List<GspMdExtRelation> getExtRelationByExtNameSpaceAndCode(String extMetadataNameSpace, String extMdCode) {
		return extRelationRepo.findAllByExtMdNameSpaceAndAndExtMdCode(extMetadataNameSpace, extMdCode);
	}
	/**
	 * 根据基础元数据信息（元数据id，证书，版本）获取基础元数据 与 扩展元数据扩展关系，有多条（维度值有多个）
	 * @param basicMetadataId   基础元数据id
	 * @param basicMdCertId 基础元数据证书id
	 * @return List<GspMdExtRelation>   元数据扩展关系列表
	 */
	public List<GspMdExtRelation> getExtRelationByBasicMdInfo(String basicMetadataId, String basicMdCertId) {
		if (StringUtils.isEmpty(basicMdCertId)){
			return extRelationRepo.findAllByBasicMdId(basicMetadataId);
		}
		return extRelationRepo.findAllByBasicMdIdAndBasicMdCertId(basicMetadataId, basicMdCertId);
	}

	public List<GspMdExtRelation> getExtRelationByBasicMdIdAndCertId(String basicMetadataId, String basicMdCertId) {
		if (StringUtils.isEmpty(basicMdCertId)){
			return extRelationRepo.findAllByBasicMdId(basicMetadataId);
		}
		return extRelationRepo.findAllByBasicMdIdAndBasicMdCertId(basicMetadataId, basicMdCertId);
	}
	public List<GspMdExtRelation> getExtRelationByExtMdType(String extMdType) {
		return extRelationRepo.findAllByExtMdType(extMdType);
	}

	public void save(GspMdExtRelation extRelation){
		String extMdId = extRelation.getExtMdId();
		String extCertId = extRelation.getExtMdCertId();
		List<GspMdExtRelation> existRelations = null;
		GspMdExtRelation existOneRelation = null;
		if (StringUtils.isEmpty(extCertId)){
			existRelations = extRelationRepo.findAllByExtMdId(extMdId);
		}
		else{
			existRelations = extRelationRepo.findAllByExtMdIdAndExtMdCertId(extMdId, extCertId);
		}
		if (existRelations != null && existRelations.size() > 0){
			existOneRelation = existRelations.get(0);
		}
		if (existOneRelation != null){
			extRelation.setId(existOneRelation.getId());
		}
		extRelationRepo.save(extRelation);
	}

	public void deleteExtRelationByMdIdAndCertId(String extMetadataId, String extMdCertId) {
		List<GspMdExtRelation> relations = null;
		if (StringUtils.isEmpty(extMdCertId)){
			relations = extRelationRepo.findAllByExtMdId(extMetadataId);
		}
		else{
			relations = extRelationRepo.findAllByExtMdIdAndExtMdCertId(extMetadataId, extMdCertId);
		}

		if(relations == null)
		{
			return;
		}
		relations.forEach(item -> {
		    //extRelationRepo.deleteByExtMdIdAndExtMdCertIdAndExtMdVersion(extMetadataId, extMdCertId, item.getExtMdVersion());
			extRelationRepo.deleteById(item.getId());
		});
	}

	public void deleteExtRelation(String extMetadataId, String extMdCertId) {
		if (StringUtils.isEmpty(extMdCertId)){
			extRelationRepo.deleteByExtMdId(extMetadataId);
		}
		extRelationRepo.deleteByExtMdIdAndExtMdCertId(extMetadataId, extMdCertId);
	}
	public List<GspMdExtRelation> findAllByBasicMdIdAndBasicMdCertIdAndFirstDimValueAndSecDimValue(String basicMetdataID, String basicMetadataCertId, String firstDimension, String secondDimension){
		return extRelationRepo.findAllByBasicMdIdAndBasicMdCertIdAndFirstDimValueAndSecDimValue(basicMetdataID, basicMetadataCertId, firstDimension, secondDimension);
	}

	public void saveMetadataExtRelation(DimensionExtendEntity dimExtendEntity) {
		String extMdId = dimExtendEntity.getExtendMetadataEntity().getHeader().getId();
		String extCertId = dimExtendEntity.getExtendMetadataEntity().getHeader().getCertId();
		List<GspMdExtRelation> existRelations = null;
		GspMdExtRelation existOneRelation = null;
		if (StringUtils.isEmpty(extCertId)){
			existRelations = extRelationRepo.findAllByExtMdId(extMdId);
		}
		else{
			existRelations = extRelationRepo.findAllByExtMdIdAndExtMdCertId(extMdId, extCertId);
		}
		if (existRelations != null && existRelations.size() > 0){
			existOneRelation = existRelations.get(0);
		}
		GspMdExtRelation relation = buildMetadataExtRelation(dimExtendEntity);
		if (existOneRelation != null){
			relation.setId(existOneRelation.getId());
		}

		extRelationRepo.save(relation);
	}

	public GspMdExtRelation getRelationByDims(String basicMetdataID, String basicMetadataCertId, String firstDimension, String secondDimension) {
		List<GspMdExtRelation> relations = null;
		if (StringUtils.isEmpty(basicMetadataCertId)) {
			relations = extRelationRepo.findAllByBasicMdIdAndFirstDimValueAndSecDimValue(basicMetdataID, firstDimension, secondDimension);
		}
		else {
			relations = findAllByBasicMdIdAndBasicMdCertIdAndFirstDimValueAndSecDimValue(basicMetdataID, basicMetadataCertId, firstDimension, secondDimension);
		}
		if(relations == null || relations.size() == 0){
			return null;
		}
//		if (relations.size() == 1){
//			return relations.get(0);
//		}
//
//		relations.sort((o1, o2) -> o1.getBasicMdVersion().compareToIgnoreCase(o2.getBasicMdVersion()));

		return relations.get(0);
	}
	private GspMdExtRelation buildMetadataExtRelation(DimensionExtendEntity dimExtendEntity) {
		GspMdExtRelation relation = new GspMdExtRelation();
		relation.setId(UUID.randomUUID().toString());
		relation.setExtMdId(dimExtendEntity.getExtendMetadataEntity().getHeader().getId());
		relation.setExtMdCertId(dimExtendEntity.getExtendMetadataEntity().getHeader().getCertId());
		relation.setExtMdVersion(dimExtendEntity.getExtendMetadataEntity().getVersion());
		relation.setExtMdNameSpace(dimExtendEntity.getExtendMetadataEntity().getHeader().getNameSpace());
		relation.setExtMdCode(dimExtendEntity.getExtendMetadataEntity().getHeader().getCode());
		relation.setExtMdType(dimExtendEntity.getExtendMetadataEntity().getHeader().getType());
		relation.setBasicMdId(dimExtendEntity.getBasicMetadataId());
		relation.setBasicMdCertId(dimExtendEntity.getBasicMetadataCertId());
		relation.setBasicMdVersion(dimExtendEntity.getBasicMetadataVersion());
		relation.setBasicMdNameSpace(dimExtendEntity.getBasicMetadataNamespace());
		relation.setBasicMdCode(dimExtendEntity.getBasicMetadataCode());
		relation.setBasicMdType(dimExtendEntity.getBasicMetadataTypeStr());
		relation.setFirstDimValue(dimExtendEntity.getFirstDimension());
		relation.setSecDimValue(dimExtendEntity.getSecondDimension());
		relation.setFirstDimCode(dimExtendEntity.getFirstDimensionCode());
		relation.setFirstDimName(dimExtendEntity.getFirstDimensionName());
		relation.setSecDimCode(dimExtendEntity.getSecondDimensionCode());
		relation.setSecDimName(dimExtendEntity.getSecondDimensionName());
		return  relation;
	}
}
