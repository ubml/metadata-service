/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.metadata.rtcustomization.servermanager.dac;

import com.inspur.edp.metadata.rtcustomization.api.entity.GspMdExtRelation;
import io.iec.edp.caf.data.orm.DataRepository;

import java.util.List;

public interface CustomizationExtRelationRepo extends DataRepository<GspMdExtRelation,String> {
    List<GspMdExtRelation> findAllByExtMdIdAndExtMdCertId(String extMetadataId, String extMdCertId);
    List<GspMdExtRelation> findAllByBasicMdIdAndBasicMdCertId(String basicMetadataId, String basicMdCertId);

    List<GspMdExtRelation> findAllByBasicMdIdAndBasicMdCertIdAndFirstDimValueAndSecDimValue(String basicMetadataId, String basicMdCertId, String firstDimValue, String secondDimValue);
    List<GspMdExtRelation> findAllByBasicMdIdAndFirstDimValueAndSecDimValue(String basicMetdataID, String firstDimension, String secondDimension);

    List<GspMdExtRelation> findAllByBasicMdId(String basicMetadataId);
    List<GspMdExtRelation> findAllByExtMdId(String extMdId);
    List<GspMdExtRelation> findAllByExtMdNameSpaceAndAndExtMdCode(String extMdNameSpace,String extMdCode);
    List<GspMdExtRelation> findAllByExtMdType(String extMdType);

    void deleteByExtMdIdAndExtMdCertId(String extMetadataId, String extMdCertId);
    void deleteByExtMdId(String extMetadataId);
}
