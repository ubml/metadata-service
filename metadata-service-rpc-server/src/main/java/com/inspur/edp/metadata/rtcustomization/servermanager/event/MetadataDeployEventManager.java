/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.metadata.rtcustomization.servermanager.event;

import com.inspur.edp.metadata.rtcustomization.api.exception.ErrorCodes;
import com.inspur.edp.metadata.rtcustomization.api.exception.LcmEventException;
import com.inspur.edp.metadata.rtcustomization.spi.event.IMetadataDeployEventListener;
import io.iec.edp.caf.commons.event.CAFEventArgs;
import io.iec.edp.caf.commons.event.EventManager;
import io.iec.edp.caf.commons.event.IEventListener;

public class MetadataDeployEventManager extends EventManager {

	/**
	 * 内部枚举类
	 */
	enum MetadataDeployEventType {
		fireMdPkgDeployedEvent,
		fireExtMdSavedEvent
	}

	public void fireMdPkgDeployedEvent(CAFEventArgs e){
		this.fire(MetadataDeployEventType.fireMdPkgDeployedEvent,e);
	}

	public void fireExtMdSavedEvent(CAFEventArgs e){
		this.fire(MetadataDeployEventType.fireExtMdSavedEvent,e);
	}



	/**
	 * 实现抽象方法getCode(),获取事件管理对象的编码
	 * @return 事件管理对象的编码
	 */
	@Override
	public String getEventManagerName() {
		return "MetadataDeployEventManager";
	}

	/**
	 * 当前的事件管理者是否处理输入的事件监听者对象
	 * @param listener 事件监听者
	 * @return 若处理，则返回true,否则返回false
	 */
	@Override
	public boolean isHandlerListener(IEventListener listener) {
		return listener instanceof IMetadataDeployEventListener;
	}

	/**
	 * 注册事件
	 * @param listener 监听者
	 */
	@Override
	public void addListener(IEventListener listener) {
		if(listener instanceof IMetadataDeployEventListener ==false){
			throw new LcmEventException(ErrorCodes.ECP_EVENT_0001, listener.getClass().getName(), IMetadataDeployEventListener.class.getName());
		}
		IMetadataDeployEventListener metadataDeployEventListener=(IMetadataDeployEventListener)listener;
		this.addEventHandler(MetadataDeployEventType.fireMdPkgDeployedEvent,metadataDeployEventListener,"fireMdPkgDeployedEvent");
		this.addEventHandler(MetadataDeployEventType.fireExtMdSavedEvent,metadataDeployEventListener,"fireExtMdSavedEvent");
	}

	/***
	 * 注销事件
	 * @param listener  监听者
	 */
	@Override
	public void removeListener(IEventListener listener) {
		if(listener instanceof IMetadataDeployEventListener==false){
			throw new LcmEventException(ErrorCodes.ECP_EVENT_0001, listener.getClass().getName(), IMetadataDeployEventListener.class.getName());
		}
		IMetadataDeployEventListener metadataDeployEventListener=(IMetadataDeployEventListener)listener;
		this.removeEventHandler(MetadataDeployEventType.fireMdPkgDeployedEvent,metadataDeployEventListener,"fireMdPkgDeployedEvent");
		this.removeEventHandler(MetadataDeployEventType.fireExtMdSavedEvent,metadataDeployEventListener,"fireExtMdSavedEvent");
	}
}
