/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.metadata.rtcustomization.servermanager.event;

import com.inspur.edp.lcm.metadata.spi.event.MdPkgChangedEventListener;
import com.inspur.edp.metadata.rtcustomization.api.exception.ErrorCodes;
import com.inspur.edp.metadata.rtcustomization.api.exception.LcmEventException;
import io.iec.edp.caf.commons.event.CAFEventArgs;
import io.iec.edp.caf.commons.event.EventManager;
import io.iec.edp.caf.commons.event.IEventListener;

/**
 * 元数据包变更监听mangaer
 * @author zhaoleitr
 */
public class MdPkgChangedEventManager extends EventManager {

	/**
	 * 内部枚举类
	 */
	enum MetadataRtEventType{
		fireMdPkgAddedEvent,
		fireMdPkgChangedEvent
	}

	@Override
	public String getEventManagerName() {
		return "MdpkgChangedEventManager";
	}

	@Override
	public boolean isHandlerListener(IEventListener iEventListener) {
		return iEventListener instanceof MdPkgChangedEventListener;
	}

	@Override
	public void addListener(IEventListener iEventListener) {
		if(iEventListener instanceof MdPkgChangedEventListener==false){
			throw new LcmEventException(ErrorCodes.ECP_EVENT_0001, iEventListener.getClass().getName(), MdPkgChangedEventListener.class.getName());
		}
		MdPkgChangedEventListener mdPkgChangedEventListener=(MdPkgChangedEventListener)iEventListener;
		this.addEventHandler(MetadataRtEventType.fireMdPkgAddedEvent,mdPkgChangedEventListener,"fireMdPkgAddedEvent");
		this.addEventHandler(MetadataRtEventType.fireMdPkgChangedEvent,mdPkgChangedEventListener,"fireMdPkgChangedEvent");
	}

	@Override
	public void removeListener(IEventListener iEventListener) {
		if(iEventListener instanceof MdPkgChangedEventListener==false){
			throw new LcmEventException(ErrorCodes.ECP_EVENT_0001, iEventListener.getClass().getName(), MdPkgChangedEventListener.class.getName());
		}
		MdPkgChangedEventListener mdPkgChangedEventListener=(MdPkgChangedEventListener)iEventListener;
		this.removeEventHandler(MetadataRtEventType.fireMdPkgAddedEvent,mdPkgChangedEventListener,"fireMdPkgAddedEvent");
		this.removeEventHandler(MetadataRtEventType.fireMdPkgChangedEvent,mdPkgChangedEventListener,"fireMdPkgChangedEvent");

	}

	public void fireMdPkgAddedEvent(CAFEventArgs e){
		this.fire(MetadataRtEventType.fireMdPkgAddedEvent,e);
	}

	public void fireMdPkgChangedEvent(CAFEventArgs e){
		this.fire(MetadataRtEventType.fireMdPkgChangedEvent,e);
	}
}
