/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.metadata.rtcustomization.servermanager.dac;

import com.inspur.edp.metadata.rtcustomization.api.entity.GspEmbeddedMdRtContent;
import com.inspur.edp.metadata.rtcustomization.api.entity.GspMdRtContent;
import com.inspur.edp.metadata.rtcustomization.api.entity.GspMdRtContentSuInfo;
import io.iec.caf.data.jpa.repository.CafJpaRepository;

import java.time.LocalDateTime;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MetadataRtContentRepository extends CafJpaRepository<GspEmbeddedMdRtContent, String> {
    GspEmbeddedMdRtContent findByMetadataId(String metadataId);

    GspEmbeddedMdRtContent findByMetadataIdAndSourceType(String metadataId, int sourceType);

    void deleteByMetadataId(String metadataId);

    void deleteByMdpkgId(String mdpkgId);

    // 不查询content字段
    @Query("select new com.inspur.edp.metadata.rtcustomization.api.entity.GspEmbeddedMdRtContent(g.id, g.metadataId, g.version, g.certId, g.previousVersion,g.code, g.name, g.type, g.bizObjId, g.createBy, g.createdOn,g.lastChangedBy, g.lastChangedOn, g.extended, g.nameSpace,g.extendProperty, g.sourceType, g.mdpkgId, g.language, g.translating,g.extendable,g.properties, g.filename) " +
        "from GspEmbeddedMdRtContent g " +
        "where " +
        "(g.type in :metadataTypes or 0 = :typeSize) " +
        "and (g.bizObjId in :businessObjectIds or 0 = :objSize) " +
        "and (:metadataNamespace is null or g.nameSpace = :metadataNamespace)" +
        "and (:metadataCode is null or g.code = :metadataCode)" +
        "and (:sourceType is null or g.sourceType = :sourceType)" +
        "and (:mdpkgId is null or g.mdpkgId = :mdpkgId)")
    List<GspEmbeddedMdRtContent> findAllGspMdRtContents(@Param("metadataTypes")List<String> metadataTypes, @Param("typeSize")int typeSize,
        @Param("businessObjectIds")List<String> businessObjectIds, @Param("objSize")int objSize,
        @Param("metadataNamespace")String metadataNamespace,
        @Param("metadataCode")String metadataCode,
        @Param("sourceType")Integer sourceType,
        @Param("mdpkgId")String mdpkgId);

    @Query("select new com.inspur.edp.metadata.rtcustomization.api.entity.GspEmbeddedMdRtContent(g.metadataId, g.code, g.name, g.type, g.nameSpace, g.mdpkgId, g.bizObjId) " +
            "from GspEmbeddedMdRtContent g " +
            "where " +
            "(g.type in :metadataTypes or 0 = :typeSize) " +
            "and (:sourceType is null or g.sourceType = :sourceType)")
    List<GspEmbeddedMdRtContent> findAllSimpleGspMdRtContents(@Param("metadataTypes")List<String> metadataTypes, @Param("typeSize")int typeSize, @Param("sourceType")Integer sourceType);

    @Query("select new com.inspur.edp.metadata.rtcustomization.api.entity.GspMdRtContentSuInfo(m.metadataId, p.appcode, p.serviceunitcode) " +
        "from GspEmbeddedMdRtContent m " +
        "join GspMdpkg p " +
        "on m.mdpkgId=p.id")
    List<GspMdRtContentSuInfo> findAllGspMdRtContentSuInfo();

    @Query("select new com.inspur.edp.metadata.rtcustomization.api.entity.GspMdRtContent(g.metadataId, min(g.lastChangedOn)) from GspEmbeddedMdRtContent g group by g.metadataId having count(g.metadataId) > 1")
    List<GspMdRtContent> findAllGroupByMetadataIdHavingCountGreaterThanOne();

    List<GspEmbeddedMdRtContent> findAllByMetadataIdAndLastChangedOn(String metadataId, LocalDateTime lastChangedOn);
}
