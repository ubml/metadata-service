/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.metadata.rtcustomization.servermanager.entity;

import com.inspur.edp.lcm.metadata.api.IMdExtRuleContent;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.metadata.rtcustomization.api.entity.DimensionExtendEntity;
import com.inspur.edp.metadata.rtcustomization.api.entity.GspMdChangeset;
import com.inspur.edp.metadata.rtcustomization.api.entity.GspMdExtRelation;
import lombok.Data;

import java.util.List;

@Data
public class MetadataSyncInfo {

	public MetadataSyncInfo(GspMetadata metadata){
		this.metadata=metadata;
		this.needSync=false;
	}

	public MetadataSyncInfo(GspMetadata metadata, GspMetadata oldMetadata, boolean needSync, List<DimensionExtendEntity> dimensionExtendEntityList, IMdExtRuleContent extentdRule){
		this.metadata=metadata;
		this.oldMetadata=oldMetadata;
		this.needSync=needSync;
		this.dimensionExtendEntityList=dimensionExtendEntityList;
	}

	private GspMetadata metadata;

	private GspMetadata oldMetadata;

	private boolean needSync;

	List<DimensionExtendEntity> dimensionExtendEntityList;

	private GspMetadata rootMetadata;

	private GspMdExtRelation extRelation;

	private GspMdChangeset changeSet;
}
