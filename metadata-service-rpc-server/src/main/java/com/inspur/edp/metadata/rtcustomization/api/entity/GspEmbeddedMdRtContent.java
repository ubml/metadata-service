/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.metadata.rtcustomization.api.entity;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.api.entity.MetadataHeader;
import com.inspur.edp.lcm.metadata.common.MetadataPropertyUtils;
import com.inspur.edp.lcm.metadata.common.ServiceUtils;
import com.inspur.edp.metadata.rtcustomization.api.exception.ErrorCodes;
import com.inspur.edp.metadata.rtcustomization.api.exception.LcmMetadataException;
import io.iec.edp.caf.data.multilang.CAFMultiLanguageColumn;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.UUID;

@Entity
@Table(name = "gspmdrtcontent")
@Data
public class GspEmbeddedMdRtContent {
    @Id
    private String id;
    private String metadataId;
    private String version;
    private String certId;
    private String previousVersion;
    private String code;

    @Embedded
    private CAFMultiLanguageColumn name;
    private String type;
    private String bizObjId;
    private String content;
    private String createBy;
    private LocalDateTime createdOn;
    private String lastChangedBy;
    private LocalDateTime lastChangedOn;
    private Boolean extended;
    private String nameSpace;
    private String extendProperty;
    private Integer sourceType;
    private String mdpkgId;

    @Column(name = "metadatalanguage")
    private String language;
    private Boolean translating;
    private Boolean extendable;
    @Column(name = "metadataproperties")
    private String properties;
    private String filename;
    @Transient
    private Map<String, String> nameLanguage;

    public GspEmbeddedMdRtContent() {}

    public GspEmbeddedMdRtContent(String metadataId, LocalDateTime lastChangedOn) {
        this.metadataId = metadataId;
        this.lastChangedOn = lastChangedOn;
    }

    public GspEmbeddedMdRtContent(String metadataId, String code, CAFMultiLanguageColumn name, String type, String nameSpace, String mdpkgId, String bizObjId) {
        this.metadataId = metadataId;
        this.code = code;
        this.name = name;
        this.type = type;
        this.nameSpace = nameSpace;
        this.mdpkgId = mdpkgId;
        this.bizObjId = bizObjId;
    }

    public GspEmbeddedMdRtContent(String id, String metadataId, String version, String certId, String previousVersion, String code, CAFMultiLanguageColumn name, String type, String bizObjId, String createBy, LocalDateTime createdOn, String lastChangedBy, LocalDateTime lastChangedOn, Boolean extended, String nameSpace, String extendProperty, Integer sourceType, String mdpkgId, String language, Boolean translating, Boolean extendable, String properties, String filename) {
        this.id = id;
        this.metadataId = metadataId;
        this.version = version;
        this.certId = certId;
        this.previousVersion = previousVersion;
        this.code = code;
        this.name = name;
        this.type = type;
        this.bizObjId = bizObjId;
        this.createBy = createBy;
        this.createdOn = createdOn;
        this.lastChangedBy = lastChangedBy;
        this.lastChangedOn = lastChangedOn;
        this.extended = extended;
        this.nameSpace = nameSpace;
        this.extendProperty = extendProperty;
        this.sourceType = sourceType;
        this.mdpkgId = mdpkgId;
        this.language = language;
        this.translating = translating;
        this.extendable = extendable;
        this.properties = properties;
        this.filename = filename;
    }

    public GspEmbeddedMdRtContent(String metadataId, String userName) {
        this.id = UUID.randomUUID().toString();
        this.metadataId = metadataId;
        this.createdOn = LocalDateTime.now();
        this.createBy = userName;
    }

    public GspEmbeddedMdRtContent(Integer sourceType) {
        this.sourceType = sourceType;
    }
    public GspEmbeddedMdRtContent(GspMdRtContent mdRtContent) {
        this.id = mdRtContent.getId();
        this.metadataId = mdRtContent.getMetadataId();
        this.version = mdRtContent.getVersion();
        this.certId = mdRtContent.getCertId();
        this.previousVersion = mdRtContent.getPreviousVersion();
        this.code = mdRtContent.getCode();
        this.type = mdRtContent.getType();
        this.bizObjId = mdRtContent.getBizObjId();
        this.content = mdRtContent.getContent();
        this.createBy = mdRtContent.getCreateBy();
        this.createdOn = mdRtContent.getCreatedOn();
        this.lastChangedBy = mdRtContent.getLastChangedBy();
        this.lastChangedOn = mdRtContent.getLastChangedOn();
        this.extended = mdRtContent.getExtended();
        this.nameSpace = mdRtContent.getNameSpace();
        this.extendProperty = mdRtContent.getExtendProperty();
        this.sourceType = mdRtContent.getSourceType();
        this.mdpkgId = mdRtContent.getMdpkgId();
        this.language = mdRtContent.getLanguage();
        this.translating = mdRtContent.isTranslating();
        this.extendable = mdRtContent.isExtendable();
        this.properties = mdRtContent.getProperties();
        this.filename = mdRtContent.getFilename();
    }

}
