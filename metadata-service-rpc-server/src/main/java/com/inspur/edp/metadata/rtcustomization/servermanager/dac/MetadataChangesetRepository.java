/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.metadata.rtcustomization.servermanager.dac;

import com.inspur.edp.metadata.rtcustomization.api.entity.GspMdChangeset;
import io.iec.edp.caf.data.orm.DataRepository;


public interface MetadataChangesetRepository extends DataRepository<GspMdChangeset,String> {
    void deleteByMetadataId(String metadataId);
    GspMdChangeset findByMetadataId(String metadataId);
    GspMdChangeset findByMetadataIdAndVersion(String metadataId, String version);
    GspMdChangeset findByMetadataIdAndCertId(String metadataId, String certId);
    GspMdChangeset findByMetadataIdAndCertIdAndRelatedVersion(String metadataId, String certId, String relatedVersion);
}
