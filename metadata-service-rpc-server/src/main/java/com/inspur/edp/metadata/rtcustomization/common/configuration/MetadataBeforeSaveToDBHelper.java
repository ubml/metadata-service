/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.metadata.rtcustomization.common.configuration;

import com.inspur.edp.lcm.metadata.api.ConfigData.MetadataConfiguration;
import com.inspur.edp.lcm.metadata.common.configuration.MetadataConfigurationLoader;
import com.inspur.edp.metadata.rtcustomization.api.exception.LcmConfigResolveException;
import com.inspur.edp.metadata.rtcustomization.api.exception.ErrorCodes;
import com.inspur.edp.metadata.rtcustomization.spi.MetadataBeforeSaveToDBExtend;

import java.util.HashMap;
import java.util.Map;

/**
 * @Classname MetadataBeforeToDBHelper
 * @Description 元数据入库前扩展功能帮助
 * @Date 2021-6-22
 * @Created by sunhongfei
 * @Version 1.0
 */
public class MetadataBeforeSaveToDBHelper extends MetadataConfigurationLoader {

    private static MetadataBeforeSaveToDBHelper singleton = null;
    private Map<String, MetadataBeforeSaveToDBExtend> mapperCache = new HashMap<String,MetadataBeforeSaveToDBExtend>();

    public MetadataBeforeSaveToDBHelper() {
    }

    public static MetadataBeforeSaveToDBHelper getInstance() {
        if (singleton == null) {
            singleton = new MetadataBeforeSaveToDBHelper();
        }
        return singleton;
    }

    /**
     * @param typeName 元数据类型
     * @return com.inspur.edp.lcm.metadata.spi.MetadataBeforeSaveToDBExtend
     * @author sunhongfei
     * @description 返回元数据入库前执行扩展
     * @date 2021-6-22
     **/
    public MetadataBeforeSaveToDBExtend getManager(String typeName) {
        MetadataBeforeSaveToDBExtend manager = null;
        MetadataConfiguration data = getMetadataConfigurationData(typeName);
        if (data != null) {
            Class<?> cls;
            try {
                if (data.getBeforeSaveToDB() != null) {
                    String className = data.getBeforeSaveToDB().getName();
                    if(mapperCache.containsKey(className)) {
                        return mapperCache.get(className);
                    }
                    cls = Class.forName(data.getBeforeSaveToDB().getName());
                    manager = (MetadataBeforeSaveToDBExtend) cls.newInstance();
                    mapperCache.put(className,manager);
                }
            } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
                throw new LcmConfigResolveException(e, ErrorCodes.ECP_CONFIG_RESOLVE_0001, typeName);
            }
        }
        return manager;
    }
}
