/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.metadata.rtcustomization.serverapi;

import com.inspur.edp.lcm.metadata.api.entity.*;
import com.inspur.edp.lcm.metadata.api.entity.uri.MetadataURI;
import com.inspur.edp.metadata.rtcustomization.api.entity.*;
import io.iec.edp.caf.rpc.api.annotation.GspServiceBundle;
import io.iec.edp.caf.rpc.api.annotation.RpcParam;

import java.util.List;

/**
 * The interface Customization server service.
 *
 * @author zhaoleitr
 */
@GspServiceBundle(applicationName = "runtime", serviceUnitName = "Lcm", serviceName = "metadata-customization")
public interface CustomizationServerService {
    MetadataStringDto getMetadataStringDto(@RpcParam(paramName = "metadataId") String metadataId);

    /**
     * 根据维度值获取元数据信息
     *
     * @param basicMetadataId     基础元数据ID
     * @param basicMetadataCertId 基础元数据证书id
     * @param firstDimension      第一维度值
     * @param secondDimension     第二维度值
     * @return 元数据传输实体 metadataDto
     */
    MetadataDto getMetadataWithDimensions(@RpcParam(paramName = "basicMetadataId") String basicMetadataId, @RpcParam(paramName = "basicMetadataCertId") String basicMetadataCertId, @RpcParam(paramName = "firstDimension") String firstDimension, @RpcParam(paramName = "secondDimension") String secondDimension);

    /**
     * 根据跟元数据id和维度值获取元数据信息
     *
     * @param rootMetadataId      基础元数据ID
     * @param basicMetadataCertId 基础元数据证书id
     * @param firstDimension      第一维度值
     * @param secondDimension     第二维度值
     * @return 元数据传输实体 metadataDto
     */
    MetadataDto getMetadataWithRootMdIdAndDimensions(@RpcParam(paramName = "rootMetadataId") String rootMetadataId, @RpcParam(paramName = "basicMetadataCertId") String basicMetadataCertId, @RpcParam(paramName = "firstDimension") String firstDimension, @RpcParam(paramName = "secondDimension") String secondDimension);

    /**
     * 保存扩展维度上的元数据
     *
     * @param dimExtEntityDto 根据维度扩展的元数据传输实体
     * @return boolean 保存是否成功
     */
    boolean saveExtMetadataWithDimensions(@RpcParam(paramName = "dimExtEntityDto") DimensionExtendEntityDto dimExtEntityDto);

    /**
     * 根据扩展元数据获取被扩展的元数据
     *
     * @param metadataId 元数据id
     * @param certId     证书id
     * @return 元数据实体 MetadataDto
     */
    MetadataDto getParentMdByExtMdId(String metadataId, String certId);

    /**
     * 根据扩展元数据获取被扩展的元数据
     *
     * @param metadataId 元数据id
     * @param certId     证书id
     * @return 元数据实体
     */
    List<DimensionExtendEntity> getExtMdByBasicMdId(@RpcParam(paramName = "metadataId") String metadataId, @RpcParam(paramName = "certId") String certId);

    /**
     * 删除扩展的元数据
     *
     * @param metadataId 元数据id
     * @param certId     证书标识
     */
    void deleteExtMetadata(@RpcParam(paramName = "metadataId") String metadataId, @RpcParam(paramName = "certId") String certId);

    /**
     * 删除运行时生成的元数据
     *
     * @param metadataId 元数据ID
     */
    void deleteGeneratedMetadata(@RpcParam(paramName = "metadataId") String metadataId);

    /**
     * 判断某维度值下，是否已经存在扩展的元数据
     *
     * @param basicMetadataId     基础元数据ID
     * @param basicMetadataCertId 基础元数据证书id
     * @param firstDimension      第一维度值
     * @param secondDimension     第二维度值
     * @return 是否存在扩展元数据 ，存在返回true，不存在返回false
     */
    boolean isExistExtendObjectWithDims(@RpcParam(paramName = "basicMetadataId") String basicMetadataId, @RpcParam(paramName = "basicMetadataCertId") String basicMetadataCertId, @RpcParam(paramName = "firstDimension") String firstDimension, @RpcParam(paramName = "secondDimension") String secondDimension);

    /**
     * 获取所有定制的元数据信息
     *
     * @return 序列化的运行时定制元数据信息列表 all customized metadata info
     */
    List<Metadata4Ref> getAllCustomizedMetadataInfo();


    /**
     * 获取所有运行时生成的元数据信息
     *
     * @return 序列化的运行时生成元数据信息列表 all generated metadata info
     */
    List<Metadata4Ref> getAllGeneratedMetadataInfo();

    /**
     * 获取指定类型的运行时定制元数据
     *
     * @param metadataTypes 元数据类型列表
     * @return 根据元数据类型获取所有运行时生成的元数据信息列表 all customized metadata info with types
     */
    List<Metadata4Ref> getAllCustomizedMetadataInfoWithTypes(@RpcParam(paramName = "metadataTypes") String metadataTypes);

    /**
     * 获取指定类型的运行时生成的元数据
     *
     * @param metadataTypes 元数据类型列表
     * @return 根据元数据类型获取所有运行时生成的元数据信息列表
     */
    List<Metadata4Ref> getAllGeneratedMdInfoWithTypes(@RpcParam(paramName = "metadataTypes") String metadataTypes);

    /**
     * 获取指定类型的运行时生成的和元数据包中的元数据信息列表
     *
     * @param metadataTypes 元数据类型列表
     * @return 根据元数据类型获取所有运行时生成的和元数据包中的元数据信息列表
     */
    List<Metadata4Ref> getAllOriginMdInfoWithTypes(String metadataTypes);

    /**
     * @param filter 元数据过滤实体
     * @return 元数据信息列表
     */
    List<Metadata4Ref> getMetadataInfoByFilter(@RpcParam(paramName = "filter") MetadataRTFilter filter);

    /**
     * @return 元数据包中元数据信息列表
     */
    List<Metadata4Ref> getAllPackagedMetadataInfo();


    /**
     * @param metadataTypes 元数据类型列表
     * @return 元数据包中元数据信息列表
     */
    List<Metadata4Ref> getAllPackagedMetadataInfoWithTypes(@RpcParam(paramName = "metadataTypes") String metadataTypes);

    /**
     * @param basicMetadataID     基础元数据id
     * @param basicMetadataCertId 基础元数据证书id
     * @return 元数据扩展信息及扩展元数据信息传输实体列表
     */
    List<DimensionExtendEntity> getMetadataInfoListRecusively(@RpcParam(paramName = "basicMetadataID") String basicMetadataID, @RpcParam(paramName = "basicMetadataCertId") String basicMetadataCertId);

    /**
     * 将元数据从设计表中发布到运行表
     *
     * @param metadataId 元数据id
     * @param certId     证书id
     * @return 发布成功返回true，否则返回false
     **/
    boolean releaseMetadataToRt(@RpcParam(paramName = "metadataId") String metadataId, @RpcParam(paramName = "certId") String certId);

    /**
     * 将元数据从设计表中发布到运行表
     *
     * @param metadataId 元数据id
     * @param certId     证书id
     * @param sourceType 类型
     * @return 发布成功返回true，否则返回false
     **/
    boolean releaseMetadataToRtWithSourceType(@RpcParam(paramName = "metadataId") String metadataId, @RpcParam(paramName = "certId") String certId, @RpcParam(paramName = "sourceType") String sourceType);

    /**
     * 根据扩展元数据标识，获取扩展关系
     *
     * @param metadataId 元数据id
     * @param certId     证书id
     * @return 维度扩展关系
     */
    DimensionExtendRelation getDimExtRelationByExtMetadata(@RpcParam(paramName = "metadataId") String metadataId, @RpcParam(paramName = "certId") String certId);

    /**
     * 根据扩展元数据获取基础元数据,此基础元数据为 根元数据
     * be现在解析元数据，但是考虑产品元数据承载大部分逻辑，扩展内容是少量内容；
     * 还要获取基础元数据，通过基础元数据解析具体的代码逻辑，进行扩展内容合并执行
     *
     * @param metadataId 元数据Id
     * @param certId     证书Id
     * @return 元数据传输实体
     */
    MetadataDto getBasicMetadataByExtMdId(@RpcParam(paramName = "metadataId") String metadataId, @RpcParam(paramName = "certId") String certId);
    /**
     * 根据扩展元数据ID获取基础元数据ID,此基础元数据为 根元数据
     *
     * @param metadataId 元数据Id
     * @param certId     证书Id
     * @return 元数据传输实体
     */
    String getBasicMetadataIdByExtMdId(@RpcParam(paramName = "metadataId") String metadataId, @RpcParam(paramName = "certId") String certId);


    /**
     * 根据扩展元数据获取基础元数据,此基础元数据为 根元数据
     * be现在解析元数据，但是考虑产品元数据承载大部分逻辑，扩展内容是少量内容；
     * 还要获取基础元数据，通过基础元数据解析具体的代码逻辑，进行扩展内容合并执行
     *
     * @param metadataId 元数据Id
     * @param certId     证书Id
     * @return 元数据信息
     */
    Metadata4Ref getBasicPkgMetadataByExtMdId(@RpcParam(paramName = "metadataId") String metadataId, @RpcParam(paramName = "certId") String certId);

    /**
     * 根据元数据ID跟版本获取当前关联的增量
     *
     * @param metadataId 元数据id
     * @param certId     证书问题
     * @param version    版本
     * @return 元数据变更实体
     */
    GspMdChangeset getChangeset(@RpcParam(paramName = "metadataId") String metadataId, @RpcParam(paramName = "certId") String certId, @RpcParam(paramName = "version") String version);


    /**
     * 运行时动态保存元数据
     *
     * @param metadata 元数据传输实体
     */
    void save(@RpcParam(paramName = "metadata") MetadataDto metadata);

    void saveGeneratedMetadata(GspMetadata metadata);

    /**
     * 保存与检查
     *
     * @param dto
     */
    void saveCheck(@RpcParam(paramName = "dto") MetadataDto dto);

    /**
     * 根据元数据唯一标识获取元数据
     *
     * @param metadataId 元数据ID
     * @return 元数据传输实体
     */
    MetadataDto getMetadataDto(@RpcParam(paramName = "metadataId") String metadataId);


    /**
     * 根据元数据唯一标识获取元数据
     *
     * @param metadataId 元数据ID
     * @return 元数据实体
     */
    GspMetadata getMetadata(String metadataId);

    /**
     * 根据元数据唯一标识获取元数据，并存入缓存中
     *
     * @param metadataId 元数据ID
     * @return void
     */
    void saveMetadataToCache(String metadataId);

    /**
     * 获取运行时元数据列表，包含包中的跟定制生成的
     *
     * @param metadataTypes 元数据类型列表
     * @return 元数据信息列表
     */
    List<Metadata4Ref> getMetadataInfoListWithTypes(@RpcParam(paramName = "metadataTypes") String metadataTypes);

    /**
     * 获取全部元数据列表
     *
     * @return 序列化的元数据信息列表
     */
    List<Metadata4Ref> getMetadataList();

    /**
     * 根据特定条件获取元数据
     *
     * @param nameSpace 命名空间
     * @param code      编号
     * @param typeCode  元数据类型编号
     * @return 元数据传输实体
     */
    MetadataDto getMetadataBySemanticId(@RpcParam(paramName = "nameSpace") String nameSpace, @RpcParam(paramName = "code") String code, @RpcParam(paramName = "typeCode") String typeCode);


    /**
     * @param metadataId 元数据id
     * @param certId     证书id
     * @return
     */
    boolean isMdDeletable(@RpcParam(paramName = "metadataId") String metadataId, @RpcParam(paramName = "certId") String certId);

    GspMdExtRelation getExtRelationByExtMdIdAndCertId(String extMetadataId, String extMdCertId);

    void saveExtMetadata(GspMetadata metadata);

    /**
     * 根据元数据ID跟版本获取当前关联的增量
     *
     * @param metadataId 元数据id
     * @param certId     证书id
     * @return 运行时定制增量
     */
    GspMdChangeset getChangeset(String metadataId, String certId);

    void saveMetadataExtRelation(GspMdExtRelation extRelation);

    void saveMdChangeSet(GspMdChangeset changeSet);

    /**
     * 根据元数据编码列表删除其缓存
     *
     * @param metadataIds 待删除的元数据编码集合
     * @author sunhongfei01
     * @date 2021-7-20
     */
    void removeCacheByMetadataIds(List<String> metadataIds);

    void afterMetadataChanged();

    void initMetadataCache();

    void clearAllCache();

    /**
     * 根据产品元数据id与维度值动态获取存在的扩展元数据（根据父元数据、两个纬度值获取扩展元数据）
     * 如果不存在相应维度值的扩展，根据扩展关系获取基础维度的扩展元数据，直至获取对应的产品元数据
     *
     * @param rootMetadataId      根元数据ID
     * @param rootMetadataCertId  根元数据证书id
     * @param firstDimensionValue      第一维度值
     * @param secondDimensionValue     第二维度值
     * @return 元数据传输实体 metadataDto
     */
    MetadataDto getDynamicMetadataWithRootMdIdAndDimensions(@RpcParam(paramName = "rootMetadataId") String rootMetadataId, @RpcParam(paramName = "rootMetadataCertId") String rootMetadataCertId, @RpcParam(paramName = "firstDimensionValue") String firstDimensionValue, @RpcParam(paramName = "secondDimensionValue") String secondDimensionValue);

    /**
     * 根据用户指定的SourceTypeEnum获取元数据
     *
     * @param metadataTypes 元数据类型列表
     * @return 元数据信息列表
     */
    List<Metadata4Ref> getMetadata4Refs(@RpcParam(paramName = "metadataTypes") List<String> metadataTypes,@RpcParam(paramName = "sourceTypeEnumList") List<SourceTypeEnum> sourceTypeEnumList);

    /**
     * 根据查询参数获取元数据引用列表
     *
     * @param metadata4RefQueryParam
     * @return
     */
    Metadata4RefPageQueryResult getMetadata4RefsByQueryParam(@RpcParam(paramName = "metadata4RefQueryParam") Metadata4RefPageQueryParam metadata4RefQueryParam);

    /**
     * 根据来源和业务对象id查询运行时定制表数据
     *
     * @param sourceType 来源类型
     * @param bizObjectID 业务对象ID
     * @return 元数据信息列表
     */
    List<MetadataDto> getAllBySourceTypeAndBizObjectID(@RpcParam(paramName = "sourceType") Integer sourceType,@RpcParam(paramName = "bizObjectID") String bizObjectID);

    /**
     * 更新扩展关系
     * 使用场景：更新扩展关系中的维度值
     * 使用方式：更新指定的扩展关系维度值，需要构造（extMdId，extMdCode，extMdNamespace, extMdType, firstDimCode，firstDimValue,secDimCode,secDimValue)
     * extMdId唯一确定扩展关系，(extMdCode, exMdNamespace, exMdType)用于出异常时提示
     * (firstDimCode,firstDimValue,secDimCode,secDimValue)指定要更新的维度值，如果某个维度没有值，不需要构造，传空。
     * firstDimCode，secDimCode用于标识维度信息，建议构造。可以不构造，传空，会使用原关系中维度编号替换空值。
     */
    void updateExtRelation(DimensionExtendRelation extendRelation);
    /**
     * 根据id获取元数据引用(不带content)
     * @param metadataId
     * @return
     */
    Metadata4Ref getMetadata4RefById(@RpcParam(paramName = "metadataId")String metadataId);
    MetadataURI getRefMetadataURI(@RpcParam(paramName = "metadataId") String metadataId);

    /**
     * 构建资源元数据
     * @param metadata
     * @return
     */
    MetadataDto buildResourceMetadata(@RpcParam(paramName = "metadata")MetadataDto metadata);

    /**
     *  根据扩展元数据类型获取扩展关系
     * @param metadataType
     * @return
     */
    List<DimensionExtendRelation> getDimExtRelationListByExtMetadataType(@RpcParam(paramName = "metadataType")String metadataType);
}
