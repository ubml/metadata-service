/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.metadata.rtcustomization.api.entity;

import lombok.extern.slf4j.Slf4j;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;


/**
 * @author zhaoleitr
 */
@Entity
@Table
@Slf4j
public class GspMdRefs implements Cloneable {
    @Id
    private String id;
    private String mdId;
    private String certId;
    private String mdNameSpace;
    private String mdCode;
    private String mdType;
    private String refMdId;
    private String refMdCertId;
    private String refMdNameSpace;
    private String refMdCode;
    private String refMdType;

    public GspMdRefs() {
    }

    public GspMdRefs(String mdId, String certId, String mdNameSpace, String mdCode, String mdType) {
        this.id = UUID.randomUUID().toString();
        this.mdId = mdId;
        this.certId = certId;
        this.mdNameSpace = mdNameSpace;
        this.mdCode = mdCode;
        this.mdType = mdType;
    }

    public GspMdRefs(String id, String mdId, String certId, String mdNameSpace, String mdCode, String mdType, String refMdId, String refMdCertId, String refMdNameSpace, String refMdCode, String refMdType) {
        this.id = id;
        this.mdId = mdId;
        this.certId = certId;
        this.mdNameSpace = mdNameSpace;
        this.mdCode = mdCode;
        this.mdType = mdType;
        this.refMdId = refMdId;
        this.refMdCertId = refMdCertId;
        this.refMdNameSpace = refMdNameSpace;
        this.refMdCode = refMdCode;
        this.refMdType = refMdType;
    }

    public GspMdRefs(String id, String mdId, String mdCode, String refMdId) {
        this.id = id;
        this.mdId = mdId;
        this.mdCode = mdCode;
        this.refMdId = refMdId;
    }

    public void buildGspmdRefs(String refMdId, String refMdCertId, String refMdNameSpace, String refMdCode, String refMdType) {
        this.refMdId = refMdId;
        this.refMdCertId = refMdCertId;
        this.refMdNameSpace = refMdNameSpace;
        this.refMdCode = refMdCode;
        this.refMdType = refMdType;
    }

    @Override
    public GspMdRefs clone() {
        try {
            return (GspMdRefs) super.clone();
        } catch (CloneNotSupportedException e) {
            log.info("mdrefs不支持clone");
        }
        return new GspMdRefs(this.id, this.mdId, this.certId, this.mdNameSpace, this.mdCode, this.mdType, this.refMdId, this.refMdCertId, this.refMdNameSpace, this.refMdCode, this.refMdType);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMdId() {
        return mdId;
    }

    public void setMdId(String mdId) {
        this.mdId = mdId;
    }

    public String getCertId() {
        return certId;
    }

    public void setCertId(String certId) {
        this.certId = certId;
    }

    public String getMdNameSpace() {
        return mdNameSpace;
    }

    public void setMdNameSpace(String mdNameSpace) {
        this.mdNameSpace = mdNameSpace;
    }

    public String getMdCode() {
        return mdCode;
    }

    public void setMdCode(String mdCode) {
        this.mdCode = mdCode;
    }

    public String getMdType() {
        return mdType;
    }

    public void setMdType(String mdType) {
        this.mdType = mdType;
    }

    public String getRefMdId() {
        return refMdId;
    }

    public void setRefMdId(String refMdId) {
        this.refMdId = refMdId;
    }

    public String getRefMdCertId() {
        return refMdCertId;
    }

    public void setRefMdCertId(String refMdCertId) {
        this.refMdCertId = refMdCertId;
    }

    public String getRefMdNameSpace() {
        return refMdNameSpace;
    }

    public void setRefMdNameSpace(String refMdNameSpace) {
        this.refMdNameSpace = refMdNameSpace;
    }

    public String getRefMdCode() {
        return refMdCode;
    }

    public void setRefMdCode(String refMdCode) {
        this.refMdCode = refMdCode;
    }

    public String getRefMdType() {
        return refMdType;
    }

    public void setRefMdType(String refMdType) {
        this.refMdType = refMdType;
    }
}
