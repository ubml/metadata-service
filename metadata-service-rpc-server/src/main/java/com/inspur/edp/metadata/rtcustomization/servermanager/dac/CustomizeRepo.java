/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.metadata.rtcustomization.servermanager.dac;

import com.inspur.edp.metadata.rtcustomization.api.entity.GspMdContent;
import com.inspur.edp.metadata.rtcustomization.api.entity.GspMdContentData;
import io.iec.edp.caf.boot.context.CAFContext;
import io.iec.edp.caf.commons.dataaccess.DbType;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import lombok.var;
import org.springframework.beans.BeanUtils;
import org.springframework.util.CollectionUtils;

import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


/**
 * @author zhaoleitr
 */
public class CustomizeRepo {
    private CustomizationDataProducer cdp;
    public CustomizeRepo(CustomizationDataProducer cdp) {
        this.cdp = cdp;
    }

    private String sql;



    public List<GspMdContent> getAllCustomizedList() {
//        sql = "select metadataid,code,name,type,extended,bizobjid,certid,version,namespace from gspmdcontent";
//        return getAllMetadataFromDatabase(sql);
        MetadataContentRepository metadataContentRepository = SpringBeanUtils.getBean(MetadataContentRepository.class);
        List<GspMdContentData> gspMdContentDataList = metadataContentRepository.findAllGspMdContents();
        return getAllMetadataFromDatabase(gspMdContentDataList);
    }


    public List<GspMdContent> getCustomizedListByIdAndCertId(String id, String certid, String version) {
        if(certid==null)
        {
//            sql = "select metadataid,code,name,type,extended,bizobjid,certid,version,namespace from gspmdcontent where metadataid='" + id + "' and version='" + version+"'";
//            List<GspMdContent> result=getAllMetadataFromDatabase(sql);
            MetadataContentRepository metadataContentRepository = SpringBeanUtils.getBean(MetadataContentRepository.class);
            List<GspMdContentData> gspMdContentDataList = metadataContentRepository.findAllGspMdContents(id);
            List<GspMdContent> result = getAllMetadataFromDatabase(gspMdContentDataList);
            return result.stream().filter(item->item.getCertId()==null).collect(Collectors.toList());
        }
        else{
//            sql = "select metadataid,code,name,type,extended,bizobjid,certid,version,namespace from gspmdcontent where metadataid='" + id + "' and certid='" + certid + "' and version='" + version+"'";
//            return getAllMetadataFromDatabase(sql);
            MetadataContentRepository metadataContentRepository = SpringBeanUtils.getBean(MetadataContentRepository.class);
            List<GspMdContentData> gspMdContentDataList = metadataContentRepository.findAllGspMdContents(id, certid);
            return getAllMetadataFromDatabase(gspMdContentDataList);
        }
    }

    public List<GspMdContentData> findAllBySourcetypeAndBizObjID(Integer sourceType, String bizObjID) {
        MetadataContentRepository metadataContentRepository = SpringBeanUtils.getBean(MetadataContentRepository.class);
        return metadataContentRepository.findAllBySourceTypeAndBizObjId(sourceType, bizObjID);
    }



    private List<GspMdContent> getAllMetadataFromDatabase(String sql) {
        List<GspMdContent> metadataList = new ArrayList<>();
        Query query = cdp.getEm().createNativeQuery(sql);
        List<Object[]> result = query.getResultList();

        for(Object[] item : result){
            var metadata = new GspMdContent();
            metadata.setMetadataId((String) item[0]);
            metadata.setCode((String) item[1]);
            metadata.setName((String) item[2]);
            metadata.setType((String) item[3]);
            if(CAFContext.current.getDbType().equals(DbType.Oracle)) {
                Number extend = (Number)item[4];
                if(extend.equals(0))
                    metadata.setExtended(false);
                else if(extend.equals(1))
                    metadata.setExtended(true);
                else
                    metadata.setExtended(false);
            } else
                metadata.setExtended((boolean)item[4]);
            metadata.setBizObjId((String) item[5]);
            metadata.setCertId((String) item[6]);
            metadata.setVersion((String) item[7]);
            metadata.setNameSpace((String) item[8]);
            metadataList.add(metadata);
        }
        return metadataList;
    }
    private List<GspMdContent> getAllMetadataFromDatabase(List<GspMdContentData> gspMdContentDataList) {
        List<GspMdContent> metadataList = new ArrayList<>();
        if(CollectionUtils.isEmpty(gspMdContentDataList)){
            return metadataList;
        }
        for (GspMdContentData gspMdContentData : gspMdContentDataList) {
            var metadata = new GspMdContent();
            BeanUtils.copyProperties(gspMdContentData,metadata);
            metadataList.add(metadata);
        }
        return metadataList;
    }
}
