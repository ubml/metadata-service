/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.metadata.rtcustomization.servermanager.extend;

import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.metadata.rtcustomization.api.AbstractCustomizedContent;
import com.inspur.edp.metadata.rtcustomization.common.CheckerHelper;
import com.inspur.edp.metadata.rtcustomization.servermanager.utils.MetadataDeployUtils;
import com.inspur.edp.metadata.rtcustomization.spi.CustomizationExtChecker;
import com.inspur.edp.metadata.rtcustomization.spi.CustomizationExtHandler;
import com.inspur.edp.metadata.rtcustomization.spi.args.ChangeMergeArgs;
import com.inspur.edp.metadata.rtcustomization.spi.args.Compare4SameLevelArgs;
import com.inspur.edp.metadata.rtcustomization.spi.args.ConflictCheckArgs;
import com.inspur.edp.metadata.rtcustomization.spi.args.MetadataMergeArgs;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DeployStrategy {
	public static AbstractCustomizedContent getExtContent4SameLevel(GspMetadata metadata, GspMetadata oldMetadata, GspMetadata rootMetadata){
		CustomizationExtHandler manager = MetadataDeployUtils.getCustomizationExtHandler(metadata.getHeader().getType());
		if(manager==null){
			return null;
		}
		Compare4SameLevelArgs args=new Compare4SameLevelArgs();
		args.setRootMetadata(rootMetadata);
		args.setMetadata(metadata);
		args.setOldMetadata(oldMetadata);
		log.debug("正在调用类型："+manager.getClass().getName()+"的getExtContent4SameLevel()方法");
		return manager.getExtContent4SameLevel(args);
	}


	public static AbstractCustomizedContent changeMerge(String metadataType,AbstractCustomizedContent parentChange,AbstractCustomizedContent changeToParent, GspMetadata rootMetadata,GspMetadata parentMetadata,GspMetadata currentMetadata){
        //TODO add try catch record currentmetadata and changemergeresult
		CustomizationExtHandler manager = MetadataDeployUtils.getCustomizationExtHandler(metadataType);
		if(manager==null){
			return null;
		}
		ChangeMergeArgs args=new ChangeMergeArgs(parentChange,changeToParent,rootMetadata,parentMetadata,currentMetadata);
		log.debug("正在调用类型："+manager.getClass().getName()+"的changeMerge()方法");
		return manager.changeMerge(args);
	}

	public static void checkMergeConflict(String metadataType, AbstractCustomizedContent parentChange, AbstractCustomizedContent changeToParent,  GspMetadata rootMetadata) {
		CustomizationExtChecker checker = CheckerHelper.getInstance().getManager(metadataType);
		if (checker != null) {
			ConflictCheckArgs arg=new ConflictCheckArgs();
			arg.setChangToParent(changeToParent);
			arg.setParentChange(parentChange);
			arg.setRootMetadata(rootMetadata);
			log.debug("正在调用类型："+checker.getClass().getName()+"的checkMergeConflict()方法");
			checker.checkMergeConflict(arg);
		}
	}

	public static GspMetadata mergeMetadata(CustomizationExtHandler manager, GspMetadata rootMetadata, GspMetadata extendMetadata, AbstractCustomizedContent mergedChange) {
		//TODO add try catch give the rootmetadatainfo and extendmetadatainfo and record mergedchange
		MetadataMergeArgs mergeArgs=new MetadataMergeArgs();
		mergeArgs.setExtendMetadata(extendMetadata);
		mergeArgs.setCustomizedContent(mergedChange);
		mergeArgs.setRootMetadata(rootMetadata);
		log.debug("正在调用类型："+manager.getClass().getName()+"的merge()方法");
		return manager.merge(mergeArgs);
	}
}
