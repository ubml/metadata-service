/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.metadata.rtcustomization.common;

import com.inspur.edp.metadata.rtcustomization.api.entity.config.CustomizationConfiguration;
import com.inspur.edp.metadata.rtcustomization.api.exception.LcmConfigResolveException;
import com.inspur.edp.metadata.rtcustomization.api.exception.ErrorCodes;
import com.inspur.edp.metadata.rtcustomization.spi.CustomizationExtChecker;


/**
 * @author zhaoleitr
 */
public class CheckerHelper extends CustomizationConfigLoader {
    private static CheckerHelper singleton = null;

    public CheckerHelper(){}

    public static CheckerHelper getInstance(){
        if (singleton == null){
            singleton = new CheckerHelper();
        }
        return singleton;
    }


    public CustomizationExtChecker getManager(String typeName)  {
        CustomizationExtChecker manager = null;
        CustomizationConfiguration data = getCustomizationConfigurationData(typeName);
        if (data != null && data.getChecker() != null){
            Class<?> cls = null;
            try {
                cls = Class.forName(data.getChecker().getName());
                manager = (CustomizationExtChecker)cls.newInstance();
            } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
                throw new LcmConfigResolveException(e, ErrorCodes.ECP_CONFIG_RESOLVE_0001, typeName);
            }
        }
        return manager;
    }
}
