/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.metadata.rtcustomization.api.entity;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.api.entity.MetadataHeader;
import com.inspur.edp.lcm.metadata.common.MetadataPropertyUtils;
import com.inspur.edp.lcm.metadata.common.ServiceUtils;
import java.time.LocalDateTime;

import com.inspur.edp.lcm.metadata.common.context.RuntimeContext;
import com.inspur.edp.metadata.rtcustomization.api.exception.ErrorCodes;
import com.inspur.edp.metadata.rtcustomization.api.exception.LcmMetadataException;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class GspMdRtContent {
    private String id;
    private String metadataId;
    private String version;
    private String certId;
    private String previousVersion;
    private String code;

    private String name;
    private String type;
    private String bizObjId;
    private String content;
    private String createBy;
    private LocalDateTime createdOn;
    private String lastChangedBy;
    private LocalDateTime lastChangedOn;
    private Boolean extended;
    private String nameSpace;
    private String extendProperty;
    private Integer sourceType;
    private String mdpkgId;

    private String language;
    private Boolean translating;
    private Boolean extendable;
    private String properties;
    private String filename;
    private Map<String, String> nameLanguage;

    public GspMdRtContent() {}

    public GspMdRtContent(String metadataId, LocalDateTime lastChangedOn) {
        this.metadataId = metadataId;
        this.lastChangedOn = lastChangedOn;
    }
    public GspMdRtContent(GspEmbeddedMdRtContent embeddedMdRtContent) {
        this.id = embeddedMdRtContent.getId();
        this.metadataId = embeddedMdRtContent.getMetadataId();
        this.code = embeddedMdRtContent.getCode();
        if(embeddedMdRtContent.getName() != null) {
            this.name = embeddedMdRtContent.getName().getValue(RuntimeContext.getLanguage());
            // 兼容旧数据,默认获取中文
            if(!StringUtils.hasText(this.name)){
                this.name = embeddedMdRtContent.getName().getCafMlcchs();
            }
        }
        // 如果名称为空，则设置为空字符串，主要是为了处理null场景，前端显示会报错
        if(!StringUtils.hasText(this.name)) {
            this.name = "";
        }
        this.lastChangedOn = embeddedMdRtContent.getLastChangedOn();
        this.type = embeddedMdRtContent.getType();
        this.nameSpace = embeddedMdRtContent.getNameSpace();
        this.mdpkgId = embeddedMdRtContent.getMdpkgId();
        this.bizObjId = embeddedMdRtContent.getBizObjId();
        this.content = embeddedMdRtContent.getContent();
        this.extendable = embeddedMdRtContent.getExtendable();
        this.translating = embeddedMdRtContent.getTranslating();
        this.properties = embeddedMdRtContent.getProperties();
        this.filename = embeddedMdRtContent.getFilename();
        this.extendProperty = embeddedMdRtContent.getExtendProperty();
        this.sourceType = embeddedMdRtContent.getSourceType();
        this.language = embeddedMdRtContent.getLanguage();
        this.createdOn = embeddedMdRtContent.getCreatedOn();
        this.lastChangedBy = embeddedMdRtContent.getLastChangedBy();
        this.createBy = embeddedMdRtContent.getCreateBy();
        this.extended = embeddedMdRtContent.getExtended();
        this.version = embeddedMdRtContent.getVersion();
        this.previousVersion = embeddedMdRtContent.getPreviousVersion();
        this.certId = embeddedMdRtContent.getCertId();
        // todo nameLanguage字段需要单独调试
        this.nameLanguage = embeddedMdRtContent.getNameLanguage();
    }

    public GspMdRtContent(String metadataId, String code, String name, String type, String nameSpace, String mdpkgId, String bizObjId) {
        this.metadataId = metadataId;
        this.code = code;
        this.name = name;
        this.type = type;
        this.nameSpace = nameSpace;
        this.mdpkgId = mdpkgId;
        this.bizObjId = bizObjId;
    }

    public GspMdRtContent(String id, String metadataId, String version, String certId, String previousVersion, String code, String name, String type, String bizObjId, String createBy, LocalDateTime createdOn, String lastChangedBy, LocalDateTime lastChangedOn, Boolean extended, String nameSpace, String extendProperty, Integer sourceType, String mdpkgId, String language, Boolean translating, Boolean extendable, String properties, String filename) {
        this.id = id;
        this.metadataId = metadataId;
        this.version = version;
        this.certId = certId;
        this.previousVersion = previousVersion;
        this.code = code;
        this.name = name;
        this.type = type;
        this.bizObjId = bizObjId;
        this.createBy = createBy;
        this.createdOn = createdOn;
        this.lastChangedBy = lastChangedBy;
        this.lastChangedOn = lastChangedOn;
        this.extended = extended;
        this.nameSpace = nameSpace;
        this.extendProperty = extendProperty;
        this.sourceType = sourceType;
        this.mdpkgId = mdpkgId;
        this.language = language;
        this.translating = translating;
        this.extendable = extendable;
        this.properties = properties;
        this.filename = filename;
    }

    public GspMdRtContent(String metadataId, String userName) {
        this.id = UUID.randomUUID().toString();
        this.metadataId = metadataId;
        this.createdOn = LocalDateTime.now();
        this.createBy = userName;
    }

    public GspMdRtContent(Integer sourceType) {
        this.sourceType = sourceType;
    }

    public void update(GspMetadata metadata,String content,String lastChangedBy,Integer sourceType,String mdpkgId,String properties) {

        MetadataHeader header = metadata.getHeader();
        this.version = metadata.getVersion();
        this.previousVersion = metadata.getPreviousVersion();
        this.extendProperty = metadata.getExtendProperty();
        this.extended = metadata.isExtended();
        this.certId = header.getCertId();
        this.code = header.getCode();
        this.name = header.getName();
        this.type = header.getType();
        this.bizObjId = header.getBizobjectID();
        this.content = content;
        this.lastChangedBy = lastChangedBy;
        this.lastChangedOn = LocalDateTime.now();
        this.nameSpace = header.getNameSpace();
        this.sourceType = sourceType;
        this.mdpkgId = mdpkgId;
        this.language = header.getLanguage();
        this.translating = header.isTranslating();
        this.extendable = header.isExtendable();
        this.properties = properties;
        this.filename = header.getFileName();
        this.nameLanguage = header.getNameLanguage();
        if (CollectionUtils.isEmpty(this.nameLanguage)) {
            initNameLanguage();
        }
    }
    public void update(JsonNode mdJsonNode, String content, String lastChangedBy, Integer sourceType, String mdpkgId) {
        // 初始变量
        ObjectMapper mapper = ServiceUtils.getMapper();
        JsonNode headerNode = mdJsonNode.findValue(MetadataPropertyUtils.header);
        String metadataId = headerNode.findValue("ID").asText();
        MetadataHeader header;
        try {
            header = mapper.readValue(headerNode.toString(), MetadataHeader.class);
        } catch (JsonProcessingException e) {
            throw new LcmMetadataException(e,  ErrorCodes.ECP_METADATA_0001, metadataId);
        }

        JsonNode extendPropertyNode = mdJsonNode.findValue(MetadataPropertyUtils.extendProperty);
        this.extendProperty = (extendPropertyNode == null || extendPropertyNode.asText().equals("null")) ? "" : extendPropertyNode.asText();
        JsonNode extendedNode = mdJsonNode.findValue(MetadataPropertyUtils.extended);
        this.extended = extendedNode!=null && extendedNode.asBoolean();
        JsonNode versionNode = mdJsonNode.findValue(MetadataPropertyUtils.version);
        this.version = (versionNode == null || versionNode.asText().equals("null")) ? "" : versionNode.asText();
        JsonNode previousVersionNode = mdJsonNode.findValue(MetadataPropertyUtils.previousVersion);
        this.previousVersion = (previousVersionNode == null || previousVersionNode.asText().equals("null")) ? "" : previousVersionNode.asText();
        JsonNode propertiesNode = mdJsonNode.findValue(MetadataPropertyUtils.properties);
        this.properties = (propertiesNode == null || propertiesNode.asText().equals("null")) ? "" : propertiesNode.asText();
        this.certId = header.getCertId();
        this.code = header.getCode();
        this.name = header.getName();
        this.type = header.getType();
        this.bizObjId = header.getBizobjectID();
        this.content = content;
        this.lastChangedBy = lastChangedBy;
        this.lastChangedOn = LocalDateTime.now();
        this.nameSpace = header.getNameSpace();
        this.sourceType = sourceType;
        this.mdpkgId = mdpkgId;
        this.language = header.getLanguage();
        this.translating = header.isTranslating();
        this.extendable = header.isExtendable();
        this.filename = header.getFileName();
        this.nameLanguage = header.getNameLanguage();
        if (CollectionUtils.isEmpty(this.nameLanguage)) {
            initNameLanguage();
        }
    }

    /**
     * 初始化多语信息
     */
    public void initNameLanguage() {
        if (nameLanguage == null) {
            nameLanguage = new HashMap<>();
        }
        if (CollectionUtils.isEmpty(this.nameLanguage)) {
            nameLanguage.put(LanguageEnum.ZH_CHS.getCode(), getName());
        }
    }

    public Boolean isTranslating() {
        return translating != null &&translating;
    }

    public void setTranslating(Boolean translating) {
        this.translating = translating;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Boolean isExtendable() {
        return extendable != null && extendable;
    }

    public void setExtendable(Boolean extendable) {
        this.extendable = extendable;
    }

    public String getProperties() {
        return properties;
    }

    public void setProperties(String properties) {
        this.properties = properties;
    }

    public String getMdpkgId() {
        return mdpkgId;
    }

    public void setMdpkgId(String mdpkgId) {
        this.mdpkgId = mdpkgId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMetadataId() {
        return metadataId;
    }

    public void setMetadataId(String metadataId) {
        this.metadataId = metadataId;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getCertId() {
        return certId;
    }

    public void setCertId(String certId) {
        this.certId = certId;
    }

    public String getPreviousVersion() {
        return previousVersion;
    }

    public void setPreviousVersion(String previousVersion) {
        this.previousVersion = previousVersion;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        if(name == null){
            return "";
        }
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBizObjId() {
        return bizObjId;
    }

    public void setBizObjId(String bizObjId) {
        this.bizObjId = bizObjId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public LocalDateTime getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(LocalDateTime createdOn) {
        this.createdOn = createdOn;
    }

    public String getLastChangedBy() {
        return lastChangedBy;
    }

    public void setLastChangedBy(String lastChangedBy) {
        this.lastChangedBy = lastChangedBy;
    }

    public LocalDateTime getLastChangedOn() {
        return lastChangedOn;
    }

    public void setLastChangedOn(LocalDateTime lastChangedOn) {
        this.lastChangedOn = lastChangedOn;
    }

    public Boolean getExtended() {
        return extended != null && extended;
    }

    public void setExtended(Boolean extended) {
        this.extended = extended;
    }

    public Integer getSourceType() {
        return sourceType;
    }

    public void setSourceType(Integer sourceType) {
        this.sourceType = sourceType;
    }

    public String getNameSpace() {
        return nameSpace;
    }

    public void setNameSpace(String nameSpace) {
        this.nameSpace = nameSpace;
    }

    public String getExtendProperty() {
        return extendProperty;
    }

    public void setExtendProperty(String extendProperty) {
        this.extendProperty = extendProperty;
    }

    public Map<String, String> getNameLanguage() {
        if(CollectionUtils.isEmpty(nameLanguage)) {
            initNameLanguage();
        }
        return nameLanguage;
    }

    public void setNameLanguage(Map<String, String> nameLanguage) {
        this.nameLanguage = nameLanguage;
    }
}
