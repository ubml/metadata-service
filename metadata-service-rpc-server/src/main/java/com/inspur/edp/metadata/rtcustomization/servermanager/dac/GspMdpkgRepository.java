/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.metadata.rtcustomization.servermanager.dac;

import com.inspur.edp.metadata.rtcustomization.api.entity.GspMdpkg;
import io.iec.edp.caf.data.orm.DataRepository;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface GspMdpkgRepository extends DataRepository<GspMdpkg, String> {
    GspMdpkg findByName(String name);

    @Query("select new com.inspur.edp.metadata.rtcustomization.api.entity.GspMdpkg(g.id,g.name,g.version,g.processmode,g.location,g.appcode," +
        "g.serviceunitcode,g.createdBy,g.createdOn,g.lastChangedBy,g.lastChangedOn) from GspMdpkg g " +
        "where " +
        "(:name is null or g.name = :name) " +
        "and (g.processmode in :processModes or 0 = :processSize)"
    )
    List<GspMdpkg> findAllInfo(@Param("name") String name, @Param("processModes") List<String> processModes, @Param("processSize") int processSize);

    @Query(value = "select new com.inspur.edp.metadata.rtcustomization.api.entity.GspMdpkg(mdpkg.name,mdpkg.lastChangedOn) from GspMdpkg mdpkg")
    List<GspMdpkg> findAllLastChangedOn();

    @Query("select new com.inspur.edp.metadata.rtcustomization.api.entity.GspMdpkg(g.name,min(g.lastChangedOn)) from GspMdpkg g group by g.name having count(g.name) > 1")
    List<GspMdpkg> findAllGroupByNameHavingCountGreaterThanOne();

    List<GspMdpkg> findAllByNameAndLastChangedOn(String name, LocalDateTime lastChangedOn);

    @Query("select new com.inspur.edp.metadata.rtcustomization.api.entity.GspMdpkg(g.id, g.name, g.manifestInfo) from GspMdpkg g")
    List<GspMdpkg> findAllManifest();
}
