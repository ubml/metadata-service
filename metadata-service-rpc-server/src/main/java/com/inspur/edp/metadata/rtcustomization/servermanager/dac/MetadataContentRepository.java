/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.metadata.rtcustomization.servermanager.dac;

import com.inspur.edp.metadata.rtcustomization.api.entity.GspMdContent;
import com.inspur.edp.metadata.rtcustomization.api.entity.GspMdContentData;
import io.iec.edp.caf.data.orm.DataRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MetadataContentRepository extends DataRepository<GspMdContent,String> {
    //语法正确，从数据库能正常执行，程序中查不到结果@Query(value = "select m from GspMdContent m where m.metadataId = :metadataId and (:certId is null or :certId ='' or m.certId = :certId)")
    //语法错误 unexpected AST CODE@Query(value = "select m from GspMdContent m where m.metadataId = :metadataId and if(:certId != null or :certId !='' , m.certId = :certId, 1=1)")
    //不起作用 @Query(value = "select m from GspMdContent m where m.metadataId = :metadataId and m.certId like %:certId%")
    //语法正确，从数据库能正常执行，程序中查不到结果@Query(value = "select m from GspMdContent m where m.metadataId = :metadataId and (coalesce(:certId, null) is null or :certId ='' or m.certId = :certId)")
    //@Query(value = "select m from GspMdContent m where m.metadataId = :metadataId and  m.certId = :certId")
    GspMdContent findByMetadataIdAndCertId(String metadataId, String certId);
    GspMdContent findByMetadataId(String metadataId);
    void deleteByMetadataIdAndCertId(String metadataId, String certId);
    void deleteByMetadataId(String metadataId);

    @Query("select new com.inspur.edp.metadata.rtcustomization.api.entity.GspMdContentData(g.metadataId,g.version,g.certId,g.code,g.name,g.type,g.bizObjId,g.extended,g.nameSpace) from GspMdContent g")
    List<GspMdContentData> findAllGspMdContents();

//    @Query("select new com.inspur.edp.metadata.rtcustomization.api.entity.GspMdContentData(g.metadataId,g.version,g.certId,g.code,g.name,g.type,g.bizObjId,g.extended,g.nameSpace) from GspMdContent g where g.metadataId = ?1 and g.version = ?2")
    @Query("select new com.inspur.edp.metadata.rtcustomization.api.entity.GspMdContentData(g.metadataId,g.version,g.certId,g.code,g.name,g.type,g.bizObjId,g.extended,g.nameSpace) from GspMdContent g where g.metadataId = ?1 ")
    List<GspMdContentData> findAllGspMdContents(String metadataId);
//    @Query("select new com.inspur.edp.metadata.rtcustomization.api.entity.GspMdContentData(g.metadataId,g.version,g.certId,g.code,g.name,g.type,g.bizObjId,g.extended,g.nameSpace) from GspMdContent g where g.metadataId = ?1 and g.version = ?2 and g.certId = ?3")
    @Query("select new com.inspur.edp.metadata.rtcustomization.api.entity.GspMdContentData(g.metadataId,g.version,g.certId,g.code,g.name,g.type,g.bizObjId,g.extended,g.nameSpace) from GspMdContent g where g.metadataId = ?1  and g.certId = ?2")
    List<GspMdContentData> findAllGspMdContents(String metadataId,  String certId);

    @Query("select new com.inspur.edp.metadata.rtcustomization.api.entity.GspMdContent(g.metadataId,g.lastChangedOn) from GspMdContent g")
    List<GspMdContent> findAllLastChangedOn();

    @Query("select new com.inspur.edp.metadata.rtcustomization.api.entity.GspMdContentData(g.metadataId,g.version,g.certId,g.code,g.name,g.type,g.bizObjId,g.extended,g.nameSpace) from GspMdContent g where g.sourceType = ?1  and g.bizObjId = ?2")
    List<GspMdContentData> findAllBySourceTypeAndBizObjId(Integer sourceType, String bizObjId);
}
