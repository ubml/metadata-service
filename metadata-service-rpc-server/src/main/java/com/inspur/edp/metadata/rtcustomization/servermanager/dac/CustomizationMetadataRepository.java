/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.metadata.rtcustomization.servermanager.dac;

import com.inspur.edp.metadata.rtcustomization.api.entity.GspMdCustomContent;
import com.inspur.edp.metadata.rtcustomization.api.entity.GspMdCustomContentData;
import io.iec.edp.caf.data.orm.DataRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

public interface CustomizationMetadataRepository extends DataRepository<GspMdCustomContent, String> {
    GspMdCustomContent findAllById(String id);

    List<GspMdCustomContent> findAllByNameSpaceAndCodeAndType(String nameSpace, String code, String type);

    List<GspMdCustomContent> findAllByCodeAndType(String code, String type);

    @Query("select new com.inspur.edp.metadata.rtcustomization.api.entity.GspMdCustomContentData(g.id,g.code,g.name,g.type,g.bizObjId,g.isExtend,g.nameSpace) from GspMdCustomContent g")
    List<GspMdCustomContentData> findAllGspMdCustomContents();

    @Query("select new com.inspur.edp.metadata.rtcustomization.api.entity.GspMdCustomContentData(g.id,g.code,g.name,g.type,g.bizObjId,g.isExtend,g.nameSpace) from GspMdCustomContent g where g.id = ?1")
    List<GspMdCustomContentData> findAllGspMdCustomContents(String Id);

    @Query("select new com.inspur.edp.metadata.rtcustomization.api.entity.GspMdCustomContent(g.id,g.lastChangedOn) from GspMdCustomContent g")
    List<GspMdCustomContent> findAllLastChangedOn();

    @Transactional
    @Modifying
    @Query("update GspMdCustomContent g set g.lastChangedOn=?1 where g.lastChangedOn is null")
    void updateLastChangedOnWhereIsNull(LocalDateTime lastChangedOn);
}
