/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.metadata.rtcustomization.servermanager.configuration;

import com.inspur.edp.lcm.metadata.serverapi.NoCodeServerService;
import com.inspur.edp.metadata.rtcustomization.serverapi.CustomizationRtServerService;
import com.inspur.edp.metadata.rtcustomization.serverapi.CustomizationServerService;
import com.inspur.edp.metadata.rtcustomization.serverapi.MetadataDeployService;
import com.inspur.edp.metadata.rtcustomization.servermanager.CustomizationRtServerServiceImpl;
import com.inspur.edp.metadata.rtcustomization.servermanager.CustomizationServerServiceImpl;
import com.inspur.edp.metadata.rtcustomization.servermanager.MetadataDeployServiceImpl;
import com.inspur.edp.metadata.rtcustomization.servermanager.NoCodeServerServiceImpl;
import com.inspur.edp.metadata.rtcustomization.servermanager.dac.*;
import com.inspur.edp.metadata.rtcustomization.servermanager.event.MdPkgChangedEventBroker;
import com.inspur.edp.metadata.rtcustomization.servermanager.event.MetadataDeployEventBroker;
import com.inspur.edp.metadata.rtcustomization.servermanager.event.NoCodeServiceEventBroker;
import io.iec.caf.data.jpa.repository.config.EnableCafJpaRepositories;
import io.iec.edp.caf.commons.event.config.EventListenerSettings;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author zhaoleitr
 */
@Configuration(proxyBeanMethods = false)
@EnableCafJpaRepositories("com.inspur.edp.metadata.rtcustomization.servermanager.dac")
@EntityScan("com.inspur.edp.metadata.rtcustomization.api.entity")
public class ServiceConfiguration {

    @Bean
    public NoCodeServerService noCodeServerService(CustomizationServerService serverService,
                                                   NoCodeServiceEventBroker broker,
                                                   MetadataContentRepository mdContentRepo,
                                                   MetadataRtContentRepository mdRtContentRepo,
                                                   CustomizationMetadataRefsRepository metadataRefsRepository,
                                                   MetadataContentHisRepository mdContentHisRepo,
                                                   CustomizationDataProducer cdp) {
        return new NoCodeServerServiceImpl(serverService, broker, mdContentRepo, mdRtContentRepo, metadataRefsRepository, mdContentHisRepo, cdp);
    }

    @Bean
    public NoCodeServiceEventBroker noCodeServiceEventBroker(EventListenerSettings settings) {
        return new NoCodeServiceEventBroker(settings);
    }

    @Bean
    public CustomizationRtServerService createCustomizationRtServerService(CustomizationMetadataRepository metadataRepository, CustomizationMetadataRefsRepository metadataRefsRepository,
                                                                           MetadataRtContentRepository mdRtContentRepo, CustomizationExtRelationRepo extRelationRepo, CustomizationDataProducer cdp, GspMdpkgRepository gspMdpkgRepository) {
        return new CustomizationRtServerServiceImpl(metadataRepository, metadataRefsRepository, mdRtContentRepo, extRelationRepo, cdp, gspMdpkgRepository);
    }

    @Bean
    public CustomizationDataProducer createCustomizationServerDataProducer() {
        return new CustomizationDataProducer();
    }

    @Bean
    public CustomizationServerService createCustomizationServerService(CustomizationMetadataRepository metadataRepository, CustomizationMetadataRefsRepository metadataRefsRepository,
                                                                       CustomizationExtRelationRepo extRelationRepo, MetadataContentRepository mdContentRepo, MetadataContentHisRepository mdContentHisRepo,
                                                                       MetadataRtContentRepository mdRtContentRepo, MetadataChangesetRepository changesetRepo, MetadataChangesetRepositoryHis changesetHisRepo,
                                                                       CustomizationDataProducer cdp, GspMdpkgRepository gspMdpkgRepository) {
        return new CustomizationServerServiceImpl(metadataRepository, metadataRefsRepository, extRelationRepo, mdContentRepo, mdContentHisRepo, mdRtContentRepo, changesetRepo, changesetHisRepo, cdp, gspMdpkgRepository);
    }

    @Bean
    public MdPkgChangedEventBroker mdPkgChangedEventBroker(EventListenerSettings settings){
        return new MdPkgChangedEventBroker(settings);
    }

    @Bean
    public MetadataDeployEventBroker metadataDeployEventBroker(EventListenerSettings settings){
        return new MetadataDeployEventBroker(settings);
    }

    @Bean
    public MetadataDeployService createMetadataDeployService() {
        return new MetadataDeployServiceImpl();
    }
}
