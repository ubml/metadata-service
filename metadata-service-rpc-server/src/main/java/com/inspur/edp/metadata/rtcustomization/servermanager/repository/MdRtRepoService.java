/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.metadata.rtcustomization.servermanager.repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.api.entity.Metadata4Ref;
import com.inspur.edp.lcm.metadata.api.entity.MetadataHeader;
import com.inspur.edp.lcm.metadata.api.entity.MetadataPackageHeader;
import com.inspur.edp.lcm.metadata.api.entity.MetadataPackageVersion;
import com.inspur.edp.lcm.metadata.api.entity.MetadataProperties;
import com.inspur.edp.lcm.metadata.api.entity.ProcessMode;
import com.inspur.edp.lcm.metadata.api.entity.ServiceUnitInfo;
import com.inspur.edp.lcm.metadata.common.MetadataPropertyUtils;
import com.inspur.edp.lcm.metadata.common.MetadataSerializer;
import com.inspur.edp.lcm.metadata.common.SerializerUtils;
import com.inspur.edp.lcm.metadata.common.ServiceUtils;
import com.inspur.edp.lcm.metadata.common.configuration.MetadataSerializerHelper;
import com.inspur.edp.lcm.metadata.spi.MetadataContentSerializer;
import com.inspur.edp.metadata.rtcustomization.api.entity.GspEmbeddedMdRtContent;
import com.inspur.edp.metadata.rtcustomization.api.entity.GspMdRtContent;
import com.inspur.edp.metadata.rtcustomization.api.entity.GspMdRtContentSuInfo;
import com.inspur.edp.metadata.rtcustomization.api.entity.GspMdpkg;
import com.inspur.edp.metadata.rtcustomization.api.entity.MetadataFilter;
import com.inspur.edp.metadata.rtcustomization.api.entity.SourceTypeEnum;
import com.inspur.edp.metadata.rtcustomization.api.exception.LcmConfigResolveException;
import com.inspur.edp.metadata.rtcustomization.api.exception.ErrorCodes;
import com.inspur.edp.metadata.rtcustomization.api.exception.LcmMetadataException;
import com.inspur.edp.metadata.rtcustomization.api.exception.LcmParseException;
import com.inspur.edp.metadata.rtcustomization.common.PredicateUtils;
import com.inspur.edp.metadata.rtcustomization.servermanager.CustomizationServiceContext;
import com.inspur.edp.metadata.rtcustomization.servermanager.dac.CustomizationDataProducer;
import com.inspur.edp.metadata.rtcustomization.servermanager.dac.GspMdpkgRepository;
import com.inspur.edp.metadata.rtcustomization.servermanager.dac.MetadataRtContentRepository;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import io.iec.caf.data.jpa.repository.CafI18nColumnDict;
import lombok.extern.slf4j.Slf4j;
import lombok.var;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

@Slf4j
public class MdRtRepoService {
    private MetadataRtContentRepository mdRtContentRepo;
    private GspMdpkgRepository gspMdpkgRepository;
    private CustomizationDataProducer cdp;

    public MdRtRepoService(MetadataRtContentRepository mdRtContentRepo, GspMdpkgRepository gspMdpkgRepository) {
        this.mdRtContentRepo = mdRtContentRepo;
        this.gspMdpkgRepository = gspMdpkgRepository;
    }

    public MdRtRepoService(MetadataRtContentRepository mdRtContentRepo, GspMdpkgRepository gspMdpkgRepository, CustomizationDataProducer cdp) {
        this.mdRtContentRepo = mdRtContentRepo;
        this.gspMdpkgRepository = gspMdpkgRepository;
        this.cdp = cdp;
    }

    /**
     * 从运行时表获取元数据
     */
    public GspMetadata getRtMetadata(String metadataId) {
        GspMdRtContent mdRtContent = findGspMdRtContentByMetadataId(metadataId);
        return buildGspMetadataFromGspMdRtContent(mdRtContent);
    }

    public GspMdRtContent findByMetadataIdAndSourceType(String metadataId, SourceTypeEnum sourceType) {
        return findByMetadataIdAndSourceType(metadataId, sourceType.getCode());
    }

    /**
     * 从运行时表获取包中的元数据
     */
    public GspMetadata getRtMetadataByMetadataIdAndSourceType(String metadataId, SourceTypeEnum sourceType) {
        GspMdRtContent mdRtContent = findByMetadataIdAndSourceType(metadataId, sourceType.getCode());
        return buildGspMetadataFromGspMdRtContent(mdRtContent);
    }

    public GspMetadata getMetadataByIdSourceType(String metadataId, int sourceType) {
        GspMdRtContent mdRtContent = findByMetadataIdAndSourceType(metadataId, sourceType);
        return buildGspMetadataFromGspMdRtContent(mdRtContent);
    }

    public List<GspMdRtContent> findByMdpkgid(String mdpkgId) {
        List<GspMdRtContent> gspMdRtContentList = findAllGspMdRtContents(null,0,null,0,null,null,null,mdpkgId);
        if(CollectionUtils.isEmpty(gspMdRtContentList)){
            return null;
        }
        return gspMdRtContentList;
    }

    public List<Metadata4Ref> findAllByMdpkgid(String mdpkgId) {
        List<Metadata4Ref> metadataList = new ArrayList<>();
        List<GspMdRtContent> mdRtContentList = findByMdpkgid(mdpkgId);
        if (!CollectionUtils.isEmpty(mdRtContentList)) {
            mdRtContentList.forEach(mdRtContent -> {
                Metadata4Ref metadata = buildMetadata4RefByGspMdContent(mdRtContent);
                metadataList.add(metadata);
            });
        }
        return metadataList;
    }

    public GspMdRtContent findByMetadataId(String metadataId) {
        return findGspMdRtContentByMetadataId(metadataId);
    }

    public void saveMdRtContent(GspMetadata metadata, SourceTypeEnum sourceType) {
        saveMdRtContent(metadata, sourceType, "");
    }

    public void saveMdRtContent(GspMetadata metadata, SourceTypeEnum sourceType, String mdpkgId) {
        GspMdRtContent rtContent = buildGspMdRtContentFromGspMetadata(metadata, sourceType, mdpkgId);
        mdRtContentRepo.save(new GspEmbeddedMdRtContent(rtContent), buildI18nColumnDict(rtContent.getNameLanguage()));
    }

    // 返回值为旧数据是否存在
    public void updateMetadata(GspMetadata metadata) {
        if (metadata == null) {
            log.info("元数据为空");
            return;
        }
        // 获取旧元数据
        GspMdRtContent oldGspMdRtContent = findGspMdRtContentByMetadataId(metadata.getHeader().getId());
        if (oldGspMdRtContent == null) {
            return;
        }
        GspMdRtContent gspMdRtContent = buildGspMdRtContentFromGspMetadata(metadata, null, null);
        gspMdRtContent.setId(oldGspMdRtContent.getId());
        gspMdRtContent.setSourceType(oldGspMdRtContent.getSourceType());
        gspMdRtContent.setMdpkgId(oldGspMdRtContent.getMdpkgId());
        mdRtContentRepo.save(new GspEmbeddedMdRtContent(gspMdRtContent), buildI18nColumnDict(gspMdRtContent.getNameLanguage()));
    }

    public void saveAll(List<GspMdRtContent> rtContentList) {
        rtContentList.forEach(rtContent -> {
            mdRtContentRepo.save(new GspEmbeddedMdRtContent(rtContent),buildI18nColumnDict(rtContent.getNameLanguage()));
        });
    }

    public void deleteMdRtContent(GspMetadata needToDeleteMetadata) {
        mdRtContentRepo.deleteByMetadataId(needToDeleteMetadata.getHeader().getId());
    }

    public void deleteMdRtContentByMetadataId(String metadataId){
        mdRtContentRepo.deleteByMetadataId(metadataId);
    }

    public void deleteMdRtContent(String mdpkgId) {
        mdRtContentRepo.deleteByMdpkgId(mdpkgId);
    }

    public List<Metadata4Ref> getAllCustomizedMetadata(String metadataTypes) {
        String[] types = metadataTypes.split(",");
        List<String> typeList = new ArrayList<>(Arrays.asList(types));
        return getAllCustomizedMetadata(typeList, false);
    }

    public List<Metadata4Ref> getAllCustomizedMetadata(List<String> metadataTypes, boolean isNoPackageInfo) {
        List<GspMdRtContent> gspMdRtContentList = findAllGspMdRtContents(null,0,null,0,null,null,SourceTypeEnum.CUSTOMIZED.getCode(),null);
        if (gspMdRtContentList == null || gspMdRtContentList.size() == 0) {
            return null;
        }
        List<Metadata4Ref> metadataList = new ArrayList<>();
        gspMdRtContentList.forEach(item -> {
            if(!CollectionUtils.isEmpty(metadataTypes)){
                Optional<String> md = metadataTypes.stream().filter(type -> type != null && type.equalsIgnoreCase(item.getType())).findFirst();
                if (md.isPresent()) {
                    if(isNoPackageInfo){
                        var metadata = buildMetadata4RefByGspMdContent(item, null);
                        metadataList.add(metadata);
                    }else{
                        var metadata = buildMetadata4RefByGspMdContent(item);
                        metadataList.add(metadata);
                    }
                }
            }else {
                if(isNoPackageInfo){
                    var metadata = buildMetadata4RefByGspMdContent(item, null);
                    metadataList.add(metadata);
                }else{
                    var metadata = buildMetadata4RefByGspMdContent(item);
                    metadataList.add(metadata);
                }
            }
        });

        return metadataList;
    }

    public List<Metadata4Ref> getAllNoCodeMetadata(List<String> metadataTypes) {
        List<GspMdRtContent> gspMdRtContentList = findAllGspMdRtContents(null,0,null,0,null,null,SourceTypeEnum.NOCODE.getCode(),null);
        if (CollectionUtils.isEmpty(gspMdRtContentList)) {
            return null;
        }
        List<Metadata4Ref> metadataList = new ArrayList<>();
        gspMdRtContentList.forEach(item -> {
            if(!CollectionUtils.isEmpty(metadataTypes)){
                Optional<String> md = metadataTypes.stream().filter(type -> type != null && type.equalsIgnoreCase(item.getType())).findFirst();
                if (md.isPresent()) {
                    var metadata = buildMetadata4RefByGspMdContent(item, null);
                    metadataList.add(metadata);
                }
            }else{
                var metadata = buildMetadata4RefByGspMdContent(item, null);
                metadataList.add(metadata);
            }
        });

        return metadataList;
    }

    public List<Metadata4Ref> getAllCustomizedMetadata() {
        List<GspMdRtContent> gspMdRtContentList = findAllGspMdRtContents(null,0,null,0,null,null,SourceTypeEnum.CUSTOMIZED.getCode(),null);
        if (gspMdRtContentList == null || gspMdRtContentList.size() == 0) {
            return null;
        }
        List<Metadata4Ref> metadataList = new ArrayList<>();
        gspMdRtContentList.forEach(item -> {
            var metadata = buildMetadata4RefByGspMdContent(item);
            metadataList.add(metadata);
        });

        return metadataList;
    }

    /**
     * 获取来源类型为元数据包的所有元数据
     *
     * @return
     */
    public List<Metadata4Ref> getAllPkgMetadata() {
        List<GspMdRtContent> gspMdRtContentList = findAllGspMdRtContents(null,0,null,0,null,null,SourceTypeEnum.MDPKG.getCode(),null);
        if (CollectionUtils.isEmpty(gspMdRtContentList)) {
            return null;
        }
        List<GspMdpkg> gspMdpkgList = queryGspMdpkgs(null, null, null);
        Map<String, GspMdpkg> MdpkgId2MdpkgMaps = gspMdpkgList.stream().collect(Collectors.toMap(GspMdpkg::getId, gspMdpkg -> gspMdpkg));
        List<Metadata4Ref> result = new ArrayList<>();
        gspMdRtContentList.forEach(item -> {
            Metadata4Ref metadata = buildMetadata4RefForPkgMd(item, MdpkgId2MdpkgMaps.get(item.getMdpkgId()));
            if (Objects.nonNull(metadata)) {
                result.add(metadata);
            }
        });
        return result;
    }

    /**
     * 获取来源类型为元数据包的所有元数据
     *
     * @return
     */
    public List<Metadata4Ref> getAllPkgMetadata(MetadataFilter metadataFilter) {
        if (metadataFilter == null) {
            return getAllPkgMetadata();
        }
        List<GspMdRtContent> gspMdRtContentList = queryGspMdRtContents(metadataFilter.getMetadataTypes(), metadataFilter.getMetadataNamespace(), metadataFilter.getMetadataCode(), metadataFilter.getSourceType(), metadataFilter.getBusinessObjectIds());
        if (CollectionUtils.isEmpty(gspMdRtContentList)) {
            return null;
        }
        List<String> processModesList = null;
        if (!CollectionUtils.isEmpty(metadataFilter.getProcessModes())) {
            processModesList = metadataFilter.getProcessModes().stream().map(processMode -> processMode.toString()).collect(Collectors.toList());
        }
        List<GspMdpkg> gspMdpkgList = queryGspMdpkgs(metadataFilter.getMdpkgName(), processModesList, metadataFilter.getSuCodes());
        Map<String, GspMdpkg> MdpkgId2MdpkgMaps = gspMdpkgList.stream().collect(Collectors.toMap(GspMdpkg::getId, gspMdpkg -> gspMdpkg));

        List<Metadata4Ref> result = new ArrayList<>();
        gspMdRtContentList.forEach(item -> {
            Metadata4Ref metadata = buildMetadata4RefForPkgMd(item, MdpkgId2MdpkgMaps.get(item.getMdpkgId()));
            if (Objects.nonNull(metadata)) {
                result.add(metadata);
            }
        });
        return result;
    }

    public List<GspMdpkg> queryGspMdpkgs(String mdpkgName, List<String> processModes, List<String> suCodes) {
        // 构造查询条件
          List<GspMdpkg> infos = gspMdpkgRepository.findAllInfo(mdpkgName, processModes,
              CollectionUtils.isEmpty(processModes) ? 0 : processModes.size()
          );
        return infos;
    }

    /**
     * 获取来源类型为元数据包的所有元数据
     *
     * @return
     */
    public List<Metadata4Ref> getMetadata4RefList(List<String> metadataTypes, SourceTypeEnum sourceTypeEnum) {
        List<GspMdRtContent> gspMdRtContentList = queryGspMdRtContents(metadataTypes, null, null, sourceTypeEnum, null);
        if (CollectionUtils.isEmpty(gspMdRtContentList)) {
            return null;
        }
        List<GspMdpkg> gspMdpkgList = queryGspMdpkgs(null, null, null);
        Map<String, GspMdpkg> mdpkgId2MdpkgMaps = gspMdpkgList.stream().collect(Collectors.toMap(GspMdpkg::getId, gspMdpkg -> gspMdpkg));
        List<Metadata4Ref> result = new ArrayList<>();
        gspMdRtContentList.forEach(item -> {
            Metadata4Ref metadata = buildMetadata4RefForPkgMd(item, mdpkgId2MdpkgMaps.get(item.getMdpkgId()));
            if (Objects.nonNull(metadata)) {
                result.add(metadata);
            }
        });
        return result;
    }

    /**
     * 根据条件获取来源类型为元数据包的所有元数据
     *
     * @param metadataTypeList
     * @param keyword
     * @param sourceTypeEnum
     * @param beginNum
     * @param size
     * @return
     */
    public List<Metadata4Ref> getMetadata4RefListByCondition(List<String> metadataTypeList, String keyword, String bizobjectID, SourceTypeEnum sourceTypeEnum, int beginNum, int size) {
        List<GspMdRtContent> gspMdRtContentList = getGspMdRtContentsByCondition(metadataTypeList, keyword, bizobjectID, sourceTypeEnum, beginNum, size);
        if (CollectionUtils.isEmpty(gspMdRtContentList)) {
            return null;
        }
        // 因目前前端逻辑限制，全表查询效率更高，后续可考虑打开注释进行优化
        //List<GspMdpkg> gspMdpkgList = gspMdpkgRepository.findAllById(gspMdRtContentList.stream().map(GspMdRtContent::getMdpkgId).collect(Collectors.toList()));
        List<GspMdpkg> gspMdpkgList = queryGspMdpkgs(null, null, null);
        Map<String, GspMdpkg> mdpkgId2MdpkgMaps = gspMdpkgList.stream().collect(Collectors.toMap(GspMdpkg::getId, gspMdpkg -> gspMdpkg));
        List<Metadata4Ref> result = new ArrayList<>();
        gspMdRtContentList.forEach(item -> {
            Metadata4Ref metadata = buildMetadata4RefForPkgMd(item, mdpkgId2MdpkgMaps.get(item.getMdpkgId()));
            if (Objects.nonNull(metadata)) {
                result.add(metadata);
            }
        });
        return result;
    }

    /**
     * 根据条件获取来源类型为运行时定制的元数据
     *
     * @param metadataTypeList
     * @param keyword
     * @param sourceTypeEnum
     * @param isNoPackageInfo
     * @param beginNum
     * @param size
     * @return
     */
    public List<Metadata4Ref> getCustomizedMetadataListByCondition(List<String> metadataTypeList, String keyword, String bizobjectID, SourceTypeEnum sourceTypeEnum, boolean isNoPackageInfo, int beginNum, int size) {
        List<GspMdRtContent> gspMdRtContentList = getGspMdRtContentsByCondition(metadataTypeList, keyword, bizobjectID, sourceTypeEnum, beginNum, size);
        if (CollectionUtils.isEmpty(gspMdRtContentList)) {
            return null;
        }
        List<Metadata4Ref> metadataList = new ArrayList<>();
        gspMdRtContentList.forEach(item -> {
            var metadata = isNoPackageInfo ? buildMetadata4RefByGspMdContent(item, null) : buildMetadata4RefByGspMdContent(item);
            metadataList.add(metadata);
        });
        return metadataList;
    }

    /**
     * 获取根据ID元数据包内元数据(不带content)
     * @param metadataId
     * @return
     */
    public Metadata4Ref getMetadata4RefById(String metadataId) {
        if (StringUtils.isEmpty(metadataId)) {
            return null;
        }
        GspMdRtContent metadata = findGspMdRtContentByMetadataId(metadataId);
        if (metadata == null) {
            return null;
        }
        Optional<GspMdpkg> gspMdpkgOptional = gspMdpkgRepository.findById(metadata.getMdpkgId());
        return buildMetadata4RefForPkgMd(metadata, gspMdpkgOptional.get());
    }

    public List<Metadata4Ref> getMdpkgMetadata4RefList(List<String> metadataTypes) {
        List<GspMdRtContent> gspMdRtContentList = queryGspMdRtContents(metadataTypes, SourceTypeEnum.MDPKG);
        if (CollectionUtils.isEmpty(gspMdRtContentList)) {
            return null;
        }
        List<GspMdpkg> gspMdpkgList = queryGspMdpkgs(null, null, null);
        Map<String, GspMdpkg> mdpkgId2MdpkgMaps = gspMdpkgList.stream().collect(Collectors.toMap(GspMdpkg::getId, gspMdpkg -> gspMdpkg));
        List<Metadata4Ref> result = new ArrayList<>();
        gspMdRtContentList.forEach(item -> {
            Metadata4Ref metadata = buildMetadata4RefForPkgMd(item, mdpkgId2MdpkgMaps.get(item.getMdpkgId()));
            if (Objects.nonNull(metadata)) {
                result.add(metadata);
            }
        });
        return result;
    }

    public List<GspMdRtContent> queryGspMdRtContents(List<String> metadataTypes, SourceTypeEnum sourceType) {
        List<GspEmbeddedMdRtContent> allSimpleGspMdRtContents = mdRtContentRepo.findAllSimpleGspMdRtContents(CollectionUtils.isEmpty(metadataTypes) ? null : metadataTypes, CollectionUtils.isEmpty(metadataTypes) ? 0 : metadataTypes.size(), sourceType == null ? null : sourceType.getCode());
        List<GspMdRtContent> gspMdRtContentList = new ArrayList<>();
        if(!CollectionUtils.isEmpty(allSimpleGspMdRtContents)){
            allSimpleGspMdRtContents.stream().forEach(item -> {
                if(item != null){
                    gspMdRtContentList.add(new GspMdRtContent(item));
                }
            });
        }
        return gspMdRtContentList;
    }

    public List<GspMdRtContent> queryGspMdRtContents(List<String> metadataTypes, String metadataNamespace, String metadataCode, SourceTypeEnum sourceType, List<String> businessObjectIds) {
        List<GspMdRtContent> gspMdRtContentList = findAllGspMdRtContents(CollectionUtils.isEmpty(metadataTypes)?null:metadataTypes,
            CollectionUtils.isEmpty(metadataTypes) ? 0 : metadataTypes.size(),
            businessObjectIds,
            CollectionUtils.isEmpty(businessObjectIds) ? 0 : businessObjectIds.size(),
            metadataNamespace,
            metadataCode,
            sourceType == null ?  null : sourceType.getCode(),
            null
        );
        if(CollectionUtils.isEmpty(gspMdRtContentList)){
           return null;
        }
        return gspMdRtContentList;
    }

    /**
     * 根据条件获取MDPKG元数据
     *
     * @param metadataTypes
     * @param keyword
     * @param sourceType
     * @param beginNum
     * @param size
     * @return
     */
    public List<GspMdRtContent> getGspMdRtContentsByCondition(List<String> metadataTypes, String keyword, String bizobjectID, SourceTypeEnum sourceType, int beginNum, int size) {

        EntityManager entityManager = cdp.getEm();
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<GspEmbeddedMdRtContent> query = cb.createQuery(GspEmbeddedMdRtContent.class);
        Root<GspEmbeddedMdRtContent> root = query.from(GspEmbeddedMdRtContent.class);
        // 实际查询字段
        query.multiselect(root.get("id"), root.get("metadataId"), root.get("version"), root.get("certId"), root.get("previousVersion"), root.get("code"),
                root.get("name"), root.get("type"), root.get("bizObjId"), root.get("createBy"), root.get("createdOn"), root.get("lastChangedBy"), root.get("lastChangedOn"),
                root.get("extended"), root.get("nameSpace"), root.get("extendProperty"), root.get("sourceType"), root.get("mdpkgId"), root.get("language"), root.get("translating"),
                root.get("extendable"), root.get("properties"), root.get("filename"));

        Predicate finalPredicate = PredicateUtils.buildMdQueryPredicate(keyword, bizobjectID, metadataTypes, sourceType, cb, root,GspEmbeddedMdRtContent.class);
        // 查询包元数据时，需要关联GspMdpkg表
        if (SourceTypeEnum.MDPKG.equals(sourceType)) {
            Root<GspMdpkg> gspMdpkgRoot = query.from(GspMdpkg.class);
            Predicate joinCondition = cb.and(root.get("mdpkgId").isNotNull(), cb.equal(root.get("mdpkgId"), gspMdpkgRoot.get("id")));
            finalPredicate = cb.and(finalPredicate, joinCondition);
        }
        query.where(finalPredicate);

        // 执行查询，获取总记录数
        List<GspMdRtContent> resultList = new ArrayList<>();
        List<GspEmbeddedMdRtContent> embeddedMdRtContentList = entityManager.createQuery(query).setFirstResult(beginNum).setMaxResults(size).getResultList();
        if(!CollectionUtils.isEmpty(embeddedMdRtContentList)){
            embeddedMdRtContentList.stream().forEach(item -> {
                resultList.add(new GspMdRtContent(item));
            });
        }
        return resultList;
    }

    /**
     * 根据条件获取MDPKG元数据总数量
     *
     * @param metadataTypeList
     * @param keyword
     * @param sourceType
     * @return
     */
    public int getGspMdRtContentsCountByCondition(List<String> metadataTypeList, String keyword, String bizobjectID, SourceTypeEnum sourceType) {

        EntityManager entityManager = cdp.getEm();
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> query = cb.createQuery(Long.class);
        Root<GspEmbeddedMdRtContent> root = query.from(GspEmbeddedMdRtContent.class);
        // 查询总数
        query.select(cb.count(root));
        Predicate finalPredicate = PredicateUtils.buildMdQueryPredicate(keyword, bizobjectID, metadataTypeList, sourceType, cb, root,GspEmbeddedMdRtContent.class);
        // 查询包元数据时，需要关联GspMdpkg表
        if (SourceTypeEnum.MDPKG.equals(sourceType)) {
            Root<GspMdpkg> gspMdpkgRoot = query.from(GspMdpkg.class);
            Predicate joinCondition = cb.and(root.get("mdpkgId").isNotNull(), cb.equal(root.get("mdpkgId"), gspMdpkgRoot.get("id")));
            finalPredicate = cb.and(finalPredicate, joinCondition);
        }
        query.where(finalPredicate);

        // 执行查询，获取总记录数
        Long count = entityManager.createQuery(query).getSingleResult();
        return Math.toIntExact(count);
    }

    /**
     * 获取指定元数据
     *
     * @return
     */
    public Metadata4Ref getPkgMetadataByMetadataId(String metadataId) {
        if (Objects.isNull(metadataId)) {
            return null;
        }
        GspMdRtContent gspMdRtContent = findGspMdRtContentByMetadataId(metadataId);
        if (gspMdRtContent == null) {
            return null;
        }
        Optional<GspMdpkg> gspMdpkgOptional = gspMdpkgRepository.findById(gspMdRtContent.getMdpkgId());
        if (!gspMdpkgOptional.isPresent()) {
            return null;
        }
        return buildMetadata4RefForPkgMd(gspMdRtContent, gspMdpkgOptional.get());
    }

    /**
     * 获取指定元数据(带content)
     *
     * @return
     */
    public Metadata4Ref getMetadata4RefByMetadataId(String metadataId) {
        if (Objects.isNull(metadataId)) {
            return null;
        }
        GspMdRtContent gspMdRtContent = findByMetadataId(metadataId);
        GspMetadata metadata = buildGspMetadataFromGspMdRtContent(gspMdRtContent);
        Optional<GspMdpkg> gspMdpkgOptional = gspMdpkgRepository.findById(gspMdRtContent.getMdpkgId());
        if (!gspMdpkgOptional.isPresent()) {
            return null;
        }
        ServiceUnitInfo serviceUnitInfo = new ServiceUnitInfo(gspMdpkgOptional.get().getAppcode(), gspMdpkgOptional.get().getServiceunitcode());
        MetadataPackageHeader metadataPackageHeader = new MetadataPackageHeader(gspMdpkgOptional.get().getName(),
            new MetadataPackageVersion(gspMdpkgOptional.get().getVersion()),
            gspMdpkgOptional.get().getLocation(),
            ProcessMode.interpretation.toString().equals(gspMdpkgOptional.get().getProcessmode()) ? ProcessMode.interpretation : ProcessMode.generation);
        return new Metadata4Ref(metadataPackageHeader, serviceUnitInfo, metadata);
    }

    public Metadata4Ref getMetadata4RefWithResourceType(String metadataId) {
        GspMdRtContent gspMdRtContent = findByMetadataId(metadataId);
        if (gspMdRtContent == null) {
            return null;
        }
        Metadata4Ref metadata4Ref;
        if (StringUtils.hasLength(gspMdRtContent.getMdpkgId())) {
            GspMdpkg gspMdpkg = gspMdpkgRepository.findById(gspMdRtContent.getMdpkgId()).orElse(null);
            if (gspMdpkg == null) {
                throw new LcmMetadataException(ErrorCodes.ECP_METADATA_0020, metadataId, gspMdRtContent.getMdpkgId());
            }
            metadata4Ref = buildMetadata4RefForPkgMd(gspMdRtContent, gspMdpkg);
        } else {
            metadata4Ref = buildMetadata4RefByGspMdContent(gspMdRtContent, null);
        }
        // 识别运行时定制
        if (gspMdRtContent.getSourceType() == null) {
            gspMdRtContent.setSourceType(SourceTypeEnum.CUSTOMIZED.getCode());
        }
        // 这里metadata4Ref不可能为null
        metadata4Ref.setSourceType(SourceTypeEnum.fromValue(gspMdRtContent.getSourceType()));
        return metadata4Ref;
    }

    private Metadata4Ref buildMetadata4RefByGspMdContent(GspMdRtContent gspMdRtContent) {
        return buildMetadata4RefByGspMdContent(gspMdRtContent, new MetadataPackageHeader());
    }

    private Metadata4Ref buildMetadata4RefByGspMdContent(GspMdRtContent gspMdRtContent, MetadataPackageHeader metadataPackageHeader) {
        GspMetadata metadata = buildGspMetadataWithoutContentFromGspMdRtContent(gspMdRtContent);
        Metadata4Ref metadata4Ref = new Metadata4Ref(metadataPackageHeader, new ServiceUnitInfo(), metadata);
        return metadata4Ref;
    }

    private Metadata4Ref buildMetadata4RefForPkgMd(GspMdRtContent gspMdRtContent, GspMdpkg gspMdpkg) {
        if (Objects.isNull(gspMdpkg)) {
            return null;
        }
        GspMetadata metadata = buildGspMetadataWithoutContentFromGspMdRtContent(gspMdRtContent);
        ServiceUnitInfo serviceUnitInfo = new ServiceUnitInfo(gspMdpkg.getAppcode(), gspMdpkg.getServiceunitcode());
        MetadataPackageHeader metadataPackageHeader = new MetadataPackageHeader(gspMdpkg.getName(), new MetadataPackageVersion(gspMdpkg.getVersion()), gspMdpkg.getLocation(), ProcessMode.interpretation.toString().equals(gspMdpkg.getProcessmode()) ? ProcessMode.interpretation : ProcessMode.generation);
        Metadata4Ref metadata4Ref = new Metadata4Ref(metadataPackageHeader, serviceUnitInfo, metadata);

        return metadata4Ref;
    }

    public GspMetadata buildGspMetadataWithoutContentFromGspMdRtContent(GspMdRtContent mdContent) {
        MetadataHeader header = new MetadataHeader(mdContent.getMetadataId(), mdContent.getCertId(), mdContent.getNameSpace(), mdContent.getCode(), mdContent.getName(),
                mdContent.getFilename(), mdContent.getType(), mdContent.getBizObjId(), mdContent.getLanguage(), mdContent.isTranslating(), mdContent.isExtendable());
        MetadataProperties properties = StringUtils.isEmpty(mdContent.getProperties()) ? null : SerializerUtils.deserialize(mdContent.getProperties(), MetadataProperties.class);
        GspMetadata metadata = new GspMetadata(header, mdContent.getExtendProperty(), mdContent.getExtended(), mdContent.getPreviousVersion(), mdContent.getVersion(), properties);
        return metadata;
    }

    private GspMdRtContent buildGspMdRtContentFromGspMetadata(GspMetadata metadata, SourceTypeEnum sourceType, String mdpkgId) {
        // 若存在，则不更新id,metadataId,createdOn,createBy
        GspMdRtContent gspMdRtContent = findGspMdRtContentByMetadataId(metadata.getHeader().getId());
        if (gspMdRtContent == null) {
            gspMdRtContent = new GspMdRtContent(metadata.getHeader().getId(), CustomizationServiceContext.getUserName());
        }

        // 把metadata都存到content中
        String metadataStr = null;
        MetadataContentSerializer manager = MetadataSerializerHelper.getInstance().getManager(metadata.getHeader().getType());
        if (manager == null) {
            throw new LcmConfigResolveException(ErrorCodes.ECP_CONFIG_RESOLVE_0006, metadata.getHeader().getType());
        }
        JsonNode jsonNode = manager.Serialize(metadata.getContent());
        ObjectMapper objectMapper = ServiceUtils.getMapper();
        GspMetadata newMetadata = (GspMetadata) metadata.clone();
        try {
            metadataStr = objectMapper.writeValueAsString(newMetadata);
            JsonNode metadataObj = objectMapper.readTree(metadataStr);
            ObjectNode objNode = (ObjectNode) metadataObj;
            objNode.set("Content", jsonNode);
            metadataStr = objectMapper.writeValueAsString(objNode);
        } catch (JsonProcessingException e) {
            log.error("序列化元数据失败：" + metadata.getHeader().toString());
        }

        String properties = SerializerUtils.serialize(metadata.getProperties());

        // 更新
        gspMdRtContent.update(metadata,metadataStr,CustomizationServiceContext.getUserName(),sourceType == null ? 0 : sourceType.getCode(), mdpkgId,properties);
        return gspMdRtContent;
    }

    public GspMdRtContent buildGspMdRtContentFromFileString(String fileString, SourceTypeEnum sourceType, String mdpkgId) {
        // 判空
        if (StringUtils.isEmpty(fileString)) {
            return null;
        }
        // 初始变量
        ObjectMapper mapper = ServiceUtils.getMapper();
        JsonNode mdJsonNode;
        // 反序列化字符串为jsonNode
        try {
            mdJsonNode = mapper.readTree(fileString);
        } catch (JsonProcessingException e) {
            log.error(fileString);
            throw new LcmParseException(e, ErrorCodes.ECP_PARSE_0004);
        }
        // 检查数据库中是否存在，初始化需要保存的元数据
        JsonNode headerNode = mdJsonNode.findValue(MetadataPropertyUtils.header);
        String metadataId = headerNode.findValue("ID").asText();

        // 若存在，则不更新id,metadataId,createdOn,createBy
        GspMdRtContent gspMdRtContent = findGspMdRtContentByMetadataId(metadataId);
        if (gspMdRtContent == null) {
            gspMdRtContent = new GspMdRtContent(metadataId, CustomizationServiceContext.getUserName());
        }

        // 更新
        gspMdRtContent.update(mdJsonNode,fileString,CustomizationServiceContext.getUserName(),sourceType.getCode(),mdpkgId);
        return gspMdRtContent;
    }

    public GspMetadata buildGspMetadataFromGspMdRtContent(GspMdRtContent gspMdRtContent) {
        if (gspMdRtContent == null || gspMdRtContent.getContent() == null) {
            return null;
        }
        GspMetadata metadata = new MetadataSerializer().deserialize(gspMdRtContent.getContent(), GspMetadata.class);
        if (metadata.getProperties() == null) {
            metadata.setProperties(new MetadataProperties("", gspMdRtContent.getLastChangedOn().toString()));
        } else {
            metadata.getProperties().setCacheVersion(gspMdRtContent.getLastChangedOn().toString());
        }
        return metadata;
    }

    public List<GspMdRtContentSuInfo> findAllGspMdRtContentSuInfo() {
        return mdRtContentRepo.findAllGspMdRtContentSuInfo();
    }

    public void makeUnique() {
        List<GspMdRtContent> gspMdRtContents = mdRtContentRepo.findAllGroupByMetadataIdHavingCountGreaterThanOne();
        if (CollectionUtils.isEmpty(gspMdRtContents)) {
            return;
        }

        List<GspEmbeddedMdRtContent> gspMdRtContentsToDelete = new ArrayList<>();
        gspMdRtContents.forEach(mdRtContent -> {
            List<GspEmbeddedMdRtContent> allByName = mdRtContentRepo.findAllByMetadataIdAndLastChangedOn(mdRtContent.getMetadataId(), mdRtContent.getLastChangedOn());

            if (!CollectionUtils.isEmpty(allByName)) {
                gspMdRtContentsToDelete.add(allByName.get(0));
            }
        });
        if (!CollectionUtils.isEmpty(gspMdRtContentsToDelete)) {
            mdRtContentRepo.deleteAll(gspMdRtContentsToDelete);
        }

        // 检查是否还有重复
        makeUnique();
    }
    /**
     * 根据名称和语言的映射关系构建I18n列字典。
     *
     * 此方法用于根据提供的名称和语言映射关系，构建一个I18n列字典对象。该字典主要用于存储多语言环境下列名称的映射，
     * 允许系统根据当前语言选择合适的列名显示。
     *
     * @param nameLanguage 映射关系，包含列名及其在不同语言环境下的对应名称。
     * @return 返回构建的I18n列字典对象。如果输入的映射关系为空，则返回一个空的字典对象。
     */
    private static CafI18nColumnDict buildI18nColumnDict(Map<String, String> nameLanguage) {
        if (CollectionUtils.isEmpty(nameLanguage)) {
            return new CafI18nColumnDict();
        }

        Map<String, Map<String, String>> multiLan = new HashMap();
        multiLan.put("name", nameLanguage);

        CafI18nColumnDict dict = new CafI18nColumnDict();
        dict.setColI18nValues(multiLan);

        return dict;
    }
    public GspMdRtContent findGspMdRtContentByMetadataId(String metadataId) {
        GspEmbeddedMdRtContent embeddedMdRtContent = mdRtContentRepo.findByMetadataId(metadataId);
        return convertGspMdRtContent(embeddedMdRtContent);
    }

    public GspMdRtContent findByMetadataIdAndSourceType(String metadataId, int sourceType) {
        GspEmbeddedMdRtContent embeddedMdRtContent = mdRtContentRepo.findByMetadataIdAndSourceType(metadataId, sourceType);
        return convertGspMdRtContent(embeddedMdRtContent);
    }

    public List<GspMdRtContent> findAllGspMdRtContents(List<String> metadataTypes, int typeSize,
                                                               List<String> businessObjectIds, int objSize,
                                                               String metadataNamespace,
                                                               String metadataCode,
                                                               Integer sourceType,
                                                               String mdpkgId){
        List<GspEmbeddedMdRtContent> allGspEmbeddedMdRtContents = mdRtContentRepo.findAllGspMdRtContents(metadataTypes, typeSize, businessObjectIds, objSize, metadataNamespace, metadataCode, sourceType, mdpkgId);
        List<GspMdRtContent> gspMdRtContents = new ArrayList<>();
        if(!CollectionUtils.isEmpty(allGspEmbeddedMdRtContents)){
            allGspEmbeddedMdRtContents.forEach(gspEmbeddedMdRtContent -> {
                if(gspEmbeddedMdRtContent != null){
                    gspMdRtContents.add(new GspMdRtContent(gspEmbeddedMdRtContent));
                }
            });
        }
        return gspMdRtContents;
    }
    /**
     *  根据EmbeddedMdRtContent创建GspMdRtContent
     * @param embeddedMdRtContent
     * @return
     */
    private GspMdRtContent convertGspMdRtContent(GspEmbeddedMdRtContent embeddedMdRtContent) {

        return embeddedMdRtContent == null? null : new GspMdRtContent(embeddedMdRtContent);
    }
}
