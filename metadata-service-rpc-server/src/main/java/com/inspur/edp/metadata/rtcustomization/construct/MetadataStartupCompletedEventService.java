/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.metadata.rtcustomization.construct;

import com.inspur.edp.lcm.metadata.configuration.CacheModeEnum;
import com.inspur.edp.lcm.metadata.configuration.CustomizationCacheHelper;
import com.inspur.edp.metadata.rtcustomization.common.LockUtils;
import com.inspur.edp.metadata.rtcustomization.common.ScheduledConfigLoader;
import com.inspur.edp.metadata.rtcustomization.common.TenantUtils;
import com.inspur.edp.metadata.rtcustomization.common.ThreadUtils;
import com.inspur.edp.metadata.rtcustomization.construct.schedual.MdpkgChangedRunnable;
import com.inspur.edp.metadata.rtcustomization.serverapi.CustomizationRtServerService;
import io.iec.edp.caf.boot.context.CAFContext;
import io.iec.edp.caf.commons.event.StartupCompletedEvent;
import io.iec.edp.caf.commons.utils.CollectionUtils;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import io.iec.edp.caf.tenancy.api.ITenantService;
import io.iec.edp.caf.tenancy.api.entity.Tenant;
import io.iec.edp.caf.tenancy.core.context.MultiTenantContextHolder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.core.annotation.Order;

import java.util.List;
import java.util.concurrent.*;

// rpc的顺序是-2147483647，元数据框架依赖rpc，因为要清除元数据缓存，所以要在其他人使用之前
@Order(-2147483646)
@Slf4j
public class MetadataStartupCompletedEventService implements ApplicationListener<StartupCompletedEvent> {
    private static final String EVENT_THREAD_NAME_PREFIX = "lcm-metadata-startup-completed-event";
    private static final String SCHEDULED_THREAD_NAME_PREFIX = "lcm-metadata-scheduled-";
    private static final String METADATA_SERVICE_INIT_LOCK = "lcm-MetadataStartupCompletedEventService-init-lock";
    private static final CacheModeEnum cacheModeEnum = CustomizationCacheHelper.getInstance().getCacheMode();
    private final CustomizationRtServerService rtServerService = SpringBeanUtils.getBean(CustomizationRtServerService.class);

    private static ExecutorService executor = null;

    @Override
    public void onApplicationEvent(StartupCompletedEvent startupCompletedEvent) {
        // 多租户最小化主库场景中，主库中没有元数据表，没有session时会报错，所以需要构建租户session
        // 可以对所有场景构建租户session，但是影响较大，目前根据多租户标识来判断是否构建租户session，标识目前仅有inSuite使用
        if (cacheModeEnum == CacheModeEnum.SINGLE_TENANT) {
            executor = new ThreadPoolExecutor(3, 3, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<>(), ThreadUtils.getThreadFactory(EVENT_THREAD_NAME_PREFIX));
            init(new Tenant());
        } else {
            ITenantService tenantService = SpringBeanUtils.getBean(ITenantService.class);
            List<Tenant> allTenants = tenantService.getAllTenants(CAFContext.current.getLanguage());
            // 租户为空的情况应该不存在，存在的话就按照单租户处理
            if (CollectionUtils.isEmpty(allTenants)) {
                executor = new ThreadPoolExecutor(3, 3, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<>(), ThreadUtils.getThreadFactory(EVENT_THREAD_NAME_PREFIX));
                init(new Tenant());
            } else {
                int poolSize = allTenants.size() * 3;
                executor = new ThreadPoolExecutor(poolSize, poolSize, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<>(), ThreadUtils.getThreadFactory(EVENT_THREAD_NAME_PREFIX));
                for (Tenant tenant : allTenants) {
                    // 在context中设置租户信息
                    MultiTenantContextHolder.set(TenantUtils.buildTenantContext(tenant));
                    // 单租户刷新
                    init(tenant);
                }
            }
        }
        // 关闭线程池
        executor.shutdown();

        // 启动轮询
        if (ScheduledConfigLoader.isScheduledStart()) {
            ScheduledExecutorService scheduledExecutorService = new ScheduledThreadPoolExecutor(1, ThreadUtils.getThreadFactory(SCHEDULED_THREAD_NAME_PREFIX));
            scheduledExecutorService.scheduleAtFixedRate(new MdpkgChangedRunnable(), 30, 10, TimeUnit.SECONDS); // 30s 后开始执行，每 10s 执行一次
        }
    }

    private void init(Tenant tenant) {

        // 启动时加载所有元数据的su信息，存入缓存
        executor.submit(rtServerService::saveSuInfoToCache);

        // 获取带看门狗的分布式锁，失败则不进行预加载
        if (!LockUtils.tryLock(tenant.getId() + METADATA_SERVICE_INIT_LOCK)) {
            return;
        }

        // 启动时加载所有gspmdcustomcontent(idp)中的元数据
        if (CustomizationCacheHelper.getInstance().isPreload()) {
            executor.submit(rtServerService::preloadAllGeneratedMetadata);
        }

        // 启动时初始化gspmdrefs
        executor.submit(rtServerService::initMdRefs);
    }

}
