/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.metadata.rtcustomization.api.entity;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Entity
@Table
public class GspMdpkg {
    @Id
    private String id;
    private String name;
    private String version;
    private String processmode;
    private String location;
    private String appcode;
    private String serviceunitcode;
    private String referenceinfo;
    private String createdBy;
    private LocalDateTime createdOn;
    private String lastChangedBy;
    private LocalDateTime lastChangedOn;
    private String manifestInfo;

    @OneToMany
    @JoinColumn(name = "mdpkgid")
    private List<GspEmbeddedMdRtContent> metadatas;

    public GspMdpkg() {}

    public GspMdpkg(String name, LocalDateTime lastChangedOn) {
        this.name = name;
        this.lastChangedOn = lastChangedOn;
    }

    public GspMdpkg(String id, String name, String version, String processmode, String location, String appcode, String serviceunitcode, String createdBy, LocalDateTime createdOn, String lastChangedBy, LocalDateTime lastChangedOn) {
        this.id = id;
        this.name = name;
        this.version = version;
        this.processmode = processmode;
        this.location = location;
        this.appcode = appcode;
        this.serviceunitcode = serviceunitcode;
        this.createdBy = createdBy;
        this.createdOn = createdOn;
        this.lastChangedBy = lastChangedBy;
        this.lastChangedOn = lastChangedOn;
    }

    public GspMdpkg(String name, String userName) {
        this.id = UUID.randomUUID().toString();
        this.name = name;
        this.createdOn = LocalDateTime.now();
        this.createdBy = userName;
    }

    public GspMdpkg(String id, String name, String manifestInfo) {
        this.id = id;
        this.name = name;
        this.manifestInfo = manifestInfo;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getProcessmode() {
        return processmode;
    }

    public void setProcessmode(String processmode) {
        this.processmode = processmode;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getAppcode() {
        return appcode;
    }

    public void setAppcode(String appcode) {
        this.appcode = appcode;
    }

    public String getServiceunitcode() {
        return serviceunitcode;
    }

    public void setServiceunitcode(String serviceunitcode) {
        this.serviceunitcode = serviceunitcode;
    }

    public String getReferenceinfo() {
        return referenceinfo;
    }

    public void setReferenceinfo(String referenceinfo) {
        this.referenceinfo = referenceinfo;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDateTime getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(LocalDateTime createdOn) {
        this.createdOn = createdOn;
    }

    public String getLastChangedBy() {
        return lastChangedBy;
    }

    public void setLastChangedBy(String lastChangedBy) {
        this.lastChangedBy = lastChangedBy;
    }

    public LocalDateTime getLastChangedOn() {
        return lastChangedOn;
    }

    public void setLastChangedOn(LocalDateTime lastChangedOn) {
        this.lastChangedOn = lastChangedOn;
    }


    public List<GspEmbeddedMdRtContent> getMetadatas() {
        return metadatas;
    }

    public void setMetadatas(List<GspEmbeddedMdRtContent> metadatas) {
        this.metadatas = metadatas;
    }

    public String getManifestInfo() {
        return manifestInfo;
    }

    public void setManifestInfo(String manifestInfo) {
        this.manifestInfo = manifestInfo;
    }
}
