/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.metadata.rtcustomization.servermanager.event;

import io.iec.edp.caf.commons.event.CAFEventArgs;
import io.iec.edp.caf.commons.event.EventBroker;
import io.iec.edp.caf.commons.event.IEventListener;
import io.iec.edp.caf.commons.event.config.EventListenerData;
import io.iec.edp.caf.commons.event.config.EventListenerSettings;

public class MetadataDeployEventBroker extends EventBroker {
	public MetadataDeployEventManager metadataDeployEventManager;
	public MetadataDeployEventBroker(EventListenerSettings settings){
		super(settings);
		this.metadataDeployEventManager=new MetadataDeployEventManager();
		this.init();
	}

	/**
	 * 触发事件
	 * @param e 事件参数
	 * @return
	 */
	public void fireMdPkgDeployedEvent(CAFEventArgs e)
	{
		this.metadataDeployEventManager.fireMdPkgDeployedEvent(e);
	}

	public void fireExtMdSavedEvent(CAFEventArgs e)
	{
		this.metadataDeployEventManager.fireExtMdSavedEvent(e);
	}

	/**
	 * 初始化
	 */
	@Override
	protected void onInit(){
		this.eventManagerCollection.add(metadataDeployEventManager);
	}

	/**
	 * 测试异常是否被抛出
	 */
	public IEventListener createEventListenerTest(EventListenerData eventListenerData)
	{
		return this.createEventListener(eventListenerData);
	}
	/**
	 * 测试注销事件
	 * @param eventListener
	 */
	public void removeListenerTest(IEventListener eventListener){
		this.removeListener(eventListener);
	}
}
