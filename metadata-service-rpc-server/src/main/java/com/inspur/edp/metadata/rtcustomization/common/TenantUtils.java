/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.metadata.rtcustomization.common;

import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import io.iec.edp.caf.tenancy.api.ITenantService;
import io.iec.edp.caf.tenancy.api.context.MultiTenantContextInfo;
import io.iec.edp.caf.tenancy.api.entity.AppInstanceInfo;
import io.iec.edp.caf.tenancy.api.entity.Tenant;

import java.util.List;
/**
 * 租户信息工具类
 *
 * @author liuyuxin01
 * @since 0.1.35
 */
public class TenantUtils {
    private static final String DEFAULT_SERVICEUNIT = "Lcm";

    public static MultiTenantContextInfo buildTenantContext(Tenant tenant) {
        ITenantService tenantService = SpringBeanUtils.getBean(ITenantService.class);
        List<AppInstanceInfo> allAppInstInfos = tenantService.getAllAppInstInfos(tenant.getId());
        MultiTenantContextInfo multiTenantContextInfo = new MultiTenantContextInfo();
        multiTenantContextInfo.setTenantId(tenant.getId());
        multiTenantContextInfo.setAppCode(allAppInstInfos.get(0).getCode());
        multiTenantContextInfo.setServiceUnit(DEFAULT_SERVICEUNIT);
        return multiTenantContextInfo;
    }
}
