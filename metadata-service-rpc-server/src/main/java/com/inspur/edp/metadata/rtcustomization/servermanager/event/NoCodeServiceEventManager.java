/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.metadata.rtcustomization.servermanager.event;

import com.inspur.edp.lcm.metadata.spi.event.NoCodeServiceEventListener;
import com.inspur.edp.lcm.metadata.spi.event.nodecode.NoCodeEventArgs;
import com.inspur.edp.metadata.rtcustomization.api.exception.ErrorCodes;
import com.inspur.edp.metadata.rtcustomization.api.exception.LcmEventException;
import io.iec.edp.caf.commons.event.EventManager;
import io.iec.edp.caf.commons.event.IEventListener;

/**
 * 功能描述:
 *
 * @ClassName: NoCodeServiceEventManager
 * @Author: Fynn Qi
 * @Date: 2021/5/26 14:34
 * @Version: V1.0
 */
public class NoCodeServiceEventManager extends EventManager {

    private static final String NO_CODE_SERVICE_EVENT_MANAGER_NAME="NoCodeServiceEventManager";

    private enum EventType{
        BEFORE_SAVE,
        AFTER_SAVE,
        BEFORE_DELETE,
        AFTER_DELETE
    }

    public void fireMetadataBeforeSaveEvent(NoCodeEventArgs args){
        this.fire(EventType.BEFORE_SAVE,args);
    }

    public void fireMetadataAfterSaveEvent(NoCodeEventArgs args){
        this.fire(EventType.AFTER_SAVE,args);
    }

    public void fireMetadataBeforeDeleteEvent(NoCodeEventArgs args){
        this.fire(EventType.BEFORE_DELETE,args);
    }

    public void fireMetadataAfterDeleteEvent(NoCodeEventArgs args){
        this.fire(EventType.AFTER_DELETE,args);
    }

    @Override
    public String getEventManagerName() {
        return NO_CODE_SERVICE_EVENT_MANAGER_NAME;
    }

    @Override
    public boolean isHandlerListener(IEventListener listener) {
        return listener instanceof NoCodeServiceEventListener;
    }

    @Override
    public void addListener(IEventListener listener) {
        if(!(listener instanceof NoCodeServiceEventListener)){
            throw new LcmEventException(ErrorCodes.ECP_EVENT_0001, listener.getClass().getName(), NoCodeServiceEventListener.class.getName());
        }
        NoCodeServiceEventListener eventListener=(NoCodeServiceEventListener)listener;
        this.addEventHandler(EventType.BEFORE_SAVE,eventListener,"fireMetadataBeforeSaveEvent");
        this.addEventHandler(EventType.AFTER_SAVE,eventListener,"fireMetadataAfterSaveEvent");
        this.addEventHandler(EventType.BEFORE_DELETE,eventListener,"fireMetadataBeforeDeleteEvent");
        this.addEventHandler(EventType.AFTER_DELETE,eventListener,"fireMetadataAfterDeleteEvent");
    }

    @Override
    public void removeListener(IEventListener listener) {
        if(!(listener instanceof NoCodeServiceEventListener)){
            throw new LcmEventException(ErrorCodes.ECP_EVENT_0001, listener.getClass().getName(), NoCodeServiceEventListener.class.getName());
        }
        NoCodeServiceEventListener eventListener=(NoCodeServiceEventListener)listener;
        this.removeEventHandler(EventType.BEFORE_SAVE,eventListener,"fireMetadataBeforeSaveEvent");
        this.removeEventHandler(EventType.AFTER_SAVE,eventListener,"fireMetadataAfterSaveEvent");
        this.removeEventHandler(EventType.BEFORE_DELETE,eventListener,"fireMetadataBeforeDeleteEvent");
        this.removeEventHandler(EventType.AFTER_DELETE,eventListener,"fireMetadataAfterDeleteEvent");
    }

}
