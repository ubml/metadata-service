/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.metadata.rtcustomization.serverapi;

import com.inspur.edp.lcm.metadata.api.entity.*;
import com.inspur.edp.metadata.rtcustomization.api.entity.DimensionExtendEntity;
import com.inspur.edp.metadata.rtcustomization.api.entity.GspMdpkg;
import com.inspur.edp.metadata.rtcustomization.api.entity.MetadataFilter;
import com.inspur.edp.metadata.rtcustomization.api.entity.MetadataStringDto;
import io.iec.edp.caf.rpc.api.annotation.GspServiceBundle;
import io.iec.edp.caf.rpc.api.annotation.RpcParam;

import java.io.File;
import java.util.List;
import java.util.Map;

@GspServiceBundle(applicationName = "runtime",serviceUnitName = "Lcm",serviceName = "metadata-customization-rt")
public interface CustomizationRtServerService {

    /**
     * @param metadataId 元数据id
     * @return 元数据传输实体
     */
    MetadataDto getMetadataDto(@RpcParam(paramName = "metadataId") String metadataId);

    /**
     * 根据元数据唯一标识获取元数据
     *
     * @param metadataId 元数据ID
     * @return 元数据实体
     */
    GspMetadata getMetadata(@RpcParam(paramName = "metadataId") String metadataId);

    GspMetadata getMetadataWithoutI18n(@RpcParam(paramName = "metadataId") String metadataId);

    GspMetadata getGenerateMetadataWithoutI18n(String metadataId);

    MetadataStringDto getMetadataStringDto(@RpcParam(paramName = "metadataId") String metadataId);

    GspMetadata getMetadataFromMdpkg(String mdpkgPath, String fileName);

    /**
     * 根据元数据唯一标识获取元数据（带有国际化），并存入缓存中
     *
     * @param metadataId 元数据ID
     * @return void
     */
    void saveMetadataToCache(@RpcParam(paramName="metadataId") String metadataId);

    /**
     * 根据元数据唯一标识获取元数据（不带国际化），并存入缓存中
     *
     * @param metadataId 元数据ID
     * @return void
     */
    void saveMetadataWithoutI18nToCache(@RpcParam(paramName="metadataId") String metadataId);

    /**
     * 获取指定类型的运行时定制元数据
     * @param metadataTypes 元数据类型列表
     * @return 根据元数据类型获取所有运行时生成的元数据信息列表
     * */
    List<Metadata4Ref> getAllCustomizedRtMetadataInfoWithTypes(@RpcParam(paramName="metadataTypes")String metadataTypes);

    /**
     * 根据扩展元数据获取被扩展的元数据
     *
     * @param metadataId 元数据id
     * @param certId     证书id
     * @return 元数据实体
     */
    List<DimensionExtendEntity> getExtMdByBasicMdId(@RpcParam(paramName="metadataId") String metadataId, @RpcParam(paramName="certId") String certId);

    /**
     * 预加载元数据
     * @param metadataFilter 为null则不进行过滤。其中包括suCodes,metadataTypes,processModes，均为list，为null则不过滤。
     */
    void preloadMetadata(MetadataFilter metadataFilter);

    /**
     * 过滤获取所有元数据
     * @param metadataFilters 为null则不进行过滤。其中包括suCodes,metadataTypes,processModes，均为list，为null则不过滤。
     * @return 元数据列表
     */
    List<Metadata4Ref> getMetadataListByFilters(List<MetadataFilter> metadataFilters);

    /**
     * 过滤获取所有元数据
     * @param metadataFilter 为null则不进行过滤。其中包括suCodes,metadataTypes,processModes，均为list，为null则不过滤。
     * @return 元数据列表
     */
    List<Metadata4Ref> getMetadataListByFilter(MetadataFilter metadataFilter);

    List<Metadata4Ref> getMetadataListByMdpkgName(String mdpkgName);

    /**
     * 将元数据包信息和元数据信息更新到数据库中
     * @param mdpkgFile 元数据包文件
     */
    void saveMdpkg(File mdpkgFile, boolean isPatchAssembly);

    /**
     * 根据元数据ID获取SU信息
     *
     * @param metadataId 元数据ID
     * @return 元数据所属SU信息
     */
    ServiceUnitInfo getServiceUnitInfo(String metadataId);

    /**
     * 获取路径下元数据包列表
     * @param path 路径
     * @return 元数据包列表
     */
    Map<String, MetadataPackage> getMetadataPackagesRecursivly(String path);

    List<GspMdpkg> findAllLastChangedOnFromMdpkg();

    MetadataPackage findMetadataPackageByName(String packageName);

    void preloadAllGeneratedMetadata();

    void saveSuInfoToCache();

    /**
     * 更新元数据包中的元数据
     * @param metadata      元数据实体
     */
    void updateMetadata(GspMetadata metadata);

    /**
     * 根据元数据编码列表删除其缓存
     *
     * @param metadataIds 待删除的元数据编码集合
     * @author sunhongfei01
     * @date 2021-7-20
     */
    void removeCacheByMetadataIds(List<String> metadataIds);

    void initCache();

    void afterMdpkgChanged(boolean isFirst);

    void afterMdpkgChanged(MetadataPackage metadataPackage);

    void makeUnique();

    List<MetadataHeader> getMetadatasByRefedMetadataId(String metadataId);

    /**
     * 根据引用的元数据id查找引用此元数据的元数据列表
     * @param metadataId
     * @param metadataTypes 获取的类型列表，如为null，则全部获取
     * @return
     */
    List<MetadataHeader> getMetadatasByRefedMetadataIdAndMetadataTypes(String metadataId, List<String> metadataTypes);

    void initMdRefs();

    /**
     * 设计时查询依赖引用元数据时使用,无需缓存;仅查询已发布的运行时定制元数据
     *
     * @param metadataId 运行时定制元数据ID
     * @return 已发布的运行时定制元数据实体
     */
    GspMetadata getCustomizedMetadata(String metadataId);

    /**
     * IDE设计时选择元数据使用，无需加缓存；只查mdpkg类型的数据
     *
     * @param metadataId 元数据ID
     * @return 元数据所属SU信息
     */
    Metadata4Ref getMetadata4Ref(String metadataId);

    /**
     * 获取元数据包信息，带有来源类型（包里的、运行时生成的、运行时定制的、零代码的）
     * 如果元数据是包元数据，则返回元数据包信息，不带content
     * 如果元数据是包元数据，但是缺少包信息，则抛异常
     * 如果元数据不是包元数据，是运行时定制发布的元数据，或者是零代码发布的元数据，或者是运行时生成的元数据，则返回Metadata4Ref，但是其中的MetadataPackageHeader是空
     * 如果元数据不存在，则返回null
     * 当前给低零代码融合使用，零代码转成低代码工程时，根据元数据依赖关系，给工程添加数据库引用，需要使用这个接口获取包信息和引用类型
     * @param metadataId		元数据id
     * @return Metadata4Ref     元数据包信息，不带元数据包信息的Metadata4Ref，或者null
     */
    Metadata4Ref getMetadata4RefWithSourceType(@RpcParam(paramName="metadataId") String metadataId);

    /**
     * 根据id获取元数据引用(不带content)
     * @param metadataId
     * @return
     */
    @Deprecated
    Metadata4Ref getMetadata4RefById(String metadataId);
}
