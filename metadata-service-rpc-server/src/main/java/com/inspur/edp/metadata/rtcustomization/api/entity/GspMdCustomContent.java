/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.metadata.rtcustomization.api.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.UUID;


/**
 * @author zhaoleitr
 */
@Entity
@Table
public class GspMdCustomContent {
    @Id
    private String id;
    private String code;
    private String name;
    private String type;
    private String bizObjId;
    private String content;
    private String createBy;
    private LocalDateTime createdOn;
    private String lastChangedBy;
    private LocalDateTime lastChangedOn;
    private Character isExtend;
    private String nameSpace;
    private String extendProperty;

    public GspMdCustomContent() {}

    public GspMdCustomContent(String id, String createBy) {
        this.id = id;
        this.createdOn = LocalDateTime.now();
        this.createBy = createBy;
    }

    public GspMdCustomContent(String id, LocalDateTime lastChangedOn) {
        this.id = id;
        this.lastChangedOn = lastChangedOn;
    }

    public GspMdCustomContent(String id,
                              String code,
                              String name,
                              String type,
                              String bizObjId,
                              Character isExtend,
                              String nameSpace) {
        this.id = id;
        this.code = code;
        this.name = name;
        this.type = type;
        this.bizObjId = bizObjId;
        this.isExtend = isExtend;
        this.nameSpace = nameSpace;
    }
    public GspMdCustomContent update(String code, String name, String type, String bizObjId, String content, String lastChangedBy, LocalDateTime lastChangedOn, Character isExtend, String nameSpace, String extendProperty) {
        this.code = code;
        this.name = name;
        this.type = type;
        this.bizObjId = bizObjId;
        this.content = content;
        this.lastChangedBy = lastChangedBy;
        this.lastChangedOn = lastChangedOn;
        this.isExtend = isExtend;
        this.nameSpace = nameSpace;
        this.extendProperty = extendProperty;
        return this;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBizObjId() {
        return bizObjId;
    }

    public void setBizObjId(String bizObjId) {
        this.bizObjId = bizObjId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public LocalDateTime getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(LocalDateTime createdOn) {
        this.createdOn = createdOn;
    }

    public String getLastChangedBy() {
        return lastChangedBy;
    }

    public void setLastChangedBy(String lastChangedBy) {
        this.lastChangedBy = lastChangedBy;
    }

    public LocalDateTime getLastChangedOn() {
        return lastChangedOn;
    }

    public void setLastChangedOn(LocalDateTime lastChangedOn) {
        this.lastChangedOn = lastChangedOn;
    }

    public Character getIsExtend() {
        return isExtend;
    }

    public void setIsExtend(Character isExtend) {
        this.isExtend = isExtend;
    }

    public String getNameSpace() {
        return nameSpace;
    }

    public void setNameSpace(String nameSpace) {
        this.nameSpace = nameSpace;
    }

    public String getExtendProperty() {
        return extendProperty;
    }

    public void setExtendProperty(String extendProperty) {
        this.extendProperty = extendProperty;
    }
}
