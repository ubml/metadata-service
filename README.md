# metadata-service

#### 介绍
业务处理过程依赖具体的元数据内容，在不同处理环节需要解析不同类型的元数据。元数据服务提供统一的运行时元数据服务接口，支持运行时元数据加载、外部推送元数据保存、运行时元数据扩展等服务。

#### 软件架构

业务处理过程依赖元数据RPC Client服务获取要使用的元数据。

![serviceinvokestructure](./imgs/serviceinvokestructure.png)

元数据服务主要包括元数据获取与查询服务、缓存处理（加载、更新）、外部系统推送元数据保存、元数据部署服务、运行时元数据扩展等服务。

![servicestructure](./imgs/metadataservicestructure.png)

[//]: # (#### 使用)
[//]: # ()
[//]: # (（待补充）)

##### Maven dependency

```xml
<properties>
    <metadata.service.api.version>0.1.0</metadata.service.api.version>
</properties>
```
```xml
<dependencies>
    <dependency>
        <groupId>com.inspur.edp</groupId>
        <artifactId>metadata-service-api</artifactId>
        <version>${metadata.service.api.version}</version>
    </dependency>
</dependencies>
```

### 参与贡献
参与贡献，请参照[如何贡献](https://gitee.com/ubml/community/blob/master/zh-cn/CONTRIBUTING.md)获取帮助。

### License
[Apache License 2.0](https://gitee.com/ubml/metadata-common/blob/master/LICENSE)




