/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.metadata.rtcustomization.common;

import com.inspur.edp.lcm.metadata.api.entity.*;
import com.inspur.edp.lcm.metadata.common.MetadataDtoConverter;
import com.inspur.edp.metadata.rtcustomization.api.AbstractCustomizedContent;
import com.inspur.edp.metadata.rtcustomization.api.entity.DimensionExtendEntity;
import com.inspur.edp.metadata.rtcustomization.api.entity.DimensionExtendEntityDto;
import com.inspur.edp.metadata.rtcustomization.api.entity.GspMdChangeset;
import com.inspur.edp.metadata.rtcustomization.api.exception.LcmConfigResolveException;
import com.inspur.edp.metadata.rtcustomization.api.exception.ErrorCodes;
import com.inspur.edp.metadata.rtcustomization.spi.CustomizationSerializer;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class MetadataCustomizationUtils {
	public static final String RESOURCE_METADATA_TYPE = "ResourceMetadata";
	public static final String LINGUISTIC_RESOURCE_METADATA_TYPE = "LinguisticResource";
	private static final String FORM_METADATA_TYPE = "Form";
	private static final String MOBILEFORM_METADATA_TYPE = "MobileForm";
	public DimensionExtendEntity buildDimensionExtendEntity(DimensionExtendEntityDto dto) {
		DimensionExtendEntity extendEntity = new DimensionExtendEntity();
		extendEntity.setFirstDimension(dto.getFirstDimension());
		extendEntity.setSecondDimension(dto.getSecondDimension());
		extendEntity.setBasicMetadataId(dto.getBasicMetadataId());
		extendEntity.setBasicMetadataCertId(dto.getBasicMetadataCertId());
		extendEntity.setBasicMetadataCode(dto.getBasicMetadataCode());
		extendEntity.setBasicMetadataNamespace(dto.getBasicMetadataNamespace());
		extendEntity.setBasicMetadataVersion(dto.getBasicMetadataVersion());
		extendEntity.setBasicMetadataTypeStr(dto.getBasicMetadataTypeStr());
		extendEntity.setExtendMetadataEntity(MetadataDtoConverter.asMetadata(dto.getMetadataDto()));
		extendEntity.setFirstDimensionName(dto.getFirstDimensionName());
		extendEntity.setFirstDimensionCode(dto.getFirstDimensionCode());
		extendEntity.setSecondDimensionCode(dto.getSecondDimensionCode());
		extendEntity.setSecondDimensionName(dto.getSecondDimensionName());
		return extendEntity;
	}

	public DimensionExtendEntityDto buildDimensionExtendEntityDto(DimensionExtendEntity dimExtEntity) {
		if(dimExtEntity==null||dimExtEntity.getExtendMetadataEntity()==null){
			return null;
		}
		DimensionExtendEntityDto dto = new DimensionExtendEntityDto();
		MetadataDto metadataDto = MetadataDtoConverter.asDto(dimExtEntity.getExtendMetadataEntity());
		dto.setBasicMetadataId(dimExtEntity.getBasicMetadataId());
		dto.setBasicMetadataCertId(dimExtEntity.getBasicMetadataCertId());
		dto.setBasicMetadataCode(dimExtEntity.getBasicMetadataCode());
		dto.setBasicMetadataNamespace(dimExtEntity.getBasicMetadataNamespace());
		dto.setBasicMetadataTypeStr(dimExtEntity.getBasicMetadataTypeStr());
		dto.setBasicMetadataVersion(dimExtEntity.getBasicMetadataVersion());
		dto.setFirstDimension(dimExtEntity.getFirstDimension());
		dto.setSecondDimension(dimExtEntity.getSecondDimension());
		dto.setMetadataDto(metadataDto);
		dto.setFirstDimensionName(dimExtEntity.getFirstDimensionName());
		dto.setFirstDimensionCode(dimExtEntity.getFirstDimensionCode());
		dto.setSecondDimensionCode(dimExtEntity.getSecondDimensionCode());
		dto.setSecondDimensionName(dimExtEntity.getSecondDimensionName());
		return dto;
	}

	public AbstractCustomizedContent buildAbstractCustomizdContet(GspMdChangeset changeset) {
		try {
			if(changeset==null){
				return null;
			}
			//序列化
			AbstractCustomizedContent customizedContent = null;
			CustomizationSerializer manager = CustomizedContentSerializerHelper.getInstance().getManager(changeset.getType());
			if (manager == null) {
				throw new LcmConfigResolveException(ErrorCodes.ECP_CONFIG_RESOLVE_0003, changeset.getType());
			}
			customizedContent =(AbstractCustomizedContent) manager.deSerialize(changeset.getContent());
			return customizedContent;
		}
		catch (Exception e) {
			throw new LcmConfigResolveException(e, ErrorCodes.ECP_CONFIG_RESOLVE_0004, changeset.getId());
		}
	}

	public List<Metadata4RefDto> buildMetadata4RefDto(List<Metadata4Ref> metadata4Refs){
		if(metadata4Refs==null ||metadata4Refs.size()<=0){
			return null;
		}
		List<Metadata4RefDto> metadata4RefDtos=new ArrayList<>();
		for(Metadata4Ref metadata4Ref:metadata4Refs){
			Metadata4RefDto dto = new Metadata4RefDto();
			MetadataDto metadataDto = MetadataDtoConverter.asDto(metadata4Ref.getMetadata());
			dto.setPackageHeader(metadata4Ref.getPackageHeader());
			dto.setMetadata(metadataDto);
			dto.setServiceUnitInfo(metadata4Ref.getServiceUnitInfo());
			metadata4RefDtos.add(dto);
		}

		return metadata4RefDtos;
	}

	public List<DimensionExtendEntityDto> buildDimensionExtendEntityDto(List<DimensionExtendEntity> entities){
		if(entities==null||entities.size()<=0){
			return null;
		}
		List<DimensionExtendEntityDto> dtos=new ArrayList<>();
		for (DimensionExtendEntity entity :entities){
			if(entity==null){
				continue;
			}
			dtos.add(buildDimensionExtendEntityDto(entity));
		}
		return dtos;
	}
	public boolean isValidMd(Metadata4Ref metadata4Ref, String text) {

		if (StringUtils.isEmpty(text)) {
			return true;
		}
		if (isMath(metadata4Ref.getMetadata().getHeader().getCode(), text)
				|| isMath(metadata4Ref.getMetadata().getHeader().getName(), text)
				|| Objects.nonNull(metadata4Ref.getPackageHeader()) && isMath(metadata4Ref.getPackageHeader().getName(), text)) {
			return true;
		}
		return false;
	}
	private boolean isMath(String a, String b) {
		if (StringUtils.isEmpty(a)) {
			return false;
		}
		return a.toLowerCase().contains(b.toLowerCase());
	}

	/**
	 * 获取资源元数据引用关系
	 * @param metadata
	 * @return
	 */
	public static MetadataReference findResourceMetatadaRef(GspMetadata metadata) {
		if(CollectionUtils.isEmpty(metadata.getRefs())) {
			return null;
		}
		return metadata.getRefs().stream().filter(ref -> {
			if(ref==null || ref.getDependentMetadata()==null){
				return false;
			}
			return RESOURCE_METADATA_TYPE.equals(ref.getDependentMetadata().getType());
		}).findFirst().orElse(null);
	}
	/**
	 * 获取资源元数据引用关系
	 * @param metadata
	 * @return
	 */
	public static boolean isFormTypeMetadata(GspMetadata metadata) {
		String metadataType = metadata.getHeader().getType();
		return FORM_METADATA_TYPE.equals(metadataType) || MOBILEFORM_METADATA_TYPE.equals(metadataType);
	}
}
