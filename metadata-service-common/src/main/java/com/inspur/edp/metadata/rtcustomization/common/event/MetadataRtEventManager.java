/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.metadata.rtcustomization.common.event;

import com.inspur.edp.metadata.rtcustomization.api.exception.ErrorCodes;
import com.inspur.edp.metadata.rtcustomization.api.exception.LcmEventException;
import com.inspur.edp.metadata.rtcustomization.spi.event.IMetadataRtEventListener;
import io.iec.edp.caf.commons.event.CAFEventArgs;
import io.iec.edp.caf.commons.event.EventManager;
import io.iec.edp.caf.commons.event.IEventListener;

public class MetadataRtEventManager  extends EventManager {

	/**
	 * 内部枚举类
	 */
	enum MetadataRtEventType{
		fireMetadataSavingEvent,
		fireMetadataSavedEvent,
		fireMetadataDeletingEvent,
		fireMetadataDeletedEvent,
		fireGeneratedMetadataSavingEvent,
		fireGeneratedMetadataSavedEvent,
		fireMetadataAchievedEvent;
	}

	public void fireMetadataSavingEvent(CAFEventArgs e){
		this.fire(MetadataRtEventType.fireMetadataSavingEvent,e);
	}

	public void fireMetadataSavedEvent(CAFEventArgs e){
		this.fire(MetadataRtEventType.fireMetadataSavedEvent,e);
	}

	public void fireMetadataDeletingEvent(CAFEventArgs e){
		this.fire(MetadataRtEventType.fireMetadataDeletingEvent,e);
	}

	public void fireMetadataDeletedEvent(CAFEventArgs e){
		this.fire(MetadataRtEventType.fireMetadataDeletedEvent,e);
	}

	public void fireGeneratedMetadataSavingEvent(CAFEventArgs e){
		this.fire(MetadataRtEventType.fireGeneratedMetadataSavingEvent,e);
	}

	public void fireGeneratedMetadataSavedEvent(CAFEventArgs e){
		this.fire(MetadataRtEventType.fireGeneratedMetadataSavedEvent,e);
	}

	public void fireMetadataAchievedEvent(CAFEventArgs e){
		this.fire(MetadataRtEventType.fireMetadataAchievedEvent,e);
	}

	/**
	 * 实现抽象方法getCode(),获取事件管理对象的编码
	 * @return 事件管理对象的编码
	 */
	@Override
	public String getEventManagerName() {
		return "MetadataRtEventManager";
	}

	/**
	 * 当前的事件管理者是否处理输入的事件监听者对象
	 * @param listener 事件监听者
	 * @return 若处理，则返回true,否则返回false
	 */
	@Override
	public boolean isHandlerListener(IEventListener listener) {
		return listener instanceof IMetadataRtEventListener;
	}

	/**
	 * 注册事件
	 * @param listener 监听者
	 */
	@Override
	public void addListener(IEventListener listener) {
		if(listener instanceof IMetadataRtEventListener==false){
			throw new LcmEventException(ErrorCodes.ECP_EVENT_0001, listener.getClass().getName(), IMetadataRtEventListener.class.getName());
		}
		IMetadataRtEventListener metadataRtEventListener=(IMetadataRtEventListener)listener;
		this.addEventHandler(MetadataRtEventType.fireMetadataSavingEvent,metadataRtEventListener,"fireMetadataSavingEvent");
		this.addEventHandler(MetadataRtEventType.fireMetadataSavedEvent,metadataRtEventListener,"fireMetadataSavedEvent");
		this.addEventHandler(MetadataRtEventType.fireMetadataDeletingEvent,metadataRtEventListener,"fireMetadataDeletingEvent");
		this.addEventHandler(MetadataRtEventType.fireMetadataDeletedEvent,metadataRtEventListener,"fireMetadataDeletedEvent");
		this.addEventHandler(MetadataRtEventType.fireGeneratedMetadataSavingEvent,metadataRtEventListener,"fireGeneratedMetadataSavingEvent");
		this.addEventHandler(MetadataRtEventType.fireGeneratedMetadataSavedEvent,metadataRtEventListener,"fireGeneratedMetadataSavedEvent");
		this.addEventHandler(MetadataRtEventType.fireMetadataAchievedEvent,metadataRtEventListener,"fireMetadataAchievedEvent");
	}

	/***
	 * 注销事件
	 * @param listener  监听者
	 */
	@Override
	public void removeListener(IEventListener listener) {
		if(listener instanceof IMetadataRtEventListener==false){
			throw new LcmEventException(ErrorCodes.ECP_EVENT_0001, listener.getClass().getName(), IMetadataRtEventListener.class.getName());
		}
		IMetadataRtEventListener metadataRtEventListener=(IMetadataRtEventListener)listener;
		this.removeEventHandler(MetadataRtEventType.fireMetadataSavingEvent,metadataRtEventListener,"fireMetadataSavingEvent");
		this.removeEventHandler(MetadataRtEventType.fireMetadataSavedEvent,metadataRtEventListener,"fireMetadataSavedEvent");
		this.removeEventHandler(MetadataRtEventType.fireMetadataDeletingEvent,metadataRtEventListener,"fireMetadataDeletingEvent");
		this.removeEventHandler(MetadataRtEventType.fireMetadataDeletedEvent,metadataRtEventListener,"fireMetadataDeletedEvent");
		this.removeEventHandler(MetadataRtEventType.fireGeneratedMetadataSavingEvent,metadataRtEventListener,"fireGeneratedMetadataSavingEvent");
		this.removeEventHandler(MetadataRtEventType.fireGeneratedMetadataSavedEvent,metadataRtEventListener,"fireGeneratedMetadataSavedEvent");
		this.removeEventHandler(MetadataRtEventType.fireMetadataAchievedEvent,metadataRtEventListener,"fireMetadataAchievedEvent");
	}
}
