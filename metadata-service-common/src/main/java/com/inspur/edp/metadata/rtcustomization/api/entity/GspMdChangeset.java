/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.metadata.rtcustomization.api.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Data
@Entity
@Table
public class GspMdChangeset {
    @Id
    private String id;
    private String metadataId;
    private String certId;
    private String version;
    private String relatedVersion;
    private String previousVersion;
    private String code;
    private String name;
    private String type;
    private String bizObjId;
    private String content;
    private String createBy;
    private LocalDateTime createdOn;
    private String lastChangedBy;
    private LocalDateTime lastChangedOn;
    private boolean extended;
    private String nameSpace;
    private String extendProperty;
}
