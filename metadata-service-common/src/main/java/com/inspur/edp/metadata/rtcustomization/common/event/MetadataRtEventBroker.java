/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.metadata.rtcustomization.common.event;

import io.iec.edp.caf.commons.event.CAFEventArgs;
import io.iec.edp.caf.commons.event.EventBroker;
import io.iec.edp.caf.commons.event.config.EventListenerSettings;

public class MetadataRtEventBroker extends EventBroker {
	public MetadataRtEventManager metadataRtEventManager;
	public MetadataRtEventBroker(EventListenerSettings settings){
		super(settings);
		this.metadataRtEventManager=new MetadataRtEventManager();
		this.init();
	}

	/**
	 * 触发事件
	 * @param e 事件参数
	 * @return
	 */
	public void fireMetadataSavingEvent(CAFEventArgs e)
	{
		this.metadataRtEventManager.fireMetadataSavingEvent(e);

	}
	public void fireMetadataSavedEvent(CAFEventArgs e)
	{
		this.metadataRtEventManager.fireMetadataSavedEvent(e);
	}

	public void fireMetadataDeletingEvent(CAFEventArgs e){
		this.metadataRtEventManager.fireMetadataDeletingEvent(e);
	}

	public void fireMetadataDeletedEvent(CAFEventArgs e){
		this.metadataRtEventManager.fireMetadataDeletedEvent(e);
	}

	public void fireGeneratedMetadataSavingEvent(CAFEventArgs e){
		this.metadataRtEventManager.fireGeneratedMetadataSavingEvent(e);
	}

	public void fireGeneratedMetadataSavedEvent(CAFEventArgs e){
		this.metadataRtEventManager.fireGeneratedMetadataSavedEvent(e);
	}

	public void fireMetadataAchievedEvent(CAFEventArgs e){
		this.metadataRtEventManager.fireMetadataAchievedEvent(e);
	}
	/**
	 * 初始化
	 */
	@Override
	protected void onInit(){
		this.eventManagerCollection.add(metadataRtEventManager);
	}

}
