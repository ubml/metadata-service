/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.metadata.rtcustomization.common;

import com.inspur.edp.lcm.metadata.common.FileServiceImp;
import com.inspur.edp.lcm.metadata.common.ServiceUtils;

/**
 * desc
 *
 * @author liangff
 * @since
 */
public class ScheduledConfigLoader {
    private static final String FILE_NAME = "config/platform/common/lcm_scheduled.json";
    private static final String SCHEDULED_SECTION_NAME = "Scheduled";

    public static boolean isScheduledStart() {
        return FileServiceImp.readSection(ServiceUtils.getAbsoluteConfigFilePath(FILE_NAME), SCHEDULED_SECTION_NAME).asBoolean();
    }
}
