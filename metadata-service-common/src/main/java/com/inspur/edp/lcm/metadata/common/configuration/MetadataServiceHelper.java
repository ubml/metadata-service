/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.lcm.metadata.common.configuration;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.BeanDescription;
import com.fasterxml.jackson.databind.DeserializationConfig;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.BeanDeserializerModifier;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.inspur.edp.lcm.metadata.api.entity.MetadataType;
import com.inspur.edp.lcm.metadata.api.entity.MetadataTypeDeserializer;
import com.inspur.edp.lcm.metadata.common.FileServiceImp;
import com.inspur.edp.lcm.metadata.common.ServiceUtils;
import com.inspur.edp.metadata.rtcustomization.api.exception.LcmConfigResolveException;
import com.inspur.edp.metadata.rtcustomization.api.exception.ErrorCodes;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * 元数据序列化器帮助
 *
 * @author zhongchq
 * @since 1.0
 */
public class MetadataServiceHelper {

    public static <T> List<T> getConfigurationList(String path, String sectionName, Class<T> clazz) throws IOException {
        try {
            String configuration = FileServiceImp.readSection(path, sectionName).toString();
            ObjectMapper objectMapper = ServiceUtils.getMapper();
            SimpleModule module = new SimpleModule();
            module.setDeserializerModifier(new BeanDeserializerModifier() {
                @Override
                public JsonDeserializer<?> modifyDeserializer(DeserializationConfig config, BeanDescription beanDesc, JsonDeserializer<?> deserializer) {
                    if (beanDesc.getBeanClass() == MetadataType.class) {
                        return new MetadataTypeDeserializer(deserializer);
                    }
                    return deserializer;
                }
            });
            objectMapper.registerModule(module);
            CollectionType collectionType = objectMapper.getTypeFactory().constructCollectionType(List.class, clazz);
            return objectMapper.readValue(configuration, collectionType);
        } catch (Exception e) {
            throw new LcmConfigResolveException(e, ErrorCodes.ECP_CONFIG_RESOLVE_0002, path);
        }
    }

    public static Map<String, List<String>> getConfigurationMap(String path, String sectionName) throws IOException {
        try {
            String metadataConfiguration = FileServiceImp.readSection(path, sectionName).toString();
            return ServiceUtils.getMapper().readValue(metadataConfiguration, new TypeReference<Map<String, List<String>>>() {
            });
        }
        catch (Exception e) {
            throw new LcmConfigResolveException(e, ErrorCodes.ECP_CONFIG_RESOLVE_0002, path);
        }

    }

    public static String getConfigurationString(String path, String sectionName) {
        return FileServiceImp.readSection(path, sectionName).asText();
    }


}
