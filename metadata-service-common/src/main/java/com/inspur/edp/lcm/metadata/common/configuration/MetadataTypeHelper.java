/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.lcm.metadata.common.configuration;

import com.inspur.edp.lcm.metadata.api.entity.MetadataType;
import com.inspur.edp.lcm.metadata.api.entity.OperationEnum;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 元数据类型配置帮助
 * @author zhongchq
 * @since 1.0
 */
public class MetadataTypeHelper extends MetadataTypeConfigLoader {

    private static MetadataTypeHelper singleton = null;

    public MetadataTypeHelper(){}

    public static MetadataTypeHelper getInstance(){
        if (singleton == null){
            singleton = new MetadataTypeHelper();
        }
        return singleton;
    }

    public List<MetadataType> getMetadataTypes(){
        return loadMetadataConfigurations();
    }

    public MetadataType getMetadataType(String typeCode) {
        List<MetadataType> metadataTypeList = loadMetadataConfigurations();
        return metadataTypeList.stream().filter(type -> type.getTypeCode().equals(typeCode)).findFirst().orElse(null);
    }

    public List<MetadataType> getMetadataTypes(OperationEnum operationEnum) {
        List<MetadataType> metadataTypeList = loadMetadataConfigurations();
        return metadataTypeList.stream().filter(type -> type.getOperationEnum().contains(operationEnum.toString())).collect(Collectors.toList());
    }
}
