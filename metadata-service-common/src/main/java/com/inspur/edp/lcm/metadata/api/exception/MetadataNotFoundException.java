/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.lcm.metadata.api.exception;

import com.inspur.edp.lcm.metadata.api.entity.uri.AbstractURI;
import com.inspur.edp.lcm.metadata.api.entity.uri.MetadataURI;
import io.iec.edp.caf.commons.exception.CAFRuntimeException;


public class MetadataNotFoundException extends CAFRuntimeException {
    private MetadataURI targetURI;

    private AbstractURI sourceURI;

    private String projectPath;

    public MetadataNotFoundException(String serviceUnitCode, String exceptionCode, String message, Exception innerException) {
        super(serviceUnitCode, exceptionCode, message, innerException);
    }

    public MetadataNotFoundException(String serviceUnitCode, String exceptionCode, String message, Exception innerException, MetadataURI targetURI, AbstractURI sourceURI, String projectPath) {
        super(serviceUnitCode, exceptionCode, message, innerException);
        this.targetURI = targetURI;
        this.sourceURI = sourceURI;
        this.projectPath = projectPath;
    }

    public MetadataURI getTargetURI() {
        return targetURI;
    }

    public void setTargetURI(MetadataURI targetURI) {
        this.targetURI = targetURI;
    }

    public AbstractURI getSourceURI() {
        return sourceURI;
    }

    public void setSourceURI(AbstractURI sourceURI) {
        this.sourceURI = sourceURI;
    }

    public String getProjectPath() {
        return projectPath;
    }

    public void setProjectPath(String projectPath) {
        this.projectPath = projectPath;
    }

    @Override
    public String toString() {
        String s = getClass().getName();
        String message = getLocalizedMessage();
        return (message == null) ? s : message;
    }
}
