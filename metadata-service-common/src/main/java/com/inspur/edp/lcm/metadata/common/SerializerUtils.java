/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.lcm.metadata.common;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.inspur.edp.metadata.rtcustomization.api.exception.ErrorCodes;
import com.inspur.edp.metadata.rtcustomization.api.exception.LcmFileException;
import com.inspur.edp.metadata.rtcustomization.api.exception.LcmMetadataException;
import com.inspur.edp.metadata.rtcustomization.api.exception.LcmParseException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

import java.io.File;
import java.io.IOException;
import java.util.List;

@Slf4j
public class SerializerUtils {
    private static final ObjectMapper mapper = ServiceUtils.getMapper();

    public static String serialize(Object o) {
        try {
            return mapper.writeValueAsString(o);
        } catch (JsonProcessingException e) {
            throw new LcmParseException(e, ErrorCodes.ECP_PARSE_0001);
        }
    }

    public static <T> T deserialize(String s, Class<T> clazz) {
        try {
            return mapper.readValue(s, clazz);
        } catch (JsonProcessingException e) {
            throw new LcmParseException(e, ErrorCodes.ECP_PARSE_0002, s);
        }
    }

    public static JsonNode readFile(File file) {
        ServiceUtils.checkExist(file, "文件不存在：" + file);
        try {
            return mapper.readTree(file);
        } catch (IOException e) {
            throw new LcmFileException(e, ErrorCodes.ECP_FILE_0001, file.getPath());
        }
    }

    public static <T> T readFile(File file, Class<T> clazz) {
        ServiceUtils.checkExist(file, "文件不存在：" + file);
        try {
            return mapper.readValue(file, clazz);
        } catch (IOException e) {
            throw new LcmFileException(e, ErrorCodes.ECP_FILE_0001, file.getPath());
        }
    }

    public static <T> void writeFile(File file, T t) {
        ServiceUtils.checkExist(file, "文件不存在：" + file);
        try {
            mapper.writeValue(file, t);
        } catch (IOException e) {
            throw new LcmFileException(e, ErrorCodes.ECP_FILE_0002, file.getPath());
        }
    }

    public static <T> T readSection(File file, String sectionName, Class<T> clazz) {
        ServiceUtils.checkExist(file, "文件不存在：" + file);
        JsonNode sectionJsonNode = FileServiceImp.readSection(file.getPath(), sectionName);
        if (sectionJsonNode == null) {
            throw new LcmFileException(ErrorCodes.ECP_FILE_0003, file.getPath(), sectionName);
        }
        String sectionString = sectionJsonNode.toString();
        return deserialize(sectionString, clazz);
    }

    public static <T> List<T> readSectionList(File file, String sectionName, Class<T> clazz) {
        ServiceUtils.checkExist(file, "文件不存在：" + file);
        JsonNode sectionJsonNode = FileServiceImp.readSection(file.getPath(), sectionName);
        if (sectionJsonNode == null) {
            throw new LcmFileException(ErrorCodes.ECP_FILE_0003, file.getPath(), sectionName);
        }
        String sectionString = sectionJsonNode.toString();
        return deserializeList(sectionString, clazz);
    }

    public static <T> List<T> deserializeList(String s, Class<T> clazz) {
        if (!StringUtils.hasLength(s)) {
            throw new LcmMetadataException(ErrorCodes.ECP_METADATA_0003, "s");
        }
        CollectionType collectionType = mapper.getTypeFactory().constructCollectionType(List.class, clazz);
        try {
            return mapper.readValue(s, collectionType);
        } catch (IOException e) {
            throw new LcmParseException(e, ErrorCodes.ECP_PARSE_0002, s);
        }
    }

    public static <T> List<T> readSectionList(File file, String sectionName, Class<T> clazz, SimpleModule module) {
        if (!file.exists()) {
            throw new LcmFileException(ErrorCodes.ECP_FILE_0004, file.getPath());
        }
        JsonNode sectionJsonNode = FileServiceImp.readSection(file.getPath(), sectionName);
        if (sectionJsonNode == null) {
            throw new LcmFileException(ErrorCodes.ECP_FILE_0003, file.getPath(), sectionName);
        }
        String sectionString = sectionJsonNode.toString();
        return deserializeList(sectionString, clazz, module);
    }

    public static <T> List<T> deserializeList(String s, Class<T> clazz, SimpleModule module) {
        if (!StringUtils.hasLength(s)) {
            throw new LcmMetadataException(ErrorCodes.ECP_METADATA_0003, "s");
        }
        ObjectMapper mapper = ServiceUtils.getMapper();
        if (module != null) {
            mapper.registerModule(module);
        }

        CollectionType collectionType = mapper.getTypeFactory().constructCollectionType(List.class, clazz);
        try {
            return mapper.readValue(s, collectionType);
        } catch (IOException e) {
            throw new LcmParseException(e, ErrorCodes.ECP_PARSE_0002, s);
        }
    }
}
