/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.inspur.edp.lcm.metadata.configuration;

/**
 * 客制化缓存帮助
 *
 * @author liuyuxin01
 * @since 0.1.35
 */
public class CustomizationCacheHelper extends CustomizationCacheConfigLoader {
    private static CustomizationCacheHelper helper = null;

    private CacheModeEnum cacheModeEnum = null;

    public static CustomizationCacheHelper getInstance() {
        if (helper == null) {
            helper = new CustomizationCacheHelper();
        }
        return helper;
    }

    public String getCacheKey(String key) {
        if (cacheModeEnum == null) {
            cacheModeEnum = getCacheMode();
        }
        return cacheModeEnum.getCacheKey(key);
    }
}
