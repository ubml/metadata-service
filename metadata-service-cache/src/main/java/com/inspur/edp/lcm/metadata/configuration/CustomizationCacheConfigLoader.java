/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.lcm.metadata.configuration;

import com.inspur.edp.lcm.metadata.common.FileServiceImp;
import com.inspur.edp.lcm.metadata.common.ServiceUtils;
import org.springframework.util.StringUtils;


/**
 * 客制化缓存配置加载器
 *
 * @author liuyuxin01
 * @since 0.1.35
 */
public class CustomizationCacheConfigLoader {
    private static final String FILE_NAME = "config/platform/common/lcm_customization_cache.json";
    private static final String SECTION_NAME = "CacheMode";
    private static final String PRELOAD_SECTION_NAME = "Preload";

    private String absoluteFilePath;
    private CacheModeEnum cacheMode = null;

    public String getAbsoluteFilePath() {
        if (!StringUtils.hasLength(absoluteFilePath)) {
            absoluteFilePath = ServiceUtils.getAbsoluteConfigFilePath(FILE_NAME);
        }
        return absoluteFilePath;
    }

    public CacheModeEnum getCacheMode() {
        if(cacheMode == null) {
            String name = FileServiceImp.readSection(getAbsoluteFilePath(), SECTION_NAME).asText();
            cacheMode = !StringUtils.hasLength(name) ? CacheModeEnum.SINGLE_TENANT : CacheModeEnum.valueOf(name);
        }
        return cacheMode;
    }

    public boolean isPreload() {
        return FileServiceImp.readSection(getAbsoluteFilePath(), PRELOAD_SECTION_NAME).asBoolean();
    }
}
