/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.lcm.metadata.cache;

import com.inspur.edp.lcm.metadata.common.serializer.TimeSerializer;
import io.iec.edp.caf.caching.api.Cache;
import io.iec.edp.caf.caching.api.CacheManager;
import io.iec.edp.caf.caching.enums.ExpireMode;
import io.iec.edp.caf.caching.setting.LayeringCacheSetting;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;

import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;

/**
 * desc
 *
 * @author liangff
 * @since
 */
public class TokenCache {
    public static final String MDPKG_INIT_TOKEN = "mdpkgInit2";

    public static final String METADATA_INIT_TOKEN = "metadataInit2";

    private static Cache tokenCache;

    public static Cache getTokenCache() {
        if (tokenCache == null) {
            CacheManager cacheManager = SpringBeanUtils.getBean(CacheManager.class);
            //声明当前要使用的缓存配置
            LayeringCacheSetting layeringCacheSetting = new LayeringCacheSetting.Builder()
                    //开启一级缓存
                    .enableFirstCache()
                    //一级缓存过期策略(可选写入后过期、访问后过期)
                    .firstCacheExpireMode(ExpireMode.ACCESS)
                    //一级缓存过期时间
                    .firstCacheExpireTime(36500)
                    //一级缓存过期时间单位
                    .firstCacheTimeUnit(TimeUnit.DAYS)
                    //一级缓存初始容量
                    .firstCacheInitialCapacity(10)
                    //一级缓存最大容量(到达最大容量后 开始驱逐低频缓存)
                    .firstCacheMaximumSize(100)
                    //开启二级缓存
                    .enableSecondCache()
                    //二级缓存过期时间
                    .secondCacheExpireTime(36500)
                    //二级缓存时间单位
                    .secondCacheTimeUnit(TimeUnit.DAYS)
                    //二级缓存序列化器
                    .dataSerializer(new TimeSerializer())
                    //缓存配置说明
                    .depict("元数据包和元数据的最后修改时间缓存初始化标识缓存").build();
            //获取缓存实例
            tokenCache = cacheManager.getCache("tokenCache", layeringCacheSetting);
        }

        return tokenCache;
    }

    public static void put(String key, LocalDateTime value) {
        Cache cache = getTokenCache();
        cache.put(RtCacheHandler.getCacheKey(key), value);
    }

    public static LocalDateTime get(String key) {
        return getTokenCache().get(RtCacheHandler.getCacheKey(key), LocalDateTime.class);
    }

    public static void evict(String key) {
        Cache cache = getTokenCache();
        cache.evict(RtCacheHandler.getCacheKey(key));
    }

    public static void setInit(String token, LocalDateTime initTime) {
        put(token, initTime);
    }

    public static Boolean hasInit(String token) {
        LocalDateTime init = get(token);
        return init != null;
    }
}
