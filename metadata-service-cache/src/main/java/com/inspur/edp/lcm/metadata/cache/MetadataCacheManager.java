/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.lcm.metadata.cache;

import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.api.entity.MetadataPackage;
import com.inspur.edp.lcm.metadata.api.entity.MetadataPackageReference;
import com.inspur.edp.lcm.metadata.api.entity.MetadataReference;
import com.inspur.edp.lcm.metadata.api.entity.ServiceUnitInfo;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 元数据运行时缓存
 */
public class MetadataCacheManager {

    //元数据包基本信息
	private static final Map<String, MetadataPackage> medataPackageInfo = new HashMap<>();
    //元数据包清单列表信息
	private static final Map<String, List<GspMetadata>> metadataManifestInfo = new HashMap<>();
    //元数据基础信息
	private static final Map<String, GspMetadata> metadataInfo = new HashMap<>();
    //元数据依赖关系
	private static final Map<String, List> metadataDependence = new HashMap<>();
    //元数据与元数据包名的映射关系
	private static final Map<String, String> metadataPathMapping = new HashMap<>();
    //所有的元数据包信息
	private static final Map<String, List<MetadataPackage>> allMetadataPackageInfo = new HashMap<>();
    //所有的元数据信息
	private static final Map<String, List<GspMetadata>> allMetadataInfo = new HashMap<>();
	private static Map<String, List<GspMetadata>> metadataListCache=new HashMap<>();
	private static final ReentrantLock lock = new ReentrantLock();

	// gspmdpkg更新时间信息
	private static Map<String, LocalDateTime> gspMdpkgLastChangedOnMap = new HashMap<>();

	// 元数据su信息
	private static Map<String, ServiceUnitInfo> serviceUnitInfoMap = new HashMap<>();

	MetadataCacheManager() {
	}

	public static void removeMetadataInfo(String key) {
		lock.lock();
		try {
			metadataInfo.remove(key);
		} finally {
			lock.unlock();
		}
	}

	public static Object getMetadataInfo(String metadataKey) {
		lock.lock();
		try {
			return metadataInfo.get(metadataKey);
		} finally {
			lock.unlock();
		}
	}

	public static void addMetadataInfo(String key, GspMetadata metadata) {
		lock.lock();
		try {
			metadataInfo.put(key, metadata);
		} finally {
			lock.unlock();
		}
	}

	public static void removeMDDependence(String key) {
		lock.lock();
		try {
			metadataDependence.remove(key);
		} finally {
			lock.unlock();
		}
	}

	public static void addMDDependence(String key, List<MetadataReference> refs) {
		lock.lock();
		try {
			metadataDependence.put(key, refs);
		} finally {
			lock.unlock();
		}
	}

	public static void removeMetadataPackageInfo(String key) {
		lock.lock();
		try {
			medataPackageInfo.remove(key);
		} finally {
			lock.unlock();
		}
	}

	public static void addMetadataPackageInfo(String key, MetadataPackage value) {
		lock.lock();
		try {
			medataPackageInfo.put(key, value);
		} finally {
			lock.unlock();
		}
	}

	public static Object getMetadataPackageInfo(String key) {
		lock.lock();
		try {
			return medataPackageInfo.get(key);
		} finally {
			lock.unlock();
		}
	}

	public static void removeMDManifestInfo(String key) {
		lock.lock();
		try {
			metadataManifestInfo.remove(key);
		} finally {
			lock.unlock();
		}
	}

	public static void addMDManifestInfo(String key, List<GspMetadata> value) {
		lock.lock();
		try {
			metadataManifestInfo.put(key, value);
		} finally {
			lock.unlock();
		}
	}

	public static Object getMDManifestInfo(String key) {
		lock.lock();
		try {
			return metadataManifestInfo.get(key);
		} finally {
			lock.unlock();
		}
	}

	public static void removeMPDependence(String key) {
		lock.lock();
		try {
			metadataDependence.remove(key);
		} finally {
			lock.unlock();
		}
	}

	public static void addMPDependence(String key, List<MetadataPackageReference> value) {
		lock.lock();
		try {
			metadataDependence.put(key, value);
		} finally {
			lock.unlock();
		}
	}

	public static Object getMetadataPathMapping(String key) {
		lock.lock();
		try {
			return metadataPathMapping.get(key);
		} finally {
			lock.unlock();
		}
	}

	public static void addMetadataPathMapping(String key, String value) {
		lock.lock();
		try {
			metadataPathMapping.put(key, value);
		} finally {
			lock.unlock();
		}
	}

	public static void removeMetadataPathMapping(String key) {
		lock.lock();
		try {
			metadataPathMapping.remove(key);
		} finally {
			lock.unlock();
		}
	}

	public static Object getAllMetadataPackageInfo(String key) {
		lock.lock();
		try {
			return allMetadataPackageInfo.get(key);
		} finally {
			lock.unlock();
		}
	}

	public static void addAllMetadataPackageInfo(String key, List<MetadataPackage> value) {
		lock.lock();
		try {
			allMetadataPackageInfo.put(key, value);
		} finally {
			lock.unlock();
		}
	}

	public static void removeAllMetadataPackageInfo(String key) {
		lock.lock();
		try {
			allMetadataPackageInfo.remove(key);
		} finally {
			lock.unlock();
		}
	}

	public static Object getAllMetadataInfo(String key) {
		lock.lock();
		try {
			return allMetadataInfo.get(key);
		} finally {
			lock.unlock();
		}
	}

	public static void addAllMetadataInfo(String key, List<GspMetadata> value) {
		lock.lock();
		try {
			allMetadataInfo.put(key, value);
		} finally {
			lock.unlock();
		}
	}

	public static Map<String, LocalDateTime> getGspMdpkgLastChangedOnMap() {
		return gspMdpkgLastChangedOnMap;
	}

	public static void putGspMdpkgLastChangedOn(String key, LocalDateTime value) {
		lock.lock();
		try {
			gspMdpkgLastChangedOnMap.put(key, value);
		} finally {
			lock.unlock();
		}
	}

	public static Object getServiceUnitInfo(String key) {
		lock.lock();
		try {
			return serviceUnitInfoMap.get(key);
		} finally {
			lock.unlock();
		}
	}

	public static void putServiceUnitInfo(String key, ServiceUnitInfo value) {
		lock.lock();
		try {
			serviceUnitInfoMap.put(key, value);
		} finally {
			lock.unlock();
		}
	}
}