/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.lcm.metadata.cache;

import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.common.MetadataSerializer;
import com.inspur.edp.lcm.metadata.common.ServiceUtils;
import io.iec.edp.caf.boot.context.CAFContext;
import io.iec.edp.caf.caching.api.Cache;
import io.iec.edp.caf.caching.api.CacheManager;
import io.iec.edp.caf.caching.cache.LayeringCache;
import io.iec.edp.caf.caching.enums.ExpireMode;
import io.iec.edp.caf.caching.setting.LayeringCacheSetting;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.CollectionUtils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Slf4j
public class MetadataDistCacheManager {
	private static Cache metadataCache;

	private static Cache l2MetadataCache;

	public static String SECOND_CACHE = "secondCache";

	public static Cache getMetadataCache() {
		if(metadataCache==null){
			CacheManager cacheManager= SpringBeanUtils.getBean(CacheManager.class);
			//声明当前要使用的缓存配置
			LayeringCacheSetting layeringCacheSetting = new LayeringCacheSetting.Builder()
					//开启一级缓存
					.enableFirstCache()
					//一级缓存过期策略(可选写入后过期、访问后过期)
					.firstCacheExpireMode(ExpireMode.ACCESS)
					//一级缓存过期时间
					.firstCacheExpireTime(24)
					//一级缓存过期时间单位
					.firstCacheTimeUnit(TimeUnit.HOURS)
					//一级缓存初始容量
					.firstCacheInitialCapacity(3000)
					//一级缓存最大容量(到达最大容量后 开始驱逐低频缓存)
					.firstCacheMaximumSize(6000)
					//开启二级缓存
					.enableSecondCache()
					//二级缓存过期时间
					.secondCacheExpireTime(24)
					//二级缓存时间单位
					.secondCacheTimeUnit(TimeUnit.HOURS)
					//二级缓存序列化器
					.dataSerializer(new MetadataSerializer())
					//缓存配置说明
					.depict("元数据缓存配置").build();
			//获取缓存实例
			metadataCache= cacheManager.getCache("metadataCache", layeringCacheSetting);
		}

		return metadataCache;
	}

	public static Cache getL2MetadataCache() {
		if (l2MetadataCache == null) {
			Cache cache = getMetadataCache();
			LayeringCache layeringCache = (LayeringCache)cache.getNativeCache();
			l2MetadataCache = layeringCache.getSecondCache();
		}
		return l2MetadataCache;
	}

	public static void put(String key, GspMetadata metadata, String level) {
		ServiceUtils.checkNPE(metadata, "元数据不可为空");
		ServiceUtils.checkNPE(metadata.getContent(), "元数据内容不可为空");
		Cache cache = getMetadataCache();
		if (SECOND_CACHE.equals(level)) {
			cache = getL2MetadataCache();
			if (cache == null) {
				cache = getMetadataCache();
			}
		}
		cache.put(RtCacheHandler.getCacheKey(key), metadata);
	}

	public static void remove(String key) {
		Cache cache = getMetadataCache();
		cache.evict(RtCacheHandler.getCacheKey(key));
	}

	public static void multiRemove(List<String> keys) {
		if (CollectionUtils.isEmpty(keys)) {
			return;
		}
		Cache cache = getMetadataCache();
		int batchSize = 1000; // caf建议每次批量删除数量不要过多，1000以内比较合适。我们的场景里基本上不会超过1000
		keys = keys.stream().map(RtCacheHandler::getCacheKey).collect(Collectors.toList());
		if (keys.size() <= batchSize) {
			cache.multiDel(new ArrayList<>(keys));
		} else {
			List<List<String>> subKeys = RtCacheHandler.getSubKeys(keys, batchSize);
			for (List<String> subKey : subKeys) {
				cache.multiDel(new ArrayList<>(subKey));
			}
		}
	}

	public static GspMetadata get(String key) {
		return getMetadataCache().get(RtCacheHandler.getCacheKey(key), GspMetadata.class);
	}

	public static GspMetadata get(String metadataId, boolean isI18n) {
		String language = isI18n ? CAFContext.current.getLanguage() : "";
		String metadataKey = RtCacheHandler.getMetadataCacheKey(metadataId, language);
		return get(metadataKey);
	}
}
