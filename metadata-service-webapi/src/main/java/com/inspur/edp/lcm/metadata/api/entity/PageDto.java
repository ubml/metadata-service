/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.lcm.metadata.api.entity;

import lombok.Data;

import java.util.List;

/**
 * Classname PageDto
 * Description 用于分页展示数据
 * Date 2023/03/24 13:44
 *
 * @author liuyuxin01
 * @version 1.0
 */
@Data
public class PageDto {

    /**
     * 数据集合
     */
    private List list;
    /**
     * 分页信息
     */
    private Page page;

    public PageDto() {
    }

    public PageDto(List list, int pageSize, int pageIndex, int total) {
        this.list = list;
        this.page = new Page(pageSize,pageIndex,total);
    }
}
