/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.metadata.rtcustomization.serverwebapi;

import com.inspur.edp.lcm.metadata.api.entity.*;
import com.inspur.edp.lcm.metadata.common.configuration.MetadataTypeHelper;
import com.inspur.edp.lcm.metadata.inner.api.BoService;
import com.inspur.edp.lcm.metadata.inner.api.utils.BoUtils;
import com.inspur.edp.metadata.rtcustomization.api.entity.DimensionExtendEntity;
import com.inspur.edp.metadata.rtcustomization.api.entity.DimensionExtendEntityDto;
import com.inspur.edp.metadata.rtcustomization.api.entity.DimensionExtendRelation;
import com.inspur.edp.metadata.rtcustomization.api.exception.ErrorCodes;
import com.inspur.edp.metadata.rtcustomization.api.exception.LcmMetadataException;
import com.inspur.edp.metadata.rtcustomization.common.MetadataCustomizationUtils;
import com.inspur.edp.metadata.rtcustomization.serverapi.CustomizationRtServerService;
import com.inspur.edp.metadata.rtcustomization.serverapi.CustomizationServerService;
import io.iec.edp.caf.commons.utils.CollectionUtils;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Objects;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author zhaoleitr
 */
@Path("/")
@Produces(MediaType.APPLICATION_JSON)
public class CustomizationService4WebApiImp {
    private CustomizationServerService customService;
    private CustomizationRtServerService customRtService;
    private CustomizationServerService getCustomService(){
        if(customService==null){
            customService=SpringBeanUtils.getBean(CustomizationServerService.class);
        }
        return customService;
    }
    private CustomizationRtServerService getCustomRtService(){
        if(customRtService==null){
            customRtService=SpringBeanUtils.getBean(CustomizationRtServerService.class);
        }
        return customRtService;
    }

    /**
     * @return com.inspur.edp.lcm.metadata.api.entity.GspMetadata
     * @Author Robin
     * @Description 运行时获取元数据
     * @Date 10:01 2019/8/20
     * @Param [metadataID]
     **/
    @GET
    @Path(value = "/{id}")
    public MetadataDto getMetadata(@PathParam("id") String id) {
        if (!StringUtils.hasLength(id)) {
            throw new LcmMetadataException(ErrorCodes.ECP_METADATA_0003, "metadataId");
        }
        return getCustomService().getMetadataDto(id);
    }

    /**
     * @return com.inspur.edp.lcm.metadata.api.entity.GspMetadata
     * @Author sunhongfei01
     * @Description 运行时获取元数据
     * @Date 2021-8-30
     * @Param [metadataID]
     **/
    @GET
    @Path(value = "/rt/{id}")
    public MetadataDto getRtMetadata(@PathParam("id") String id) {
        if (!StringUtils.hasLength(id)) {
            throw new LcmMetadataException(ErrorCodes.ECP_METADATA_0003, "metadataId");
        }
        return getCustomRtService().getMetadataDto(id);
    }

    /***
     * @author zhongchq
     * @param metadataTypes 获取元数据的类型
     * @exception RuntimeException
     * @return java.util.List<com.inspur.edp.lcm.metadata.api.entity.Metadata4Ref>
     **/
    @GET
    public List<Metadata4RefDto> getMetadataList(@QueryParam(value = "metadataTypes") String metadataTypes) {
        List<Metadata4Ref> metadatas = getCustomService().getMetadataInfoListWithTypes(metadataTypes);
        MetadataCustomizationUtils utils = new MetadataCustomizationUtils();
        return utils.buildMetadata4RefDto(metadatas);
    }

    /**
     * 获取运行时元数据列表
     * @param text 搜索关键字
     * @param pageIndex
     * @param pageSize
     * @param metadataTypeList 元数据类型列表
     * @return
     */
    @GET
    @Path("rtmetadatalist")
    public PageDto getMetadataList(@DefaultValue("")@QueryParam(value = "text") String text,
                                                 @DefaultValue("1")@QueryParam(value = "pageIndex") int pageIndex,
                                                 @DefaultValue("10")@QueryParam(value = "pageSize") int pageSize,
                                                 @QueryParam(value = "metadataTypeList") String metadataTypeList) {

        if(StringUtils.isEmpty(metadataTypeList)) {
            throw new LcmMetadataException(ErrorCodes.ECP_METADATA_0003, "metadataTypeList");
        }

        List<String> typeList = new ArrayList<>(Arrays.asList(metadataTypeList.split(",")));
        // 元数据类型转换
        List<String> typeListForDB = new ArrayList<>();
        if (!CollectionUtils.isEmpty(typeList)) {
            List<MetadataType> typeListDict = getMetadataTypeList();
            typeListDict.forEach(type -> {
                if (typeList.contains(type.getPostfix().toLowerCase())) {
                    typeListForDB.add(type.getTypeCode());
                }
            });
        }
        if(CollectionUtils.isEmpty(typeListForDB)) {
            throw new LcmMetadataException(ErrorCodes.ECP_METADATA_0016, "metadataTypeList");
        }
        List<Metadata4Ref> metadatas = getCustomService().getMetadataInfoListWithTypes(String.join(",",typeListForDB));
        MetadataCustomizationUtils utils = new MetadataCustomizationUtils();
        if(!CollectionUtils.isEmpty(metadatas) && StringUtils.hasText(text)){
            metadatas = metadatas.stream().filter(metadata4Ref -> utils.isValidMd(metadata4Ref,text)).collect(Collectors.toList());
        }
        if(CollectionUtils.isEmpty(metadatas)){
            return new PageDto(new ArrayList(),pageSize,pageIndex,0);
        }
        List<Metadata4RefDto> metadata4RefDtos = utils.buildMetadata4RefDto(metadatas.stream().skip((pageIndex - 1) * pageSize).limit(pageSize).collect(Collectors.toList()));
        return new PageDto(metadata4RefDtos,pageSize,pageIndex,metadatas.size());
    }

    @GET
    @Path(value = "/root/dimensions")
    public MetadataDto getMetadataWithRootMdIdAndDimensions(@QueryParam(value = "rootMetadataId") String rootMetadataId,
                                                     @QueryParam(value = "rootMetadataCertId") String rootMetadataCertId,
                                                     @QueryParam(value = "firstDimension") String firstDimension,
                                                     @QueryParam(value = "secondDimension") String secondDimension) {
        if (!StringUtils.hasLength(rootMetadataId)) {
            throw new LcmMetadataException(ErrorCodes.ECP_METADATA_0003, "rootMetadataId");
        }
        return getCustomService().getMetadataWithRootMdIdAndDimensions(rootMetadataId,rootMetadataCertId,firstDimension,secondDimension);
    }

    /**
     * @param metadataTypes 元数据类型
     * @return 元数据信息列表
     */
    @GET
    @Path(value = "helper")
    public List<Metadata4RefDto> getMetadataListForHelp(@QueryParam(value = "metadataTypes") String metadataTypes) {
        List<Metadata4Ref> metadatas = getCustomService().getMetadataInfoListWithTypes(metadataTypes);
        if (metadatas == null || metadatas.size() <= 0) {
            return null;
        }
        MetadataCustomizationUtils utils = new MetadataCustomizationUtils();
        List<Metadata4RefDto> dtoList = utils.buildMetadata4RefDto(metadatas);

        Map<String, String> boInfo = null;
        BoService boService = BoUtils.getBoService();
        if(Objects.nonNull(boService)){
            boInfo = boService.getId2NameMap();
        }
        for(Metadata4RefDto item : dtoList){
            if (boInfo != null && boInfo.containsKey(item.getMetadata().getBizobjectID())) {
                item.getMetadata().setBizobjectID(boInfo.get(item.getMetadata().getBizobjectID()));
            }
        }

        List<MetadataType> typesList = getMetadataTypeList();
        Map<String, String> typeInfo = new HashMap<>();
        typesList.forEach(item -> typeInfo.put(item.getTypeCode(), item.getTypeName()));


        dtoList.forEach(item -> {
            if (typeInfo.containsKey(item.getMetadata().getType())) {
                item.getMetadata().setType(typeInfo.get(item.getMetadata().getType()));
            }
        });

        return dtoList;
    }

    /**
     * @return 元数据信息列表
     */
    @GET
    @Path(value = "packaged")
    public List<Metadata4RefDto> getPackagedMetadataInfo() {
        List<Metadata4Ref> metadatas = getCustomService().getAllPackagedMetadataInfo();
        MetadataCustomizationUtils utils = new MetadataCustomizationUtils();
        return utils.buildMetadata4RefDto(metadatas);
    }

    /**
     * @param metadataTypes 元数据类型
     * @return 元数据信息列表
     */
    @GET
    @Path(value = "packaged")
    public List<Metadata4RefDto> getPackagedMetadataInfoWithTypes(@QueryParam(value = "metadataTypes") String metadataTypes) {
        List<Metadata4Ref> metadatas = getCustomService().getAllPackagedMetadataInfoWithTypes(metadataTypes);
        MetadataCustomizationUtils utils = new MetadataCustomizationUtils();
        return utils.buildMetadata4RefDto(metadatas);
    }

    /**
     * @param metadataTypes 元数据类型
     * @return 元数据信息列表
     */
    @GET
    @Path(value = "origin")
    public List<Metadata4RefDto> getAllOriginMdInfoWithTypes(@QueryParam(value = "metadataTypes") String metadataTypes) {
        List<Metadata4Ref> metadatas = getCustomService().getAllOriginMdInfoWithTypes(metadataTypes);
        MetadataCustomizationUtils utils = new MetadataCustomizationUtils();
        return utils.buildMetadata4RefDto(metadatas);
    }

    /**
     * 无人使用，GET的body中无法传参，废弃
     * 根据过滤条件获取元数据列表
     *
     * @param filter 元数据过滤
     * @return 元数据信息列表
     */
    @Deprecated
    @GET
    @Path(value = "filter")
    public List<Metadata4RefDto> getMetadataInfoByFilter(MetadataRTFilter filter) {
        List<Metadata4Ref> metadatas = getCustomService().getMetadataInfoByFilter(filter);
        MetadataCustomizationUtils utils = new MetadataCustomizationUtils();
        return utils.buildMetadata4RefDto(metadatas);
    }

    /**
     * 保存扩展元数据
     */
    @POST
    public boolean saveExtMetadataWithDimensions(DimensionExtendEntityDto dimExtEntityDto) {
        return getCustomService().saveExtMetadataWithDimensions(dimExtEntityDto);
    }

    /**
     * 删除扩展的元数据
     */
    @PUT
    public void deleteExtMetadata(@QueryParam(value = "metadataId") String metadataId,
                                  @QueryParam(value = "certId") String certId) {
        if (!StringUtils.hasLength(metadataId)) {
            throw new LcmMetadataException(ErrorCodes.ECP_METADATA_0003, "metadataId");
        }
        getCustomService().deleteExtMetadata(metadataId, certId);
    }

    /**
     * 根据维度获取元数据
     */
    @GET
    @Path(value = "/dimensions")
    public MetadataDto getMetadataWithDimensions(@QueryParam(value = "basicMetadataId") String basicMetadataId,
                                                 @QueryParam(value = "basicMetadataCertId") String basicMetadataCertId,
                                                 @QueryParam(value = "firstDimension") String firstDimension,
                                                 @QueryParam(value = "secondDimension") String secondDimension) {
        if (!StringUtils.hasLength(basicMetadataId)) {
            throw new LcmMetadataException(ErrorCodes.ECP_METADATA_0003, "basicMetadataId");
        }
        return getCustomService().getMetadataWithDimensions(basicMetadataId, basicMetadataCertId, firstDimension, secondDimension);
    }

    @GET
    @Path(value = "/validation")
    public boolean isExistExtendObjectWithDims(@QueryParam(value = "basicMetadataId") String basicMetadataId,
                                               @QueryParam(value = "basicMetadataCertId") String basicMetadataCertId,
                                               @QueryParam(value = "firstDimension") String firstDimension,
                                               @QueryParam(value = "secondDimension") String secondDimension) {
        if (!StringUtils.hasLength(basicMetadataId)) {
            throw new LcmMetadataException(ErrorCodes.ECP_METADATA_0003, "basicMetadataId");
        }
        return getCustomService().isExistExtendObjectWithDims(basicMetadataId, basicMetadataCertId, firstDimension, secondDimension);
    }

    @GET
    @Path(value = "/basic")
    public MetadataDto getBasicMetadataByExtMdId(@QueryParam(value = "metadataId") String metadataId,
                                                 @QueryParam(value = "certId") String certId) {
        if (!StringUtils.hasLength(metadataId)) {
            throw new LcmMetadataException(ErrorCodes.ECP_METADATA_0003, "metadataId");
        }
        return getCustomService().getBasicMetadataByExtMdId(metadataId, certId);
    }

    @POST
    @Path(value = "/release")
    public boolean releaseMetadataToRt(@QueryParam(value = "metadataId") String metadataId,
                                       @QueryParam(value = "certId") String certId) {
        if (!StringUtils.hasLength(metadataId)) {
            throw new LcmMetadataException(ErrorCodes.ECP_METADATA_0003, "metadataId");
        }
        return getCustomService().releaseMetadataToRt(metadataId, certId);
    }

    @GET
    @Path(value = "/extendinfo")
    public List<DimensionExtendEntityDto> getMetadataInfoRecusively(@QueryParam(value = "basicMetdataID") String basicMetdataID,
                                                                    @QueryParam(value = "basicMetadataCertId") String basicMetadataCertId) {
        if (!StringUtils.hasLength(basicMetdataID)) {
            throw new LcmMetadataException(ErrorCodes.ECP_METADATA_0003, "basicMetdataID");
        }
        List<DimensionExtendEntity> dimensionExtendEntities=  getCustomService().getMetadataInfoListRecusively(basicMetdataID, basicMetadataCertId);
        if(CollectionUtils.isEmpty(dimensionExtendEntities)){
            return null;
        }else {
            return new MetadataCustomizationUtils().buildDimensionExtendEntityDto(dimensionExtendEntities);
        }
    }

    @GET
    @Path(value = "/dimextrelation")
    public DimensionExtendRelation getDimExtRelationByExtMetadata(@QueryParam(value = "metadataId") String metadataId,
                                                                  @QueryParam(value = "certId") String certId) {
        if (!StringUtils.hasLength(metadataId)) {
            throw new LcmMetadataException(ErrorCodes.ECP_METADATA_0003, "metadataId");
        }
        return getCustomService().getDimExtRelationByExtMetadata(metadataId, certId);
    }

    @GET
    @Path(value = "/typeList")
    public List<MetadataType> getMetadataTypeList() {
        return MetadataTypeHelper.getInstance().getMetadataTypes();
    }

    @GET
    @Path(value = "/clearCache")
    public void clearCache(@QueryParam(value = "metadataIds") String metadataIds) {
        if (StringUtils.isEmpty(metadataIds)) {
            return;
        }
        getCustomService().removeCacheByMetadataIds(Arrays.asList(metadataIds.split(",")));
    }

    @GET
    @Path(value = "/clearAllCache")
    public void clearAllCache() {
        getCustomService().clearAllCache();
    }

    @GET
    @Path(value = "/root/dynamic")
    public MetadataDto getDynamicMetadata(
        @QueryParam(value = "rootMetadataId") String rootMetadataId,
        @QueryParam(value = "rootMetadataCertId") String rootMetadataCertId,
        @QueryParam(value = "firstDimensionValue") String firstDimensionValue,
        @QueryParam(value = "secondDimensionValue") String secondDimensionValue){
        if (!StringUtils.hasLength(rootMetadataId)) {
            throw new LcmMetadataException(ErrorCodes.ECP_METADATA_0003, "rootMetadataId");
        }
        return getCustomService().getDynamicMetadataWithRootMdIdAndDimensions(rootMetadataId, rootMetadataCertId, firstDimensionValue, secondDimensionValue);
    }

    @POST
    @Path(value = "/dimextrelation")
    public void updateExtRelation(DimensionExtendRelation extendRelation){
        getCustomService().updateExtRelation(extendRelation);
    }
}
