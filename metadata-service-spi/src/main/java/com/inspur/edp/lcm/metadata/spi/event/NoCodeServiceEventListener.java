/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.lcm.metadata.spi.event;


import com.inspur.edp.lcm.metadata.spi.event.nodecode.NoCodeAfterDeleteArgs;
import com.inspur.edp.lcm.metadata.spi.event.nodecode.NoCodeAfterSaveArgs;
import com.inspur.edp.lcm.metadata.spi.event.nodecode.NoCodeBeforeDeleteArgs;
import com.inspur.edp.lcm.metadata.spi.event.nodecode.NoCodeBeforeSaveArgs;
import io.iec.edp.caf.commons.event.IEventListener;

/**
 * 功能描述:零代码服务事件
 *
 * @ClassName: NoCodeServiceEventListener
 * @Author: Fynn Qi
 * @Date: 2021/5/26 14:17
 * @Version: V1.0
 */
public interface NoCodeServiceEventListener extends IEventListener {

    /**
     *  元数据保存前事件
     *
     * @param args 保存前元数据事件参数
     */
    void fireMetadataBeforeSaveEvent(NoCodeBeforeSaveArgs args);

    /**
     * 元数据保存后事件
     *
     * @param args 保存后元数据事件参数
     */
    void fireMetadataAfterSaveEvent(NoCodeAfterSaveArgs args);

    /**
     * 元数据删除前事件
     *
     * @param args 元数据删除前事件参数
     */
    void fireMetadataBeforeDeleteEvent(NoCodeBeforeDeleteArgs args);

    /**
     *元数据删除后事件
     *
     * @param args 元数据删除后事件参数
     */
    void fireMetadataAfterDeleteEvent(NoCodeAfterDeleteArgs args);

}
