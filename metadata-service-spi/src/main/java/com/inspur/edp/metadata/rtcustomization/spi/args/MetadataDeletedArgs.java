package com.inspur.edp.metadata.rtcustomization.spi.args;

import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import lombok.Data;

/**
 * 元数据删除后事件参数
 *
 * @author liangff
 * @since 0.1.0
 * 2024/1/24
 */
@Data
public class MetadataDeletedArgs {
    /**
     * 元数据，只有header
     */
    private GspMetadata metadata;

    public MetadataDeletedArgs(GspMetadata metadata) {
        this.metadata = metadata;
    }
}
