package com.inspur.edp.metadata.rtcustomization.spi.args;

import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.api.entity.ProcessMode;
import lombok.Data;

/**
 * 元数据保存后事件参数
 *
 * @author liangff
 * @since 0.1.0
 * 2024/2/4
 */
@Data
public class MetadataSavedArgs {

    /**
     * 元数据，完整信息
     */
    private GspMetadata metadata;

    /**
     * 解析型、生成型
     */
    private ProcessMode processMode;

    public MetadataSavedArgs(GspMetadata metadata, ProcessMode processMode) {
        this.metadata = metadata;
        this.processMode = processMode;
    }

}
