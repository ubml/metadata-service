/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.metadata.rtcustomization.spi;

import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.metadata.rtcustomization.api.AbstractCustomizedContent;
import com.inspur.edp.metadata.rtcustomization.spi.args.*;


/**
 * @author zhaoleitr
 */
public interface CustomizationExtHandler {

	/**
	 * 获取扩展元数据增量值，元数据和它扩展的元数据对比增量
	 *
	 * @param extContentArgs
	 * @return
	 */
	AbstractCustomizedContent getExtContent(ExtContentArgs extContentArgs);


	/**
	 * 根据基础元数据及扩展元数据合并元数据
	 *
	 * @param metadataMergeArgs
	 * @return
	 */
	GspMetadata merge(MetadataMergeArgs metadataMergeArgs);

	/**
	 * 同一层两个元数据之间对比增量
	 *
	 * @param compare4SameLevelArgs
	 * @return
	 */
	AbstractCustomizedContent getExtContent4SameLevel(Compare4SameLevelArgs compare4SameLevelArgs);


	/**
	 * @param changeMergeArgs
	 * @return
	 */
	AbstractCustomizedContent changeMerge(ChangeMergeArgs changeMergeArgs);
}
