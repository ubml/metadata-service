package com.inspur.edp.metadata.rtcustomization.spi.args;

import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import lombok.Data;

/**
 * 元数据删除前事件参数
 *
 * @author liangff
 * @since 0.1.0
 * 2024/3/14
 */
@Data
public class MetadataDeletingArgs {
    /**
     * 元数据，只有header
     */
    private GspMetadata metadata;

    public MetadataDeletingArgs(GspMetadata metadata) {
        this.metadata = metadata;
    }
}
