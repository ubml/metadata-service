/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.metadata.rtcustomization.spi.event;

import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.api.entity.ProcessMode;
import com.inspur.edp.metadata.rtcustomization.api.entity.EnvironmentEnum;
import io.iec.edp.caf.commons.event.CAFEventArgs;

public class MetadataRtEventArgs extends CAFEventArgs {
	private GspMetadata metadata;

	private ProcessMode processMode;

	private EnvironmentEnum env;

	public EnvironmentEnum getEnv() {
		return env;
	}

	public void setEnv(EnvironmentEnum env) {
		this.env = env;
	}

	public ProcessMode getProcessMode() {
		return processMode;
	}

	public void setProcessMode(ProcessMode processMode) {
		this.processMode = processMode;
	}

	public MetadataRtEventArgs() {
	}

	public MetadataRtEventArgs(GspMetadata metadata, EnvironmentEnum env) {
		this.metadata = metadata;
		this.env = env;
	}

	public MetadataRtEventArgs(GspMetadata metadata, ProcessMode processMode, EnvironmentEnum env) {
		this.metadata = metadata;
		this.processMode = processMode;
		this.env = env;
	}

	public GspMetadata getMetadata() {
		return metadata;
	}

	public void setMetadata(GspMetadata metadata) {
		this.metadata = metadata;
	}
}
