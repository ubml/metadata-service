/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.metadata.rtcustomization.spi.args;

import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.metadata.rtcustomization.api.entity.DimensionExtendEntity;
import lombok.Data;


/**
 * @author zhaoleitr
 */
@Data
public class MdConflictCheckArgs {
	public MdConflictCheckArgs(){}

	public MdConflictCheckArgs(DimensionExtendEntity extendEntity,GspMetadata basicMetadata,GspMetadata rootMetadata){
		this.setBasicMetadata(basicMetadata);
		this.setExtendEntity(extendEntity);
		this.setRootMetadata(rootMetadata);
	}

	DimensionExtendEntity extendEntity;
	GspMetadata basicMetadata;
	GspMetadata rootMetadata;
}
