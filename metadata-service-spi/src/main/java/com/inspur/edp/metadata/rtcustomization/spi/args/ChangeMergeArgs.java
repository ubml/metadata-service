/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.metadata.rtcustomization.spi.args;

import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.metadata.rtcustomization.api.AbstractCustomizedContent;
import lombok.Data;

@Data
public class ChangeMergeArgs {
	public ChangeMergeArgs(){

	}

	
	public ChangeMergeArgs(AbstractCustomizedContent parentChage, AbstractCustomizedContent changeToParent, GspMetadata rootMetadata,GspMetadata parentMetadata,GspMetadata currentMetadata){
		this.parentChage=parentChage;
		this.changeToParent=changeToParent;
		this.rootMetadata=rootMetadata;
		this.parentMetadata=parentMetadata;
		this.currentMetadata=currentMetadata;
	}
	GspMetadata parentMetadata;
	GspMetadata currentMetadata;
	AbstractCustomizedContent parentChage;
	AbstractCustomizedContent changeToParent;
	GspMetadata rootMetadata;
}
