package com.inspur.edp.metadata.rtcustomization.spi;

/**
 * 事件场景
 * 大部分事件触发支持所有场景，目前只有ExternalApi不支持在更新补丁的时候触发，所以需要判断isServer
 *
 * @author liangff
 * @since 0.1.0
 * 2024/2/5
 */
public enum EventSceneEnum {
    /**
     * 事件触发在服务器中，包括运行时、新版IDE发布、旧版IDE部署
     */
    SERVER,

    /**
     * 事件触发在工具中，包括旧版IDE部署并重启、工程部署工具部署、元数据部署工具部署、更新补丁
     */
    TOOL;

    /**
     * 是否是服务器触发
     *
     * @return true: 是
     */
    public boolean isServer() {
        return this == SERVER;
    }
}
