package com.inspur.edp.metadata.rtcustomization.spi;

import com.inspur.edp.metadata.rtcustomization.spi.args.MetadataDeletedArgs;
import com.inspur.edp.metadata.rtcustomization.spi.args.MetadataDeletingArgs;
import com.inspur.edp.metadata.rtcustomization.spi.args.MetadataSavedArgs;
import io.iec.edp.caf.multicontext.annotation.Collect;

/**
 * 元数据删除后事件
 *
 * @author liangff
 * @since 0.1.0
 * 2024/1/24
 */
@Collect
public interface MetadataRtSpi {
    /**
     * 获取元数据类型，用来标识接口针对的元数据类型，如ExternalApi
     * @return 元数据类型
     */
    String getMetadataType();

    default void metadataDeleting(MetadataDeletingArgs args) {}

    /**
     * 删除元数据后触发的事件
     * @param args 事件参数
     */
    void metadataDeleted(MetadataDeletedArgs args);

    /**
     * 保存元数据后触发的事件
     * @param args 事件参数
     */
    void metadataSaved(MetadataSavedArgs args);

    /**
     * 是否在工具中使用，ExternalApi不需要在打补丁的时候使用，只在发布时使用，需要实现这个接口，并判断是否在EventSceneEnum.SERVER中使用
     * @return 是否在场景中适用，即是在服务中，还是在工具中
     */
    default boolean available(EventSceneEnum eventScene) {
        return true;
    }
}
